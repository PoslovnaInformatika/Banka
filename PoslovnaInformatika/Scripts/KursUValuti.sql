if exists (select 1
            from  sysobjects
           where  id = object_id('KURS_U_VALUTI')
            and   type = 'U')
   drop table KURS_U_VALUTI
go

create table KURS_U_VALUTI (
	KURUV_REDNI_BR			char(9)					not null,
	KURUV_KUPOVNI			decimal(9,4)			not null,
	KURUV_SREDNJI			decimal(9,4)			not null,
	KURUV_PRODAJNI			decimal(9,4)			not null,
	VAL_ID_OSN_VAL			char(4)					not null,
	VAL_ID_PREMA_VAL		char(4)					not null,
	KURL_VAL_U_LISTI		char(4)					not null,
	constraint PK_KURS_U_VALUTI primary key (KURUV_REDNI_BR)
)
go

alter table KURS_U_VALUTI
   add constraint FK_KURS_U_VALUTI_U_VALUTE foreign key (VAL_ID_OSN_VAL)
      references VALUTE (VAL_ID)
go

alter table KURS_U_VALUTI
   add constraint FK_KURS_U_VALUTI_U_VALUTEE foreign key (VAL_ID_PREMA_VAL)
      references VALUTE (VAL_ID)
go

alter table KURS_U_VALUTI
   add constraint FK_KURS_U_VALUTI_U_KURSNA_LISTA foreign key (KURL_VAL_U_LISTI)
      references KURSNA_LISTA (KURL_ID)
go

INSERT INTO [dbo].[KURS_U_VALUTI] ([KURUV_REDNI_BR],[KURUV_KUPOVNI],[KURUV_SREDNJI],[KURUV_PRODAJNI],[VAL_ID_OSN_VAL],[VAL_ID_PREMA_VAL],[KURL_VAL_U_LISTI])
	VALUES
			('KUV1','122.9554','123.3254','122.9554','RSD','EUR','KL1'),
			('KUV2','108.4454','108.7717','109.0980','RSD','DOL','KL1'),
			('KUV3','81.0143','81.2581','81.5019','RSD','AUD','KL1')
			
GO