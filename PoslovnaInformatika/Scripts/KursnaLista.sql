if exists (select 1
            from  sysobjects
           where  id = object_id('KURSNA_LISTA')
            and   type = 'U')
   drop table KURSNA_LISTA
go

create table KURSNA_LISTA (
	KURL_ID			char(4)			not null,
	KURL_DATUM 		date			not null,
	KURL_BR_KURL	char(3)			not null,
	KURL_PRIM_OD	date			not null,
	B_ID			char(4)			not null,
	constraint PK_KURSNA_LISTA primary key (KURL_ID)
)
go

alter table KURSNA_LISTA
   add constraint FK_KURSNA_LISTA_U_B_BANKA foreign key (B_ID)
      references BANKA (B_ID)
go

INSERT INTO [dbo].[KURSNA_LISTA] ([KURL_ID],[KURL_DATUM],[KURL_BR_KURL],[KURL_PRIM_OD],[B_ID])
	VALUES
			('KL1','20160615','1','20160610','ERS'),
			('KL2','20160617','2','20160610','ERS')
GO