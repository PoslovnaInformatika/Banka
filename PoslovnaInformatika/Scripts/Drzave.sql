﻿if exists (select 1
            from  sysobjects
           where  id = object_id('DRZAVA')
            and   type = 'U')
   drop table DRZAVA
go

create table DRZAVA (
   DR_SIFRA             char(4)              not null,
   DR_NAZIV             varchar(80)          not null,
   constraint PK_DRZAVA primary key  (DR_SIFRA)
)
go

INSERT INTO [dbo].[DRZAVA] ([DR_SIFRA] ,[DR_NAZIV])
     VALUES
           ('BRA', 'Brazil'),
		   ('SRB', 'Srbija'),
		   ('USA', 'Sjedinjene Američke Države'),
		   ('AUS', 'Australija'),
		   ('DEU', 'Nemačka'),
		   ('BIH', 'Bosna i Hercegovina')
GO