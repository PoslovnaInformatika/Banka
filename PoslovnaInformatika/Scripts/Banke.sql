﻿if exists (select 1
            from  sysobjects
           where  id = object_id('BANKA')
            and   type = 'U')
   drop table BANKA
go

create table BANKA (
   B_ID            char(4)          not null,
   B_SIFRA         char(3)          not null,
   B_PIB			char(10)		not null,
   B_NAZIV			varchar(120)	not null,
   B_ADRESA			varchar(120)	not null,
   B_EMAIL			varchar(128),	
   B_WEB			varchar(128),	
   B_TELEFON		varchar(20),	
   B_FAX			varchar(20),	
   constraint PK_BANKA primary key  (B_ID)
)
go

INSERT INTO [dbo].[BANKA] ([B_ID], [B_SIFRA], [B_PIB], [B_NAZIV], [B_ADRESA], [B_EMAIL], [B_WEB], [B_TELEFON], [B_FAX] )
     VALUES
           ('ERS', 'b1', '01234', 'Erste Banka', 'Bul Oslobodjenja 12', 'info@erstebanka.rs', 'www.erstebank.rs', '021870680', '0008888'),
           ('UNI', 'b2', '56789', 'UniCredit Banka', 'Ulica Kralja Aleksandra 23', 'kontakt@unicreditbanc.rs', 'www.unicreditbank.rs', '021500500', '1234567'),
           ('BINT', 'b3', '10112', 'Banca Intesa', 'Kosovska 6', 'bancaintesa@banca.net', 'www.bancaintesa.net', '021896698', '1425368')
           
		   
GO