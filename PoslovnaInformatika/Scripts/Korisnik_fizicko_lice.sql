CREATE TABLE KORISNIK_FL (
	FL_JMBG char(13) PRIMARY KEY,
	FL_IME varchar(20) NOT NULL,
	FL_PREZIME varchar(20) NOT NULL,
	FL_POL char(1) NOT NULL,
	FL_ADRESA varchar(40) NOT NULL,
	FL_DATUM_RODJENJA date NOT NULL,
	NM_SIFRA char(3) foreign key references NASELJENO_MESTO(NM_SIFRA) NOT NULL,
	FL_EMAIL varchar(60),
	FL_TELEFON varchar(15)
);

INSERT INTO [dbo].[KORISNIK_FL] ([FL_JMBG] ,[FL_IME] ,[FL_PREZIME], [FL_POL], [FL_ADRESA], [FL_DATUM_RODJENJA], [NM_SIFRA], [FL_EMAIL], [FL_TELEFON])
     VALUES
           ('1111111111111', 'Sonja', 'Zivkovic', 'z', 'Negde kod lutrije 14', '2016-06-09', 'NS', 'sonja@seficapodzemlja.com', '23456'),
           ('2222222222222', 'Vladimir', 'Matovic', 'm', 'Nepoznata ulica 52', '2016-06-09', 'NS', 'email2', '23456'),
           ('3333333333333', 'Jelena', 'Pecelj', 'z', 'Plavusin bulevar 28a', '2016-06-09', 'TR', 'email3', '23456'),
           ('4444444444444', 'Miodrag', 'Gavric', 'm', 'Izbosne 4', '2016-06-09', 'BN', 'email4', '23456'),
           ('5555555555555', 'Branislav', 'Milutinov', 'm', 'NaPodbari 12', '2016-06-09', 'NS', 'email4', '23456')
GO