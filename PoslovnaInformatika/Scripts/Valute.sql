﻿if exists (select 1
            from  sysobjects
           where  id = object_id('VALUTE')
            and   type = 'U')
   drop table VALUTE
go

create table VALUTE (
	VAL_ID			char(4)			not null,
	VAL_SIFRA 		char(3)			not null,
	VAL_NAZIV		varchar(30)		not null,
	DR_SIFRA		char(4)			not null,
	constraint PK_VALUTE primary key (VAL_ID)
)
go

alter table VALUTE
   add constraint FK_VALUTA_U_D_DRZAVA foreign key (DR_SIFRA)
      references DRZAVA (DR_SIFRA)
go

INSERT INTO [dbo].[VALUTE] ([VAL_ID],[VAL_SIFRA],[VAL_NAZIV],[DR_SIFRA])
	VALUES
			('BRL','BRL','Brazilski real','BRA'),
			('RSD','RSD','Srpski dinar','SRB'),
			('DOL','DOL','Americki dolar','USA'),
			('AUD','AUD','Australijski dolar','AUS'),
			('EUR','EUR','Euro','DEU'),
			('BAM','BAM','Bosanska Marka', 'BIH')
GO