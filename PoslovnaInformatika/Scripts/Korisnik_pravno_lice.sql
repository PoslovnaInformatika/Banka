CREATE TABLE KORISNIK_PL (
	PL_PIB char(10) PRIMARY KEY,
	PL_NAZIV varchar(50) NOT NULL,
	PL_ADRESA varchar(40) NOT NULL,
	NM_SIFRA char(3) foreign key references NASELJENO_MESTO(NM_SIFRA),
	PL_WEB varchar(100),
	PL_EMAIL varchar(60),
	PL_TELEFON varchar(15),
	PL_FAX varchar(20)
);

INSERT INTO [dbo].[KORISNIK_PL] ([PL_PIB] ,[PL_NAZIV] ,[PL_ADRESA], [NM_SIFRA], [PL_WEB], [PL_EMAIL], [PL_TELEFON], [PL_FAX])
     VALUES
           ('PL1', 'FTN', 'Trg Dositeja Obradovica', 'NS', 'web1', 'email1', '23456', '445'),
           ('PL2', 'Park City', 'Narodnog Fronta', 'NS', 'web2', 'email2', '23456', '5666'),
           ('PL3', 'Levi9', 'Narodnog Fronta', 'NS', 'web3', 'email3', '23456', '444'),
           ('PL4', 'DMS', 'Narodnog Fronta', 'NS', 'web4', 'email4', '23456', '34241')
GO