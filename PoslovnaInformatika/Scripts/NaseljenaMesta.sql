if exists (select 1
            from  sysindexes
           where  id    = object_id('NASELJENO_MESTO')
            and   name  = 'MESTA_U_DRZAVI_FK'
            and   indid > 0
            and   indid < 255)
   drop index NASELJENO_MESTO.MESTA_U_DRZAVI_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('NASELJENO_MESTO')
            and   type = 'U')
   drop table NASELJENO_MESTO
go

create table NASELJENO_MESTO (
   NM_SIFRA             char(3)              not null,
   DR_SIFRA             char(4)              not null,
   NM_NAZIV             varchar(60)          not null,
   NM_PTTOZNAKA         varchar(12)          not null,
   constraint PK_NASELJENO_MESTO primary key nonclustered (NM_SIFRA)
)
go

alter table NASELJENO_MESTO
   add constraint FK_NASELJEN_MESTA_U_D_DRZAVA foreign key (DR_SIFRA)
      references DRZAVA (DR_SIFRA)
go

INSERT INTO [dbo].[NASELJENO_MESTO] ([NM_SIFRA] ,[DR_SIFRA] ,[NM_NAZIV], [NM_PTTOZNAKA])
     VALUES
           ('VE','SRB', 'Veternik', '21203'),
           ('FU','SRB', 'Futog', '21410'),
           ('NS','SRB', 'Novi Sad', '21000'),
           ('BG','SRB', 'Beograd', '11000'),
           ('SU','SRB', 'Subotica', '24000'),
           ('KG','SRB', 'Kragujevac', '34000'),
           ('NI','SRB', 'Nis', '18000'),
           ('KS','SRB', 'Krusevac', '37000'),
           ('KR','SRB', 'Kraljevo', '36000'),
           ('ZR','SRB', 'Zrenjanin', '23000'),
           ('TR','BIH', 'Trebinje', '89000'),
           ('BN','BIH', 'Bijeljina', '76300')
GO