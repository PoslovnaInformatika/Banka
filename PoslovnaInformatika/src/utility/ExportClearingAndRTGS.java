package utility;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import javax.swing.JFileChooser;

import database.DBConnection;
import gui.main.form.MainFrame;
import gui.xmlimport.form.AbstractImportXMLForm.VrstaTransakcije;
import gui.xmlimport.form.ImportTransakcijaForm;
import jaxb.clearing.Clearing;
import jaxb.clearing.Clearing.ListaDetalja;
import jaxb.clearing.Clearing.ListaDetalja.Detalji;
import jaxb.clearing.Clearing.Zaglavlje;
import jaxb.rtgs.Rtgs;
import jaxb.uplatnica.transakcija.UplatnicaTransakcija;

public class ExportClearingAndRTGS {
	
	private ImportTransakcijaForm transForm;
	private JFileChooser fc;
	private UplatnicaJAXB convert;
	private Rtgs doc;
	private UplatnicaTransakcija uplatnica;
	Clearing object = new Clearing();
	ListaDetalja listaDetalja = new ListaDetalja();;
	Zaglavlje zaglavlje;
	Detalji detalji;
	Integer brojac = 0;
	
	public ExportClearingAndRTGS(ImportTransakcijaForm form) {
		transForm = form;
		convert = new UplatnicaJAXB();
		fc = new JFileChooser();
	}
	
	public void exportRTGS(UplatnicaTransakcija uplatnica, int vrstaTransakcije){
		int returnVal = fc.showSaveDialog(MainFrame.getInstance());
		this.uplatnica = uplatnica;
		
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            try {
				processRTGS(uplatnica, vrstaTransakcije);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
			convert.marshall(doc, file);
        }
	}
	
	public void exportClearing(UplatnicaTransakcija uplatnica){
		int returnVal = fc.showSaveDialog(MainFrame.getInstance());
		
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            processClearing(uplatnica);
            
			convert.marshall(object, file);
        }
		}
	
	public void processClearing( UplatnicaTransakcija uplatnica) {
		
		zaglavlje = new Zaglavlje();
		detalji = new Detalji();
		
		if(brojac==0) {
		zaglavlje.setDatum(uplatnica.getPlacanje().getDatumIMestoPrijema().getDatum());
		zaglavlje.setDatumValute(uplatnica.getPlacanje().getDatumValute());
		zaglavlje.setIdPoruke(UUID.randomUUID().toString());
		zaglavlje.setSifraValute(uplatnica.getPlacanje().getValuta());
		zaglavlje.setSwiftKodBankeDuznika("id banke duz");
		zaglavlje.setSwiftKodBankePoverioca("id banke pov");
		zaglavlje.setUkupanIznos("ukupan iznos");
		object.setZaglavlje(zaglavlje);
		brojac++;
		}
		detalji.setDatumNaloga(uplatnica.getPlacanje().getDatumIMestoPrijema().getDatum());
		detalji.setDuznik(uplatnica.getDuznik().getNaziv());
		detalji.setIdNalogaZaPlacanje(UUID.randomUUID().toString());
		detalji.setIznos(uplatnica.getPlacanje().getIznos());
		detalji.setModelOdobrenja(String.valueOf(uplatnica.getPlacanje().getPoverilac().getBrojModela()));
		detalji.setModelZaduzenja(String.valueOf(uplatnica.getPlacanje().getDuznik().getBrojModela()));
		detalji.setPozivNaBrojOdobrenja(String.valueOf(uplatnica.getPlacanje().getPoverilac().getPozivNaBroj()));
		detalji.setPozivNaBrojZaduzenja(String.valueOf(uplatnica.getPlacanje().getDuznik().getPozivNaBroj()));
		detalji.setPrimalac(uplatnica.getPoverilac().getNaziv());
		detalji.setRacunDuznika(uplatnica.getDuznik().getRacun());
		detalji.setRacunPrimaoca(uplatnica.getPoverilac().getRacun());
		detalji.setSifraValute(uplatnica.getPlacanje().getValuta());
		detalji.setSvrhaPlacanja(uplatnica.getPlacanje().getSvrhaPlacanja());
		listaDetalja.getDetalji().add(detalji);
		object.setListaDetalja(listaDetalja);
	
	}
	
	private void processRTGS(UplatnicaTransakcija uplatnica, int vrstaTransakcije) throws SQLException{
		doc = new Rtgs();
		PreparedStatement stmnt = null;
		ResultSet rset;
		
		if(vrstaTransakcije == VrstaTransakcije.POSTOJECI_RACUN_DUZNIKA.getID()){
			stmnt = DBConnection.getConnection().prepareStatement("SELECT b_id, r_vazeci, val_id FROM RACUN WHERE R_BRRACUNA = ?");
			stmnt.setString(1, uplatnica.getDuznik().getRacun().replace("-", ""));
			rset = stmnt.executeQuery();
		
			if(rset.next()){
				doc.setSwiftKodBankeDuznika(rset.getString("b_id"));
				doc.setSwiftKodBankePoverioca("/");
			
			}
		}
		else if(vrstaTransakcije == VrstaTransakcije.POSTOJECI_RACUN_POVERIOCA.getID()){
			stmnt = DBConnection.getConnection().prepareStatement("SELECT b_id FROM RACUN WHERE R_BRRACUNA = ?");
			stmnt.setString(1, uplatnica.getPoverilac().getRacun().replace("-", ""));
			rset = stmnt.executeQuery();
		
			if(rset.next()){
				doc.setSwiftKodBankeDuznika("/");
				doc.setSwiftKodBankePoverioca(rset.getString("b_id"));
			
			}
		}
		else if(vrstaTransakcije == VrstaTransakcije.POSTOJECI_RACUN_NA_RACUN_DRUGE_BANKE.getID()){
			stmnt = DBConnection.getConnection().prepareStatement("SELECT b_id, r_brracuna FROM RACUN WHERE R_BRRACUNA = ? or R_BRRACUNA = ?");
			stmnt.setString(1, uplatnica.getDuznik().getRacun().replace("-", ""));
			stmnt.setString(2, uplatnica.getPoverilac().getRacun().replace("-", ""));
			rset = stmnt.executeQuery();
		
			if(rset.next()){
				if(rset.getString("r_brracuna").equals(uplatnica.getDuznik().getRacun().replace("-", "")))
					doc.setSwiftKodBankeDuznika(rset.getString("b_id"));
				else if(rset.getString("r_brracuna").equals(uplatnica.getPoverilac().getRacun().replace("-", "")))
					doc.setSwiftKodBankePoverioca(rset.getString("b_id"));
			}
		}
		
		popuniObjekatRTGS();
		
		stmnt.close();
		
	}
	
	private void popuniObjekatRTGS(){
		doc.setDuznik(uplatnica.getDuznik().getNaziv() + ", " + uplatnica.getDuznik().getAdresa().getUlica() + " "
				+ uplatnica.getDuznik().getAdresa().getBroj()  + " " + uplatnica.getDuznik().getAdresa().getPosta() + " "
				+ uplatnica.getDuznik().getAdresa().getMesto());
	
		doc.setPrimalac(uplatnica.getPoverilac().getNaziv() + ", " + uplatnica.getPoverilac().getAdresa().getUlica() + " "
				+ uplatnica.getPoverilac().getAdresa().getBroj()  + " " + uplatnica.getPoverilac().getAdresa().getPosta() + " "
				+ uplatnica.getPoverilac().getAdresa().getMesto());
	
		doc.setRacunDuznika(uplatnica.getDuznik().getRacun());
		doc.setRacunPrimaoca(uplatnica.getPoverilac().getRacun());
	
		doc.setModelZaduzenja(uplatnica.getPlacanje().getDuznik().getBrojModela());
		doc.setModelOdobrenja(uplatnica.getPlacanje().getPoverilac().getBrojModela());
	
		doc.setPozivNaBrojZaduzenja(uplatnica.getPlacanje().getDuznik().getPozivNaBroj());
		doc.setPozivNaBrojOdobrenja(uplatnica.getPlacanje().getPoverilac().getPozivNaBroj());
	
		doc.setSifraValute(uplatnica.getPlacanje().getValuta());
		doc.setSvrhaPlacanja(uplatnica.getPlacanje().getSvrhaPlacanja());
		doc.setIznos(uplatnica.getPlacanje().getIznos());
		doc.setDatumValute(uplatnica.getPlacanje().getDatumValute());
		doc.setDatumNaloga(uplatnica.getPlacanje().getDatumIMestoPrijema().getDatum());
		doc.setIdPoruke(UUID.randomUUID().toString());
	}

}
