package utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ColumnList {

  private List<Column> columns;
  
  public ColumnList() {
    columns = new ArrayList<Column>();
  }
  
  public ColumnList(List<Column> columns) {
    this();
    this.columns.addAll(columns);
  }
  
  public Column getColumn(String name) {
    Iterator<Column> it = columns.iterator();
    while (it.hasNext()) {
      Column c = it.next();
      if (c.getName().equals(name))
        return c;
    }
    return null;
  }
  
  public Object getValue(String name) {
    Column c = getColumn(name);
    if (c == null)
      return null;
    else
      return c.getValue();
  }
  
  public int getColumnCount() {
    return columns.size();
  }
  
  public Column getColumn(int index) {
    if (index >=0 && index < columns.size())
      return columns.get(index);
    else
      return null;
  }
  
  public void add(Column c) {
    columns.add(c);
  }
  
  public void remove(Column c) {
    columns.remove(c);
  }
  
  public Iterator<Column> iterator() {
    return columns.iterator();
  }

}