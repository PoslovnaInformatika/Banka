package utility;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class UplatnicaJAXB {

	public Object unmarshall(Object klasa, String path) {
		Object ud = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(klasa.getClass());
			Unmarshaller ums = jc.createUnmarshaller();
			ud = klasa.getClass().newInstance();
		    ud = ud.getClass().cast(ums.unmarshal(new File(path)));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ud;
	}
	
	public Object marshall(Object klasa, File out) {
		  Object upld = null;
		  try {
		   JAXBContext jc = JAXBContext.newInstance(klasa.getClass());
		   Marshaller ms = jc.createMarshaller();
		   ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		   ms.marshal(klasa, out);
		  } catch (Exception e) {
		   System.out.println(e.getMessage());
		  }
		  return upld;
		 }
}
