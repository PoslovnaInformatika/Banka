package export;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jaxb.analitikaIzvoda.AnalitikaIzvoda;
import jaxb.analitikaIzvoda.AnalitikaIzvoda.ListaAnalitike;
import jaxb.analitikaIzvoda.AnalitikaIzvoda.ListaAnalitike.Duznik;
import jaxb.analitikaIzvoda.AnalitikaIzvoda.ListaAnalitike.Placanje;
import jaxb.analitikaIzvoda.AnalitikaIzvoda.ListaAnalitike.Poverilac;
import database.DBConnection;
import gui.main.form.MainFrame;
import gui.standard.form.RacunStandardForm;
import net.miginfocom.swing.MigLayout;
import utility.UplatnicaJAXB;

public class ExportForm extends JDialog {

	private JLabel odabirRacunaLabel;
	private JTextField racunTf;
	private JButton createReportButton;
	private JButton zoomButton;
	AnalitikaIzvoda doc = new AnalitikaIzvoda();
	ListaAnalitike objekat;
	private JFileChooser fc = new JFileChooser();
	private UplatnicaJAXB convert;
	Duznik duznik;
	Placanje placanje;
	Poverilac poverilac;
	
	public ExportForm() {
		convert = new UplatnicaJAXB();
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		this.setTitle("Export analitike izvoda korisnika");
		setLayout(new MigLayout("gapx 15px"));
		setSize(new Dimension(310,290));
		setLocationRelativeTo(MainFrame.getInstance());
		setResizable(false);
		setModal(true);
		
		odabirRacunaLabel = new JLabel("Korisnik: ");
		racunTf = new JTextField(30);
		zoomButton = new JButton("...");
		zoomButton.setMaximumSize(new Dimension(70, 20));
		zoomButton.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				RacunStandardForm racunForm = new RacunStandardForm();
				racunForm.setVisible(true);
				racunTf.setText(racunForm.getTfBrojRacuna().getText());
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		createReportButton = new JButton("Export analitike izvoda korisnika:");
		createReportButton.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				boolean provera = false;
				ResultSet rs = null;
				PreparedStatement stmnt = null;
			//	if(validateInput(racunTf.getText().trim()) == false) return;
				try {
					stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM ANALITIKA_IZVODA WHERE AI_RACUNDUZNIKA = ? OR AI_RACUNPOVERIOCA = ? ORDER BY AI_DATUMPRIJEMA DESC");
					stmnt.setString(1, racunTf.getText());
					stmnt.setString(2, racunTf.getText());
					rs = stmnt.executeQuery();
					while(rs.next()) {
						objekat = new ListaAnalitike();
						duznik = new Duznik();
						poverilac = new Poverilac();
						placanje = new Placanje();
						duznik.setNaziv(rs.getString("ai_duznik"));
						duznik.setRacunDuznika(rs.getString("ai_racunduznika"));
						duznik.setModelZaduzenja(rs.getString("ai_modelzaduzenja"));
						duznik.setPozivNaBrojZaduzenja(rs.getString("ai_pozivnabrojzaduzenja"));
						poverilac.setNaziv(rs.getString("ai_poverilac"));
						poverilac.setRacunPoverioca(rs.getString("ai_racunpoverioca"));
						poverilac.setModelOdobrenja(rs.getString("ai_modelodobrenja"));
						poverilac.setPozivNaBrojOdobrenja(rs.getString("ai_pozivnabrojodobrenja"));
						placanje.setBrojIzvoda(rs.getString("dsr_brojizvoda"));
						placanje.setBrojStavke(rs.getString("ai_brojstavke"));
						placanje.setDatumPrijema(rs.getString("ai_datumprijema"));
						placanje.setDatumValute(rs.getString("ai_datumvalute"));
						placanje.setHitno(rs.getString("ai_hitno"));
						placanje.setIznos(rs.getString("ai_iznos"));
						placanje.setSvrhaPlacanja(rs.getString("ai_svrhaplacanja"));
						placanje.setVrstaPlacanja(rs.getString("ai_vrstaplacanja"));
						
						
						objekat.setDuznik(duznik);
						objekat.setPoverilac(poverilac);
						objekat.setPlacanje(placanje);
						provera = true;
						doc.getListaAnalitike().add(objekat);
					}	

					if (!provera) {
						JOptionPane.showMessageDialog(null, "Racun " +racunTf.getText() + " ne postoji.", "Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					int returnVal = fc.showSaveDialog(MainFrame.getInstance());
					
			        if (returnVal == JFileChooser.APPROVE_OPTION) {
			            File file = fc.getSelectedFile();

							convert.marshall(doc, file);
			        }
					
					rs.close();
					stmnt.close();
				
					} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			
				}
				
				
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		
		
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new BorderLayout());
		
		JPanel panel = new JPanel();
		panel.setLayout(new MigLayout("gapx 15px"));
		panel.add(odabirRacunaLabel);
		panel.add(racunTf);
		panel.add(zoomButton, "wrap");
		
		dataPanel.add(panel, BorderLayout.NORTH);
		createReportButton.setMaximumSize(new Dimension(100, 30));
		dataPanel.add(createReportButton, BorderLayout.SOUTH);
		
		dataPanel.setBorder(BorderFactory.createTitledBorder("Export analitike izvoda korisnika:")) ;
		add(dataPanel);
	}

}
