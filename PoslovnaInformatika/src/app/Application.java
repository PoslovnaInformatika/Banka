package app;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import gui.login.form.LoginDialog;
import gui.main.form.MainFrame;
import jaxb.uplatnica.duznik.UplatnicaDuznik;
import utility.UplatnicaJAXB;

public class Application {
	
	public static void main (String[] args){
		MainFrame.getInstance().setVisible(true);
		
		LoginDialog dialog = new LoginDialog(MainFrame.getInstance());
		dialog.setVisible(true);
	
		//UplatnicaJAXB ud = new UplatnicaJAXB();
		
		//UplatnicaDuznik nesto = (UplatnicaDuznik)ud.unmarshall(new UplatnicaDuznik(), "xml.examples\\UplatnicaDuznik.xml");
		//System.out.println( "Ime:" + nesto.getDuznik().getNaziv());
		
	}
}
