package listeners;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

public class TextFieldTextValidator extends KeyAdapter {
	
	public static enum CharTypes
	{
		Whatever,
		Letters,
		Numbers,
		Decimal,
	};
	
	private int maxTextLength;
	private JTextField textField;
	private CharTypes charType;
	
	boolean ctrlPressed = false;
    boolean vPressed = false;
	
	public TextFieldTextValidator(int maxTextLength, JTextField textField, CharTypes charType)
	{
		this.maxTextLength = maxTextLength;
		this.textField = textField;
		this.charType = charType;
	}
	
    @Override
	public void keyTyped(java.awt.event.KeyEvent evt) {
        if(textField.getText().length() >= maxTextLength 
        		&& !(evt.getKeyChar() == KeyEvent.VK_DELETE || evt.getKeyChar() == KeyEvent.VK_BACK_SPACE)) {
            textField.getToolkit().beep();
            evt.consume();
            return;
         }
        if(charType == CharTypes.Letters && !(evt.getKeyChar() >= KeyEvent.VK_A  && evt.getKeyChar() <= KeyEvent.VK_Z) 
        		&& !(evt.getKeyChar() >= KeyEvent.VK_A  + KeyEvent.VK_SPACE && evt.getKeyChar() <= KeyEvent.VK_Z + KeyEvent.VK_SPACE)
        	&& !(evt.getKeyChar() == KeyEvent.VK_DELETE || evt.getKeyChar() == KeyEvent.VK_BACK_SPACE) && !(evt.getKeyChar() == KeyEvent.VK_SPACE))
        {
        	textField.getToolkit().beep();
            evt.consume();
        }
        if(charType == CharTypes.Numbers && (evt.getKeyChar() < KeyEvent.VK_0 || evt.getKeyChar() > KeyEvent.VK_9)
        		&& !(evt.getKeyChar() == KeyEvent.VK_DELETE || evt.getKeyChar() == KeyEvent.VK_BACK_SPACE))
        {
        	textField.getToolkit().beep();
            evt.consume();
        }
        if(charType == CharTypes.Decimal)
        {
        	if((evt.getKeyChar() < KeyEvent.VK_0 || evt.getKeyChar() > KeyEvent.VK_9)
            		&& !(evt.getKeyChar() == KeyEvent.VK_DELETE || evt.getKeyChar() == KeyEvent.VK_BACK_SPACE || evt.getKeyChar() == KeyEvent.VK_PERIOD))
            {
            	textField.getToolkit().beep();
                evt.consume();
                return;
            }
        	if(textField.getText().contains(".") && evt.getKeyChar() == KeyEvent.VK_PERIOD)
        	{
        		textField.getToolkit().beep();
                evt.consume();
        	}
        }
     }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()) {
        case KeyEvent.VK_V:
            vPressed=true;
            break;
        case KeyEvent.VK_CONTROL:
            ctrlPressed=true;
            break;
        }
        if(ctrlPressed && vPressed) {
        	textField.getToolkit().beep();
            e.consume();// Stop the event from propagating.
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()) {
        case KeyEvent.VK_V:
            vPressed=false;
            break;
        case KeyEvent.VK_CONTROL:
            ctrlPressed=false;
            break;
        }
        if(ctrlPressed && vPressed) {
            e.consume();// Stop the event from propagating.
            return;
        }
    }
    
    public static boolean validateDecimalInput(String text, int num, int dec)
	{
		String regex1 = "^\\d{0," + num + "}$";
		String regex2 = "^\\d{0," + num + "}+\\.\\d{0," + dec + "}$";
    	//if(text.matches("^\\d{0,9}$") || text.matches("^\\d{0,9}+\\.\\d{0,4}$"))
		if(text.matches(regex1) || text.matches(regex2))
    	{
    		return true;
    	}
    	return false;
	}
}
