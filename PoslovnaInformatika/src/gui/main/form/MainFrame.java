package gui.main.form;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import actions.main.form.AutoriAction;
import actions.main.form.DialogAction;
import actions.main.form.ExitAction;
import actions.main.form.ExportAction;
import actions.main.form.HelpAction;
import actions.main.form.ImportovanjeAction;
import actions.main.form.IzvestajiAction;
import actions.main.form.OdjavaAction;
import database.DBConnection;
import gui.standard.form.AnalitikaIzvodaStandardForm;
import gui.standard.form.BankaStandardForm;
import gui.standard.form.DnevnoStanjeRacunaStandardForm;
import gui.standard.form.DrzavaStandardForm;
import gui.standard.form.FizickoLiceStandardForm;
import gui.standard.form.KursUValutiStandardForm;
import gui.standard.form.KursnaListaStandardForm;
import gui.standard.form.NaseljenoMestoStandardForm;
import gui.standard.form.PravnoLiceStandardForm;
import gui.standard.form.RacunStandardForm;
import gui.standard.form.UkidanjeStandardForm;
import gui.standard.form.ValuteStandardForm;
import gui.xmlimport.form.ImportDuznikForm;
import gui.xmlimport.form.ImportPoverilacForm;
import gui.xmlimport.form.ImportTransakcijaForm;
import net.miginfocom.swing.MigLayout;

public class MainFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public static MainFrame instance;
	private JMenuBar menuBar;

	public MainFrame(){

		setSize(new Dimension(600,500));
		getContentPane().setLayout(new MigLayout("fill"));
		setLocationRelativeTo(null);
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		setTitle("Poslovna banka");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		//setExtendedState(MAXIMIZED_BOTH);
		setUpMenu();

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
					DBConnection.close();
					System.exit(0);
			}
		});
		
		setJMenuBar(menuBar);
		
		JPanel picturePanel = new JPanel();
		picturePanel.setLayout(new MigLayout("fillx"));
		JLabel pic = new JLabel(new ImageIcon(getClass().getResource("/img/money-pig.jpeg")));
		picturePanel.add(pic);
		add(picturePanel, BorderLayout.CENTER);
	}

	private void setUpMenu(){
		menuBar = new JMenuBar();
		
		
		JMenu aplikacijaMenu = new JMenu("Aplikacija");
		aplikacijaMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Settings-icon.png")));
		aplikacijaMenu.setMnemonic(KeyEvent.VK_A);
		
		
		JMenuItem izvestajiMI = new JMenuItem(new IzvestajiAction());
		izvestajiMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Diary-icon.png")));
		izvestajiMI.setMnemonic(KeyEvent.VK_Z);
		
		JMenuItem exportMI = new JMenuItem(new ExportAction());
		exportMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Arrow-outside-icon.png")));
		exportMI.setMnemonic(KeyEvent.VK_E);
		
		
		JMenuItem odjavaMI = new JMenuItem(new OdjavaAction());
		odjavaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Arrow-inside-icon.png")));
		odjavaMI.setMnemonic(KeyEvent.VK_O);
		
		
		JMenuItem exitMI = new JMenuItem(new ExitAction());
		exitMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Button-power-icon.png")));
		exitMI.setMnemonic(KeyEvent.VK_I);
	
		
		
		// Transakcije meni
		JMenu transakcijeMenu = new JMenu("Transakcije");
		transakcijeMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Networking-icon.png")));
		transakcijeMenu.setMnemonic(KeyEvent.VK_T);
		JMenuItem duznikMI = new JMenuItem(new ImportovanjeAction("Duznik", KeyEvent.VK_D, new ImportDuznikForm()) );
		duznikMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Arrow-next-icon.png")));
		JMenuItem poverilacMI = new JMenuItem(new ImportovanjeAction("Poverilac", KeyEvent.VK_P, new ImportPoverilacForm()));
		poverilacMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Arrow-previous-icon.png")));
		JMenuItem transakcijaMI = new JMenuItem(new ImportovanjeAction("Transakcija", KeyEvent.VK_T, new ImportTransakcijaForm()));
		transakcijaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Arrow-reload-4-icon.png")));
		transakcijeMenu.add(duznikMI);
		transakcijeMenu.add(poverilacMI);
		transakcijeMenu.add(transakcijaMI);
		
		aplikacijaMenu.add(izvestajiMI);
		aplikacijaMenu.addSeparator();
		aplikacijaMenu.add(exportMI);
		aplikacijaMenu.addSeparator();
		aplikacijaMenu.add(transakcijeMenu);
		aplikacijaMenu.addSeparator();
		aplikacijaMenu.add(odjavaMI);
		aplikacijaMenu.addSeparator();
		aplikacijaMenu.add(exitMI);
		
		menuBar.add(aplikacijaMenu);

		JMenu orgSemaMenu = new JMenu("Organizaciona šema");
		orgSemaMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Chart-pie-icon.png")));
		JMenu korisniciMenu = new JMenu("Korisnici");
		korisniciMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Group-icon.png")));
		
		JMenuItem drzaveMI;
		JMenuItem mestoMI;
		JMenuItem pravnaLicaMI;
		JMenuItem fizickaLicaMI;
		JMenuItem racuniMI;
		JMenuItem dnevnoStanjeRacunaMI;
		JMenuItem bankaMI;
		JMenuItem kursnaListaMI;
		JMenuItem valuteMI;
		JMenuItem kursUValutiMI;
		JMenuItem analitikaIzvodaMI;
		JMenuItem ukidanjeMI;
		
		orgSemaMenu.setMnemonic(KeyEvent.VK_O);
		
		drzaveMI = new JMenuItem(new DialogAction("Države", KeyEvent.VK_D, new DrzavaStandardForm()));
		drzaveMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Browser-icon.png")));
		mestoMI = new JMenuItem(new DialogAction("Naseljena mesta", KeyEvent.VK_N, new NaseljenoMestoStandardForm()));
		mestoMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Place-icon.png")));
		
		
		dnevnoStanjeRacunaMI = new JMenuItem(new DialogAction("Dnevno stanje racuna", KeyEvent.VK_D, new DnevnoStanjeRacunaStandardForm()));
		dnevnoStanjeRacunaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Clock-icon.png")));
		bankaMI = new JMenuItem(new DialogAction("Banka", KeyEvent.VK_B, new BankaStandardForm()));
		bankaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Bank-icon.png")));
		
		
		kursnaListaMI = new JMenuItem(new DialogAction("Kursna lista", KeyEvent.VK_K, new KursnaListaStandardForm()));
		kursnaListaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Analytics-2-icon.png")));
		valuteMI = new JMenuItem(new DialogAction("Valute", KeyEvent.VK_V, new ValuteStandardForm()));
		valuteMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Dollar-icon.png")));
		kursUValutiMI = new JMenuItem(new DialogAction("Kurs u valuti", KeyEvent.VK_U, new KursUValutiStandardForm()));
		kursUValutiMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Calculator-icon.png")));
		analitikaIzvodaMI = new JMenuItem(new DialogAction("Analitika izvoda", KeyEvent.VK_A, new AnalitikaIzvodaStandardForm()));
		analitikaIzvodaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Files-icon.png")));
		ukidanjeMI = new JMenuItem(new DialogAction("Ukidanje", KeyEvent.VK_E, new UkidanjeStandardForm()));
		ukidanjeMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/ukidanje.png")));

		orgSemaMenu.add(drzaveMI);
		orgSemaMenu.add(mestoMI);
		orgSemaMenu.addSeparator();
		orgSemaMenu.add(dnevnoStanjeRacunaMI);
		orgSemaMenu.add(analitikaIzvodaMI);
		orgSemaMenu.addSeparator();

		pravnaLicaMI = new JMenuItem(new DialogAction("Pravna lica", KeyEvent.VK_P, new PravnoLiceStandardForm()));
		pravnaLicaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Briefcase-icon.png")));
		fizickaLicaMI = new JMenuItem(new DialogAction("Fizicka lica", KeyEvent.VK_F, new FizickoLiceStandardForm()));
		fizickaLicaMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/man-1-icon.png")));
		racuniMI = new JMenuItem(new DialogAction("Racuni", KeyEvent.VK_R, new RacunStandardForm()));
		racuniMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Letterhead-icon.png")));
		
		korisniciMenu.add(fizickaLicaMI);
		korisniciMenu.add(pravnaLicaMI);
		
		orgSemaMenu.add(korisniciMenu);
		orgSemaMenu.addSeparator();
		orgSemaMenu.add(bankaMI);
		orgSemaMenu.add(racuniMI);
		orgSemaMenu.add(valuteMI);
		orgSemaMenu.add(kursnaListaMI);
		orgSemaMenu.add(kursUValutiMI);
		orgSemaMenu.add(ukidanjeMI);
		
		menuBar.add(orgSemaMenu);
		
		JMenu pomocMenu = new JMenu("Pomoć");
		pomocMenu.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Problem-info-2-icon.png")));
		pomocMenu.setMnemonic(KeyEvent.VK_P);
		JMenuItem pomocMI = new JMenuItem(new HelpAction());
		pomocMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/question-faq-icon (1).png")));
		pomocMI.setMnemonic(KeyEvent.VK_P);
		JMenuItem autoriMI = new JMenuItem(new AutoriAction());
		autoriMI.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Degree-icon.png")));
		autoriMI.setMnemonic(KeyEvent.VK_U);
		pomocMenu.add(pomocMI);
		pomocMenu.add(autoriMI);
		menuBar.add(pomocMenu);
	}

	public static MainFrame getInstance(){
		if (instance==null)
			instance=new MainFrame();
		return instance;

	}
}