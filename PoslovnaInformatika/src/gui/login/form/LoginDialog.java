package gui.login.form;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import gui.main.form.MainFrame;

public class LoginDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private HashMap<String, String> admins;
	private JTextField txtUser;
	private JPasswordField txtPass;
	private JLabel message;
	
	public LoginDialog(JFrame parent) {
		super(parent, "Prijava" , true);
		setSize(400,180);
		setLocationRelativeTo(null);
		setResizable(false);
		setUndecorated(true);
		
		initAdmins();
		
		JPanel pan = new JPanel();
		Border blackline = BorderFactory.createLineBorder(Color.black);
		TitledBorder titleBorder = BorderFactory.createTitledBorder(blackline, "Prijava");
		titleBorder.setTitleJustification(TitledBorder.CENTER);
		pan.setBorder(titleBorder);
		
		JPanel panInputs = createPanelInputs();
		JPanel panMessage = createPanelMessage();
		JPanel panButtons = createPanelButtons();
		
		pan.add(panInputs, BorderLayout.NORTH);
		pan.add(panMessage, BorderLayout.CENTER);
		pan.add(panButtons, BorderLayout.SOUTH);
		
		add(pan);
	}
	
	private JPanel createPanelButtons() {
		JPanel panButton = new JPanel();
		
		Dimension dim = new Dimension(130, 30);
		
		JButton btnOk = new JButton("Prijava");
		btnOk.setPreferredSize(dim);
		btnOk.setDefaultCapable(true);
		btnOk.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/like.png")));
		getRootPane().setDefaultButton(btnOk);
		
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//String pass = new String(panel.getTxtPass().getPassword());
				
				String username = null;
				
				for (String user : admins.keySet()) {
					if(user.equals(txtUser.getText().trim()))
					{
						username = user;
						break;
					}
				}
				
				if(username == null)
				{
					message.setText("Nepostojeće korisničko ime");
					message.setVisible(true);
					return;
				}
				
				String passFromTxt = new String(txtPass.getPassword());
				
				if(!admins.get(username).equals(passFromTxt))
				{
					message.setText("Pogrešna lozinka");
					message.setVisible(true);
					return;
				}
				
				LoginDialog.this.dispose();
			}
		});
		
		JButton btnCancel= new JButton("Odustajanje");
		btnCancel.setPreferredSize(dim);
		btnCancel.setIcon(new ImageIcon(MainFrame.class.getResource("/img/ikonice/Unlike.png")));
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainFrame.getInstance().dispose();
				MainFrame.getInstance().setVisible(false);
				System.exit(0);
			}
		});
				
		panButton.add(btnOk);
		panButton.add(btnCancel);
		
		return panButton;
	}

	private JPanel createPanelMessage() {
		JPanel panMessage = new JPanel();
		panMessage.setPreferredSize(new Dimension(380, 30));
		message = new JLabel("Ovde ide poruka greske");
		message.setForeground(Color.RED);
		message.setVisible(false);
		panMessage.add(message);
		return panMessage;
	}

	private JPanel createPanelInputs() {
		JPanel panInputs = new JPanel();
		Dimension dim = new Dimension(150,20);
		panInputs.setLayout(new GridLayout(2, 1));
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		
		JLabel lblUser = new JLabel("Korisničko ime:");
		lblUser.setPreferredSize(dim);
		
		txtUser=new JTextField();
		txtUser.setPreferredSize(dim);
  	
		p1.add(lblUser);
		p1.add(txtUser);
		
		JLabel lblPass = new JLabel("Lozinka:");
		lblPass.setPreferredSize(dim);
  	
		txtPass=new JPasswordField();
		txtPass.setPreferredSize(dim);
		
		p2.add(lblPass);
		p2.add(txtPass);
		
		panInputs.add(p1);
		panInputs.add(p2);
		
		return panInputs;
	}

	private void initAdmins() {
		admins = new HashMap<String, String>();
		try {
	    	FileReader fr = new FileReader("admins.properties");
	    	BufferedReader br = new BufferedReader(fr);
	        String line = br.readLine();
	        StringTokenizer st;

	        while (line != null) {
	        	st = new StringTokenizer(line, ":");
	        	admins.put(st.nextToken(),st.nextToken());
	        	line = br.readLine();
	        }
	        br.close();
	    } catch (FileNotFoundException e) {
	    	System.out.println("File not found");
	    } catch (IOException e) {
	    	System.out.println("IOException");
	    }
	}
}
