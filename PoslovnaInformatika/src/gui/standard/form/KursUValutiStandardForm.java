package gui.standard.form;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.KursUValutiTableModel;
import net.miginfocom.swing.MigLayout;

public class KursUValutiStandardForm extends AbstractStandardForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField tfRedniBroj;
	private JTextField tfKupovni;
	private JTextField tfSrednji;
	private JTextField tfProdajni;
	private JTextField tfOsnovnaValuta;
	private JTextField tfPremaValuti;
	private JTextField tfUListi;
	
	private JButton btnOsnovnaLookup;
	private JButton btnPremaLookup;
	private JButton btnUListiLookup;

	public KursUValutiStandardForm() {
		super("Kurs u valuti", new KursUValutiTableModel(new String[]{"Redni broj", "Kupovni", "Srednji", "Prodajni", "Osnovna valuta", "Prema valuti", "U listi"}, 0));
		initGui();
		btnNextForm.setEnabled(false);
		btnPickup.setEnabled(false);
	}	
	
	@Override
	protected void initTextFields() {
		tfRedniBroj = new JTextField(10);
		tfRedniBroj.addKeyListener(new TextFieldTextValidator(9, tfRedniBroj, TextFieldTextValidator.CharTypes.Whatever));
		tfKupovni = new JTextField(15);
		tfKupovni.addKeyListener(new TextFieldTextValidator(14, tfKupovni, TextFieldTextValidator.CharTypes.Decimal));
		tfSrednji = new JTextField(15);
		tfSrednji.addKeyListener(new TextFieldTextValidator(14, tfSrednji, TextFieldTextValidator.CharTypes.Decimal));
		tfProdajni = new JTextField(15);
		tfProdajni.addKeyListener(new TextFieldTextValidator(14, tfProdajni, TextFieldTextValidator.CharTypes.Decimal));
		tfOsnovnaValuta = new JTextField(5);
		tfOsnovnaValuta.addKeyListener(new TextFieldTextValidator(4, tfOsnovnaValuta, TextFieldTextValidator.CharTypes.Whatever));
		tfPremaValuti = new JTextField(5);
		tfPremaValuti.addKeyListener(new TextFieldTextValidator(4, tfPremaValuti, TextFieldTextValidator.CharTypes.Whatever));
		tfUListi = new JTextField(5);
		tfUListi.addKeyListener(new TextFieldTextValidator(4, tfUListi, TextFieldTextValidator.CharTypes.Whatever));
		
		btnOsnovnaLookup = new JButton(new ZoomFormAction(this, "OsnovnaLookup"));
		btnPremaLookup = new JButton(new ZoomFormAction(this, "PremaLookup"));
		btnUListiLookup = new JButton(new ZoomFormAction(this, "ListaLookup"));
	}

	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		JPanel dataPanel = new JPanel();
		JPanel rightPanel = new JPanel();
		
		bottomPanel.setLayout(new MigLayout("fillx"));
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		rightPanel.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();
		
		btnOsnovnaLookup.setMaximumSize(new Dimension(70, 20));
		btnPremaLookup.setMaximumSize(new Dimension(70, 20));
		btnUListiLookup.setMaximumSize(new Dimension(70, 20));
		
		JLabel lblRedniBroj = new JLabel("Redni broj:");
		JLabel lblKupovni = new JLabel("Kupovni:");
		JLabel lblSrednji = new JLabel("Srednji:");
		JLabel lblProdajni = new JLabel("Prodajni:");
		
		JLabel lblOsnovnaValuta = new JLabel("Osnovna valuta:");
		JLabel lblPremaValuti = new JLabel("Prema valuti:");
		JLabel lblKursnaLista = new JLabel("Kursna lista:");

		dataPanel.add(lblRedniBroj);
		dataPanel.add(tfRedniBroj, "wrap");
		dataPanel.add(lblKupovni);
		dataPanel.add(tfKupovni, "wrap");
		dataPanel.add(lblSrednji);
		dataPanel.add(tfSrednji, "wrap");
		dataPanel.add(lblProdajni);
		dataPanel.add(tfProdajni, "wrap");
		
		rightPanel.add(lblOsnovnaValuta);
		rightPanel.add(tfOsnovnaValuta);
		rightPanel.add(btnOsnovnaLookup, "wrap");
		
		rightPanel.add(lblPremaValuti);
		rightPanel.add(tfPremaValuti);
		rightPanel.add(btnPremaLookup, "wrap");
		
		rightPanel.add(lblKursnaLista);
		rightPanel.add(tfUListi);
		rightPanel.add(btnUListiLookup, "wrap");
		
		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(rightPanel, "dock west, wrap");
		
		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			ClearTextFields(false);
		} else {
			tfRedniBroj.setText((String) tblModel.getValueAt(index, 0));
			tfKupovni.setText((String) tblModel.getValueAt(index, 1));
			tfSrednji.setText((String) tblModel.getValueAt(index, 2));
			tfProdajni.setText((String) tblModel.getValueAt(index, 3));
			tfOsnovnaValuta.setText((String) tblModel.getValueAt(index, 4));
			tfPremaValuti.setText((String) tblModel.getValueAt(index, 5));
			tfUListi.setText((String) tblModel.getValueAt(index, 6));
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed( new String[] {tfRedniBroj.getText(), tfKupovni.getText(), tfSrednji.getText(), tfProdajni.getText(), tfOsnovnaValuta.getText(), tfPremaValuti.getText(), tfUListi.getText()});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			 
			tblModel.izmeniRed(index, new String[] {tfRedniBroj.getText(), tfKupovni.getText(), tfSrednji.getText(), tfProdajni.getText(), tfOsnovnaValuta.getText(), tfPremaValuti.getText(), tfUListi.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		String redniBroj = tfRedniBroj.getText();
		String kupovni = tfKupovni.getText();
		String srednji = tfSrednji.getText();
		String prodajni = tfProdajni.getText();
		String osnovnaValuta = tfOsnovnaValuta.getText();
		String premaValuti = tfPremaValuti.getText();
		String lista = tfUListi.getText();
		
		if(redniBroj.equals("") && kupovni.equals("") && srednji.equals("") && prodajni.equals("") && osnovnaValuta.equals("") && premaValuti.equals("") && lista.equals(""))
			return;
		
		try {
			tblModel.Pretraga( new String[] {redniBroj, kupovni, srednji, /*prodajni,*/ osnovnaValuta, premaValuti, lista});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfRedniBroj.setText("");
		tfKupovni.setText("");
		tfSrednji.setText("");
		tfProdajni.setText("");
		tfOsnovnaValuta.setText("");
		tfPremaValuti.setText("");
		tfUListi.setText("");
		
		if(setFocus)
			tfRedniBroj.requestFocus();
		
	}

	@Override
	public void zoomForm(String btnName) {
		AbstractStandardForm zoomForm;
		if(btnName.equals("OsnovnaLookup") || btnName.equals("PremaLookup"))
		{
			zoomForm = new ValuteStandardForm();
		}
		else
		{
			zoomForm = new KursnaListaStandardForm();
		}
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		if(btnName.equals("OsnovnaLookup"))
		{
			tfOsnovnaValuta.setText(((ValuteStandardForm)zoomForm).getTfID().getText());
		}
		else if(btnName.equals("PremaLookup"))
		{
			tfPremaValuti.setText(((ValuteStandardForm)zoomForm).getTfID().getText());
		}
		else
		{
			tfUListi.setText(((KursnaListaStandardForm)zoomForm).getTfID().getText());
		}
	}

	@Override
	protected void textFieldsEditable(boolean editable) {
			tfRedniBroj.setEditable(editable);
			tfKupovni.setEditable(editable);
			tfSrednji.setEditable(editable);
			tfProdajni.setEditable(editable);
			tfOsnovnaValuta.setEditable(editable);
			tfPremaValuti.setEditable(editable);
			tfUListi.setEditable(editable);
			
			btnOsnovnaLookup.setEnabled(editable);
			btnPremaLookup.setEnabled(editable);
			btnUListiLookup.setEnabled(editable);
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfRedniBroj.setEditable(false);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		
		if(tfRedniBroj.getText().trim().equals("")) errorMessage += "Redni broj je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT kuruv_redni_br FROM kurs_u_valuti WHERE kuruv_redni_br = '" + tfRedniBroj.getText().trim() + "'");
				if (rset.next()) errorMessage += "Kurs u valuti sa rednim brojem " + tfRedniBroj.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfKupovni.getText().trim().equals("")) errorMessage += "Kupovni kurs je obavezno polje.\n";
		else if(!TextFieldTextValidator.validateDecimalInput(tfKupovni.getText(), 9, 4)) errorMessage += "Kupovni kurs je nevalidnog formata.\n";
		if(tfSrednji.getText().trim().equals("")) errorMessage += "Srednji kurs je obavezno polje.\n";
		else if(!TextFieldTextValidator.validateDecimalInput(tfSrednji.getText(), 9, 4)) errorMessage += "Srednji kurs je nevalidnog formata.\n";
		if(tfProdajni.getText().trim().equals("")) errorMessage += "Prodajni kurs je obavezno polje.\n";
		else if(!TextFieldTextValidator.validateDecimalInput(tfProdajni.getText(), 9, 4)) errorMessage += "Prodajni kurs je nevalidnog formata.\n";
		
		if(tfOsnovnaValuta.getText().trim().equals("")) errorMessage += "Osnovna valuta je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT val_id FROM valute WHERE val_id = '" + tfOsnovnaValuta.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Valuta sa ID " + tfOsnovnaValuta.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfPremaValuti.getText().trim().equals("")) errorMessage += "Prema valuti je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT val_id FROM valute WHERE val_id = '" + tfPremaValuti.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Valuta sa ID " + tfPremaValuti.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfUListi.getText().trim().equals("")) errorMessage += "Kursna lista je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT kurl_id FROM kursna_lista WHERE kurl_id = '" + tfUListi.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Kursna lista sa ID " + tfUListi.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return errorMessage;
	}
}
