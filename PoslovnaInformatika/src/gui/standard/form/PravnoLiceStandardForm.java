package gui.standard.form;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.PravnaLicaTableModel;
import net.miginfocom.swing.MigLayout;

public class PravnoLiceStandardForm extends AbstractStandardForm {

	private static final long serialVersionUID = -7615111956452678701L;

	private JTextField tfPIB;
	private JTextField tfNaziv;
	private JTextField tfAdresa;
	private JTextField tfSifraMesta;
	private JTextField tfNazivMesta;
	private JTextField tfEmail;
	private JTextField tfTelefon;
	private JTextField tfWeb;
	private JTextField tfFax;
	
	
	public PravnoLiceStandardForm() {
		super("Pravna lica", new PravnaLicaTableModel(new String[]{"PIB", "Naziv", "Adresa", "Sifra mesta", "Email", "Telefon", "Web sajt", "Fax"}, 0));
		
		nextForms.put("Racuni", new RacunStandardForm());
		where = " WHERE k_id=";
		
		initGui();
		setupTableLook();
		
	}
	
	@Override
	protected void initTextFields() {
		tfPIB = new JTextField(8);
		tfNaziv = new JTextField(20);
		tfAdresa = new JTextField(20);
		tfSifraMesta = new JTextField(8);
		tfNazivMesta = new JTextField(20);
		tfEmail = new JTextField(20);
		tfTelefon = new JTextField(10);
		tfWeb = new JTextField(20);
		tfFax = new JTextField(10);
		
		btnLookup = new JButton(new ZoomFormAction(this, null));
		
		tfPIB.addKeyListener(new TextFieldTextValidator(10, tfPIB, TextFieldTextValidator.CharTypes.Numbers));
		tfNaziv.addKeyListener(new TextFieldTextValidator(50, tfNaziv, TextFieldTextValidator.CharTypes.Letters));
		tfAdresa.addKeyListener(new TextFieldTextValidator(40, tfAdresa, TextFieldTextValidator.CharTypes.Whatever));
		tfSifraMesta.addKeyListener(new TextFieldTextValidator(3, tfSifraMesta, TextFieldTextValidator.CharTypes.Letters));
		tfNazivMesta.addKeyListener(new TextFieldTextValidator(60, tfNazivMesta, TextFieldTextValidator.CharTypes.Numbers));
		tfEmail.addKeyListener(new TextFieldTextValidator(50, tfEmail, TextFieldTextValidator.CharTypes.Whatever));
		tfTelefon.addKeyListener(new TextFieldTextValidator(15, tfTelefon, TextFieldTextValidator.CharTypes.Numbers));
		tfWeb.addKeyListener(new TextFieldTextValidator(100, tfWeb, TextFieldTextValidator.CharTypes.Whatever));
		tfFax.addKeyListener(new TextFieldTextValidator(20, tfFax, TextFieldTextValidator.CharTypes.Numbers));
		
	}
	
	private void setupTableLook(){
		tblGrid.removeColumn(tblGrid.getColumn("Telefon"));
		tblGrid.removeColumn(tblGrid.getColumn("Web sajt"));
		tblGrid.removeColumn(tblGrid.getColumn("Fax"));
		
		tblGrid.getColumn("PIB").setMaxWidth(100);
		tblGrid.getColumn("PIB").setMinWidth(100);
		tblGrid.getColumn("Sifra mesta").setMaxWidth(100);
		tblGrid.getColumn("Sifra mesta").setMinWidth(100);
	}
	
	@Override
	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelOptional = new JPanel();
		
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelOptional.setLayout(new MigLayout("gapx 15px"));
		
		JPanel buttonsPanel = new JPanel();
		
		JLabel lblPIB = new JLabel("PIB:");
		JLabel lblNaziv = new JLabel("Naziv:");
		JLabel lblAdresa = new JLabel("Adresa:");
		JLabel lblMesto = new JLabel("Mesto");
		JLabel lblSifraMesta = new JLabel("Sifra mesta:");
		JLabel lblNazivMesta = new JLabel("Naziv mesta:");
		JLabel lblEmail = new JLabel("Email:");
		JLabel lblTelefon = new JLabel("Telefon:");
		JLabel lblWeb = new JLabel("Web sajt:");
		JLabel lblFax = new JLabel("Fax:");
		
		dataPanel.add(lblPIB);
		dataPanel.add(tfPIB, "wrap");
		
		dataPanel.add(lblNaziv);
		dataPanel.add(tfNaziv, "wrap");
		
		dataPanel.add(lblAdresa);
		dataPanel.add(tfAdresa, "wrap");
		
		
		dataPanelOptional.add(lblTelefon);
		dataPanelOptional.add(tfTelefon, "wrap");
		
		dataPanelOptional.add(lblFax);
		dataPanelOptional.add(tfFax, "wrap");
		
		dataPanelOptional.add(lblEmail);
		dataPanelOptional.add(tfEmail, "wrap");
		
		dataPanelOptional.add(lblWeb);
		dataPanelOptional.add(tfWeb, "wrap");
		
		
		dataPanel.add(lblMesto);
		dataPanel.add(btnLookup, "wrap");
		
		dataPanel.add(lblSifraMesta);
		dataPanel.add(tfSifraMesta, "wrap");
		
		dataPanel.add(lblNazivMesta);
		dataPanel.add(tfNazivMesta, "wrap");
		
		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelOptional, "dock west, wrap");

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");

	}
	
	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfPIB.setText("");
		tfNaziv.setText("");
		tfAdresa.setText("");
		tfSifraMesta.setText("");
		tfNazivMesta.setText("");
		tfEmail.setText("");
		tfTelefon.setText("");
		tfWeb.setText("");
		tfFax.setText("");
		
		if(setFocus)
			tfPIB.requestFocus();

	}
	
	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed(new String[] { tfPIB.getText(), tfNaziv.getText(), tfAdresa.getText(), tfSifraMesta.getText(), 
					tfEmail.getText(), tfTelefon.getText(), tfWeb.getText(), tfFax.getText() });
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

	}
	
	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			tblModel.izmeniRed(index, new String[] { tfPIB.getText(), tfNaziv.getText(), tfAdresa.getText(), tfSifraMesta.getText(), 
					tfEmail.getText(), tfTelefon.getText(), tfWeb.getText(), tfFax.getText() });
			
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void Pretraga() {
			
		try {
			tblModel.Pretraga(new String[] { tfPIB.getText(), tfNaziv.getText(), tfAdresa.getText(), tfSifraMesta.getText(), 
					tfNazivMesta.getText(), tfEmail.getText(), tfTelefon.getText(), tfWeb.getText(), tfFax.getText() });
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) 
			ClearTextFields(false);
		else {
			tfPIB.setText((String) tblModel.getValueAt(index, tblModel.findColumn("PIB")));
			tfNaziv.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Naziv")));
			tfAdresa.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Adresa")));
			tfSifraMesta.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Sifra mesta")));
			tfEmail.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Email")));
			tfTelefon.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Telefon")));
			tfWeb.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Web sajt")));
			tfFax.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Fax")));
		}
	}

	@Override
	public void zoomForm(String btnName) {
		NaseljenoMestoStandardForm zoomForm = new NaseljenoMestoStandardForm();
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		tfSifraMesta.setText(zoomForm.getTfSifraMesta().getText());
		tfNazivMesta.setText(zoomForm.getTfNazivMesta().getText());

	}

	@Override
	protected void textFieldsEditable(boolean editable) {
			tfPIB.setEditable(editable);
			tfNaziv.setEditable(editable);
			tfAdresa.setEditable(editable);
			tfSifraMesta.setEditable(editable);
			tfNazivMesta.setEditable(editable);
			tfEmail.setEditable(editable);
			tfTelefon.setEditable(editable);
			tfWeb.setEditable(editable);
			tfFax.setEditable(editable);
			
			btnLookup.setEnabled(editable);
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfPIB.setEditable(false);
		
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		
		if(tfPIB.getText().trim().equals("")) 
			errorMessage += "PIB je obavezno polje.\n";
		else if(tfPIB.getText().length() < 10){
			errorMessage += "PIB nije validan, mora imati 10 brojeva.\n";
		}
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT pl_pib FROM korisnik_pl WHERE pl_pib = '" + tfPIB.getText().trim() + "'");
				if (rset.next()) errorMessage += "Korisnik ciji je pib " + tfPIB.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfSifraMesta.getText().trim().equals("")) 
			errorMessage += "Sifra mesta je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT nm_sifra FROM naseljeno_mesto WHERE nm_sifra = '" + tfSifraMesta.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Naseljeno mesto sa šifrom " + tfSifraMesta.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfNaziv.getText().trim().equals("")) errorMessage += "Naziv korisnika je obavezno polje.\n";
		if(tfAdresa.getText().trim().equals("")) errorMessage += "Adresa korisnika je obavezno polje.\n";
		
		return errorMessage;
	}

	public JTextField getTfPIB() {
		return tfPIB;
	}

	public JTextField getTfNaziv() {
		return tfNaziv;
	}
	
}
