package gui.standard.form;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import gui.ukidanjeRacuna.form.UkidanjeRacunaForm;
import listeners.TextFieldTextValidator;
import model.tables.RacunTableModel;
import net.miginfocom.swing.MigLayout;
import utility.DateLabelFormatter;

public class RacunStandardForm extends AbstractStandardForm {

	private static final long serialVersionUID = -4263655502832750355L;
	
	private JTextField tfID;
	private JTextField tfBrojRacuna;
	
	private SqlDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	
	private ButtonGroup bgVazeci;
	private JRadioButton rbVazeciDa;
	private JRadioButton rbVazeciNe;
	
	private JTextField tfBankaID;
	private JTextField tfBankaNaziv;
	
	private JTextField tfKorisnikID;
	private JTextField tfKorisnikNaziv;
	
	private JTextField tfValutaID;
	private JTextField tfValutaNaziv;
	
	private JButton btnLookupKorisnik;
	private JButton btnLookupValuta;
	
	public RacunStandardForm() {
		super("Racuni", new RacunTableModel(new String[]{"ID", "Broj racuna", "Datum otvaranja", "Vazeci", "Banka ID", "Korisnik ID", "Valuta ID"}, 0));
		
		nextForms.put("Dnevno stanje", new DnevnoStanjeRacunaStandardForm());
		where = " WHERE r_id=";
		
		initGui();
		setupTableLook();
		
	}
	
	private void setupTableLook(){
		tblGrid.getColumn("ID").setMaxWidth(70);
		tblGrid.getColumn("ID").setMinWidth(70);
		tblGrid.getColumn("Vazeci").setMaxWidth(70);
		tblGrid.getColumn("Vazeci").setMinWidth(70);
		tblGrid.getColumn("Valuta ID").setMaxWidth(70);
		tblGrid.getColumn("Valuta ID").setMinWidth(70);
		tblGrid.getColumn("Banka ID").setMaxWidth(70);
		tblGrid.getColumn("Banka ID").setMinWidth(70);
	}

	@Override
	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelOptional = new JPanel();
		
		dataPanel.setLayout(new MigLayout("", "[][][][][][]", "[][][][][][]"));
		dataPanelOptional.setLayout(new MigLayout("gapx 15px"));
		
		JPanel buttonsPanel = new JPanel();
		btnLookup.setName("LookupBanka");
		
		btnLookupValuta.setText("...");
		btnLookupValuta.setName("LookupValuta");
		
		
		btnLookupKorisnik.setText("...");
		// Menu odabir vrste korisnika za lookup
		final JPopupMenu korisnikMenu = new JPopupMenu();
		JMenuItem fizickoLice = new JMenuItem(new ZoomFormAction(this, "LookupFizickoLice"));
		fizickoLice.setText("Fizicko lice");
		JMenuItem pravnoLice = new JMenuItem(new ZoomFormAction(this, "LookupPravnoLice"));
		pravnoLice.setText("Pravno lice");
		korisnikMenu.add(fizickoLice);
		korisnikMenu.add(pravnoLice);
		
		btnLookupKorisnik.addMouseListener(new MouseAdapter() {
	        public void mouseReleased(MouseEvent e){
	            if ( e.getButton() == 1 ){ // 1-left, 2-middle, 3-right button
	            	korisnikMenu.show(e.getComponent(), e.getX(), e.getY());
	            }
	        }
	    });
		
		
		JLabel lblID = new JLabel("ID:");
		JLabel lblBrojRacuna = new JLabel("Broj racuna:");
		JLabel lblDatumOtvaranja = new JLabel("Datum otvaranja: ");
		JLabel lblVazeci = new JLabel("Vazeci:");
		
		JLabel lblBanka = new JLabel("Banka ");
		JLabel lblBankaSifra = new JLabel("Sifra banke: ");
		JLabel lblBankaNaziv = new JLabel("Naziv banke: ");
		
		JLabel lblKorisnik = new JLabel("Korisnik ");
		JLabel lblKorisnikJmbg = new JLabel("ID korisnika: ");
		JLabel lblKorisnikNaziv = new JLabel("Naziv korisnika: ");
		
		JLabel lblValuta = new JLabel("Valuta ");
		JLabel lblValutaID = new JLabel("ID valute: ");
		JLabel lblValutaNaziv = new JLabel("Naziv valute: ");
		
		
		dataPanel.add(lblID, "cell 0 0");
		dataPanel.add(tfID, "cell 1 0");
		
		dataPanel.add(lblBrojRacuna, "cell 0 1");
		dataPanel.add(tfBrojRacuna, "cell 1 1");
		
		dataPanel.add(lblDatumOtvaranja, "cell 0 2");
		dataPanel.add(datePicker, "cell 1 2");
		
		dataPanel.add(lblVazeci, "cell 0 3");
		dataPanel.add(rbVazeciDa, "cell 1 3, alignx left");
		dataPanel.add(rbVazeciNe, "cell 1 3, alignx right");
		
		dataPanel.add(lblBanka, "cell 0 4");
		dataPanel.add(btnLookup, "cell 1 4");
		
		dataPanel.add(lblBankaSifra, "cell 0 5");
		dataPanel.add(tfBankaID, "cell 1 5");
		
		dataPanel.add(lblBankaNaziv, "cell 0 6");
		dataPanel.add(tfBankaNaziv, "cell 1 6");
		
		dataPanel.add(lblKorisnik, "cell 4 0");
		dataPanel.add(btnLookupKorisnik, "cell 5 0");
		
		dataPanel.add(lblKorisnikJmbg, "cell 4 1");
		dataPanel.add(tfKorisnikID, "cell 5 1");
		
		dataPanel.add(lblKorisnikNaziv, "cell 4 2");
		dataPanel.add(tfKorisnikNaziv, "cell 5 2");
		
		dataPanel.add(lblValuta, "cell 4 3");
		dataPanel.add(btnLookupValuta, "cell 5 3");
		
		dataPanel.add(lblValutaID, "cell 4 4");
		dataPanel.add(tfValutaID, "cell 5 4");
		
		dataPanel.add(lblValutaNaziv, "cell 4 5");
		dataPanel.add(tfValutaNaziv, "cell 5 5");
		
		
		bottomPanel.add(dataPanel, "dock west");

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");

	}

	@Override
	protected void initTextFields() {
		tfID = new JTextField(5);
		tfBrojRacuna = new JTextField(20);
		
		model = new SqlDateModel();
		model.setSelected(true);
		model.setValue(null);
		datePanel = new JDatePanelImpl(model, new Properties());
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setShowYearButtons(true);
		btnLookup = new JButton(new ZoomFormAction(this, "LookupBanka"));
		btnLookupKorisnik = new JButton();
		btnLookupValuta = new JButton();
		btnLookupValuta = new JButton(new ZoomFormAction(this, "LookupValuta"));
		
		rbVazeciDa = new JRadioButton("Da");
		rbVazeciNe = new JRadioButton("Ne");
		bgVazeci = new ButtonGroup();
		bgVazeci.add(rbVazeciDa);
		bgVazeci.add(rbVazeciNe);
		
		tfBankaID = new JTextField(10);
		tfBankaNaziv = new JTextField(20);
		
		tfKorisnikID = new JTextField(10);
		tfKorisnikNaziv = new JTextField(20);
		
		tfValutaID = new JTextField(5);
		tfValutaNaziv = new JTextField(20);
		
		tfID.addKeyListener(new TextFieldTextValidator(5, tfID, TextFieldTextValidator.CharTypes.Whatever));
		tfBrojRacuna.addKeyListener(new TextFieldTextValidator(18, tfBrojRacuna, TextFieldTextValidator.CharTypes.Numbers));
		tfBankaID.addKeyListener(new TextFieldTextValidator(4, tfBankaID, TextFieldTextValidator.CharTypes.Letters));
		tfBankaNaziv.addKeyListener(new TextFieldTextValidator(120, tfBankaNaziv, TextFieldTextValidator.CharTypes.Letters));
		tfKorisnikID.addKeyListener(new TextFieldTextValidator(13, tfKorisnikID, TextFieldTextValidator.CharTypes.Numbers));
		tfKorisnikNaziv.addKeyListener(new TextFieldTextValidator(50, tfKorisnikNaziv, TextFieldTextValidator.CharTypes.Letters));
		tfValutaID.addKeyListener(new TextFieldTextValidator(4, tfValutaID, TextFieldTextValidator.CharTypes.Numbers));
		tfValutaNaziv.addKeyListener(new TextFieldTextValidator(30, tfValutaNaziv, TextFieldTextValidator.CharTypes.Letters));
		
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) 
			ClearTextFields(false);
		else {
			tfID.setText((String) tblModel.getValueAt(index, tblModel.findColumn("ID")));
			tfBrojRacuna.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Broj racuna")));
			tfBankaID.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Banka ID")));
			tfKorisnikID.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Korisnik ID")));
			tfValutaID.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Valuta ID")));
			
			if(tblModel.getValueAt(index, tblModel.findColumn("Vazeci")).equals("Ne"))
				rbVazeciNe.setSelected(true);
			else
				rbVazeciDa.setSelected(true);
			
			java.util.Date utilDate = null;
			try {
				utilDate = new SimpleDateFormat("yyyy-MM-dd").parse((String)tblModel.getValueAt(index, tblModel.findColumn("Datum otvaranja")));
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
			java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
			model.setValue(sqlDate);
			
		}

	}

	@Override
	public void Dodaj() {
		
		try {
			tblModel.dodajRed(new String[] { tfID.getText(), tfBrojRacuna.getText(), datePanel.getModel().getValue().toString(), 
					String.valueOf(rbVazeciDa.isSelected()), tfBankaID.getText(), tfKorisnikID.getText(), tfValutaID.getText() });
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			tblModel.izmeniRed(index, new String[] { tfID.getText(), tfBrojRacuna.getText(), datePanel.getModel().getValue().toString(), 
					String.valueOf(rbVazeciDa.isSelected()), tfBankaID.getText(), tfKorisnikID.getText(), tfValutaID.getText() });
			
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void Pretraga() {
		
		String vazeci = "";
		
		if(rbVazeciDa.isSelected())
			vazeci = "true";
		else if(rbVazeciNe.isSelected())
			vazeci = "false";
		
		try {
			tblModel.Pretraga(new String[] { tfID.getText(), tfBrojRacuna.getText(), String.valueOf(datePanel.getModel().getValue()), 
					vazeci, tfBankaID.getText(), tfKorisnikID.getText(), tfValutaID.getText() });
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void zoomForm(String btnName) {
		if(btnName.equals("LookupBanka")){
			BankaStandardForm zoomForm = new BankaStandardForm();
			zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
			zoomForm.setVisible(true);
			tfBankaID.setText(zoomForm.getTfID().getText());
			tfBankaNaziv.setText(zoomForm.getTfNaziv().getText());
		}
		else if(btnName.equals("LookupFizickoLice")){
			FizickoLiceStandardForm zoomForm = new FizickoLiceStandardForm();
			zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
			zoomForm.setVisible(true);
			tfKorisnikID.setText(zoomForm.getTfJMBG().getText());
			tfKorisnikNaziv.setText(zoomForm.getTfIme().getText() + " " + zoomForm.getTfPrezime().getText());
		}
		else if(btnName.equals("LookupPravnoLice")){
			PravnoLiceStandardForm zoomForm = new PravnoLiceStandardForm ();
			zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
			zoomForm.setVisible(true);
			tfKorisnikID.setText(zoomForm.getTfPIB().getText());
			tfKorisnikNaziv.setText(zoomForm.getTfNaziv().getText());
		}
		else if(btnName.equals("LookupValuta")){
			ValuteStandardForm zoomForm = new ValuteStandardForm();
			zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
			zoomForm.setVisible(true);
			tfValutaID.setText(zoomForm.getTfID().getText());
			tfValutaNaziv.setText(zoomForm.getTfNaziv().getText());
		}

	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfID.setText("");
		tfBrojRacuna.setText("");
		tfBankaID.setText("");
		tfBankaNaziv.setText("");
		tfKorisnikID.setText("");
		tfKorisnikNaziv.setText("");
		tfValutaID.setText("");
		tfValutaNaziv.setText("");
		bgVazeci.clearSelection();
		datePicker.getModel().setValue(null);
		
		if(setFocus)
			tfID.requestFocus();

	}

	@Override
	protected void textFieldsEditable(boolean editable) {
		
		tfID.setEditable(editable);
		tfBrojRacuna.setEditable(editable);
		tfBankaID.setEditable(editable);
		tfBankaNaziv.setEditable(editable);
		tfKorisnikID.setEditable(editable);
		tfKorisnikNaziv.setEditable(editable);
		rbVazeciDa.setEnabled(editable);
		rbVazeciNe.setEnabled(editable);
		datePicker.getComponent(1).setEnabled(editable);
		tfValutaID.setEditable(editable);
		tfValutaNaziv.setEditable(editable);
		btnLookup.setEnabled(editable);
		btnLookupKorisnik.setEnabled(editable);
		btnLookupValuta.setEnabled(editable);

	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfID.setEditable(false);
		
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		if(tfID.getText().trim().equals("")) errorMessage += "ID je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT r_id FROM racun WHERE r_id = '" + tfID.getText().trim() + "'");
				if (rset.next()) errorMessage += "Racun ciji je id " + tfID.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfBankaID.getText().trim().equals("")) 
			errorMessage += "ID banke je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT b_id FROM banka WHERE b_id = '" + tfBankaID.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Banka ciji je ID " + tfBankaID.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfKorisnikID.getText().trim().equals("")) 
			errorMessage += "ID korisnika je obavezno polje.\n";
		else if(tfKorisnikID.getText().length() != 13 && tfKorisnikID.getText().length() != 10){
			errorMessage += "ID korisnika nije validan, mora imati 10 ili 13 brojeva.\n";
		}
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT k_id FROM korisnik WHERE k_id = '" + tfKorisnikID.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Korisnik ciji je ID " + tfKorisnikID.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfValutaID.getText().trim().equals("")) 
			errorMessage += "ID valute je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT val_id FROM valute WHERE val_id = '" + tfValutaID.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Valuta ciji je ID " + tfValutaID.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		PreparedStatement stmnt;
		ResultSet rset = null;
		
		if(mode == MODE_ADD){
			try {
				stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM racun JOIN banka ON racun.b_id = banka.b_id "
						+ "WHERE BANKA.b_id = ISNULL(?, BANKA.b_id) AND r_brracuna = ISNULL(?, r_brracuna)");
				stmnt.setString(1, tfBankaID.getText());
				stmnt.setString(2, tfBrojRacuna.getText());
				rset = stmnt.executeQuery();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		if(tfBrojRacuna.getText().trim().equals("")) 
			errorMessage += "Broj racuna je obavezno polje.\n";
		else if(tfBrojRacuna.getText().length() < 15){
			errorMessage += "Broj racuna nije validan, mora imati bar 15 brojeva.\n";
		} else
			try {
				if(rset != null && rset.next()){
					errorMessage += "Broj racuna nije validan, vec postoji u odabranoj banci.\n";
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		
		if(!rbVazeciDa.isSelected() && !rbVazeciNe.isSelected()) errorMessage += "Vazeci je obavezno polje.\n";
		if(model.getValue() == null) errorMessage += "Datum otvaranja je obavezno polje.\n";
		
		return errorMessage;
	}

	public JTextField getTfID() {
		return tfID;
	}

	public JTextField getTfBrojRacuna() {
		return tfBrojRacuna;
	}
	
	@Override
	public void Search(){
		changeMode(MODE_SEARCH);
		ClearTextFields(true);
		textFieldsEditable(true);
		tfBankaNaziv.setEditable(false);
		tfKorisnikNaziv.setEditable(false);
		tfValutaNaziv.setEditable(false);
		btnCommit.setEnabled(true);
		btnRollback.setEnabled(true);
	}
	
	@Override
	public void Delete() {
		int index = tblGrid.getSelectedRow();
		if(index < 0 || mode != MODE_EDIT){
			return;
		}
		UkidanjeRacunaForm form = new UkidanjeRacunaForm();
		form.setVisible(true);
	}

}
