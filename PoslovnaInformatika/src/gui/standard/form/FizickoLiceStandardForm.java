package gui.standard.form;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.FizickoLiceTableModel;
import net.miginfocom.swing.MigLayout;
import utility.DateLabelFormatter;

public class FizickoLiceStandardForm extends AbstractStandardForm {

	private static final long serialVersionUID = -115164069297960414L;
	
	private JTextField tfJMBG;
	private JTextField tfIme;
	private JTextField tfPrezime;
	private ButtonGroup bgPol;
	private JRadioButton rbPolMuski;
	private JRadioButton rbPolZenski;
	private JTextField tfAdresa;
	
	private SqlDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	
	private JTextField tfSifraMesta;
	private JTextField tfNazivMesta;
	private JTextField tfEmail;
	private JTextField tfTelefon;
	
	public FizickoLiceStandardForm() {
		super("Fizicka lica", new FizickoLiceTableModel(new String[]{"JMBG", "Ime", "Prezime", "Pol", "Datum rodjenja", "Adresa", "Sifra mesta", "Email", "Telefon"}, 0));
		
		nextForms.put("Racuni", new RacunStandardForm());
		where = " WHERE k_id=";
		
		initGui();
		setupTableLook();
	}
	
	private void setupTableLook(){
		tblGrid.removeColumn(tblGrid.getColumn("Telefon"));
		tblGrid.removeColumn(tblGrid.getColumn("Datum rodjenja"));
		tblGrid.removeColumn(tblGrid.getColumn("Email"));
		
		tblGrid.getColumn("JMBG").setMaxWidth(100);
		tblGrid.getColumn("JMBG").setMinWidth(100);
		tblGrid.getColumn("Pol").setMaxWidth(30);
		tblGrid.getColumn("Pol").setMinWidth(30);
		tblGrid.getColumn("Sifra mesta").setMaxWidth(80);
		tblGrid.getColumn("Sifra mesta").setMinWidth(80);
	}

	@Override
	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelOptional = new JPanel();
		
		dataPanel.setLayout(new MigLayout("", "[][][]", "[][][][][][]"));
		dataPanelOptional.setLayout(new MigLayout("gapx 15px"));
		
		JPanel buttonsPanel = new JPanel();
		
		JLabel lblJMBG = new JLabel("JMBG:");
		JLabel lblIme = new JLabel("Ime:");
		JLabel lblPrezime = new JLabel("Prezime:");
		JLabel lblPol = new JLabel("Pol:");
		JLabel lblDatumRodjenja = new JLabel("Datum rodjenja: ");
		JLabel lblAdresa = new JLabel("Adresa:");
		JLabel lblMesto = new JLabel("Mesto:");
		JLabel lblSifraMesta = new JLabel("Sifra mesta:");
		JLabel lblNazivMesta = new JLabel("Naziv mesta:");
		JLabel lblEmail = new JLabel("Email:");
		JLabel lblTelefon = new JLabel("Telefon:");
		
		
		dataPanel.add(lblJMBG, "cell 0 0");
		dataPanel.add(tfJMBG, "cell 1 0");
		
		dataPanel.add(lblIme, "cell 0 1");
		dataPanel.add(tfIme, "cell 1 1");
		
		dataPanel.add(lblPrezime, "cell 0 2");
		dataPanel.add(tfPrezime, "cell 1 2");
		
		dataPanel.add(lblPol, "cell 0 3");
		dataPanel.add(rbPolMuski, "cell 1 3, alignx left");
		dataPanel.add(rbPolZenski, "cell 1 3, alignx right");
		
		dataPanel.add(lblDatumRodjenja, "cell 0 4");
		dataPanel.add(datePicker, "cell 1 4");
		
		dataPanel.add(lblAdresa, "cell 0 5");
		dataPanel.add(tfAdresa, "cell 1 5");
		
		dataPanelOptional.add(lblMesto);
		dataPanelOptional.add(btnLookup, "wrap");
		
		dataPanelOptional.add(lblSifraMesta);
		dataPanelOptional.add(tfSifraMesta, "wrap");
		
		dataPanelOptional.add(lblNazivMesta);
		dataPanelOptional.add(tfNazivMesta, "wrap");
		
		
		dataPanelOptional.add(lblTelefon);
		dataPanelOptional.add(tfTelefon, "wrap");
		
		dataPanelOptional.add(lblEmail);
		dataPanelOptional.add(tfEmail, "wrap");
		
		
		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelOptional, "dock west, wrap");

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");

	}

	@Override
	protected void initTextFields() {
		
		tfJMBG = new JTextField(10);
		tfIme = new JTextField(15);
		tfPrezime = new JTextField(15);
		tfAdresa = new JTextField(15);
		tfSifraMesta = new JTextField(5);
		tfNazivMesta = new JTextField(15);
		tfEmail = new JTextField(15);
		tfTelefon = new JTextField(10);
		
		rbPolMuski = new JRadioButton("Muski");
		rbPolZenski = new JRadioButton("Zenski");
		
		bgPol = new ButtonGroup();
		bgPol.add(rbPolMuski);
		bgPol.add(rbPolZenski);
		
		model = new SqlDateModel();
		model.setSelected(true);
		model.setValue(null);
		datePanel = new JDatePanelImpl(model, new Properties());
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setShowYearButtons(true);
		
		btnLookup = new JButton(new ZoomFormAction(this, null));
		
		tfJMBG.addKeyListener(new TextFieldTextValidator(13, tfJMBG, TextFieldTextValidator.CharTypes.Numbers));
		tfIme.addKeyListener(new TextFieldTextValidator(20, tfIme, TextFieldTextValidator.CharTypes.Letters));
		tfPrezime.addKeyListener(new TextFieldTextValidator(20, tfPrezime, TextFieldTextValidator.CharTypes.Letters));
		tfAdresa.addKeyListener(new TextFieldTextValidator(40, tfAdresa, TextFieldTextValidator.CharTypes.Whatever));
		tfSifraMesta.addKeyListener(new TextFieldTextValidator(3, tfSifraMesta, TextFieldTextValidator.CharTypes.Letters));
		tfNazivMesta.addKeyListener(new TextFieldTextValidator(60, tfNazivMesta, TextFieldTextValidator.CharTypes.Letters));
		tfEmail.addKeyListener(new TextFieldTextValidator(60, tfEmail, TextFieldTextValidator.CharTypes.Whatever));
		tfTelefon.addKeyListener(new TextFieldTextValidator(15, tfTelefon, TextFieldTextValidator.CharTypes.Numbers));
		
	}
	

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) 
			ClearTextFields(false);
		else {
			tfJMBG.setText((String) tblModel.getValueAt(index, tblModel.findColumn("JMBG")));
			tfIme.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Ime")));
			tfPrezime.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Prezime")));
			
			if(tblModel.getValueAt(index, tblModel.findColumn("Pol")).equals("M"))
				rbPolMuski.setSelected(true);
			else
				rbPolZenski.setSelected(true);
			
			java.util.Date utilDate = null;
			try {
				utilDate = new SimpleDateFormat("yyyy-MM-dd").parse((String)tblModel.getValueAt(index, tblModel.findColumn("Datum rodjenja")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
			model.setValue(sqlDate);
			
			tfAdresa.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Adresa")));
			tfSifraMesta.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Sifra mesta")));
			tfEmail.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Email")));
			tfTelefon.setText((String) tblModel.getValueAt(index, tblModel.findColumn("Telefon")));
			
		}
	}

	@Override
	public void Dodaj() {
		String pol = "Z";
		
		if(rbPolMuski.isSelected())
			pol = "M";
		
		try {
			tblModel.dodajRed(new String[] { tfJMBG.getText(), tfIme.getText(), tfPrezime.getText(), pol, datePanel.getModel().getValue().toString(), 
					tfAdresa.getText(), tfSifraMesta.getText(), tfEmail.getText(), tfTelefon.getText() });
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		String pol = "Z";
		if(rbPolMuski.isSelected())
			pol = "M";
		
		 try {
			tblModel.izmeniRed(index, new String[] { tfJMBG.getText(), tfIme.getText(), tfPrezime.getText(), pol, datePanel.getModel().getValue().toString(), 
					tfAdresa.getText(), tfSifraMesta.getText(), tfEmail.getText(), tfTelefon.getText() });
			
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void Pretraga() {
		
		String pol = "";
		
		if(rbPolMuski.isSelected())
			pol = "M";
		else if(rbPolZenski.isSelected())
			pol = "Z";
		
		try {
			tblModel.Pretraga(new String[] { tfJMBG.getText(), tfIme.getText(), tfPrezime.getText(), pol, 
					String.valueOf(datePanel.getModel().getValue()), tfAdresa.getText(), tfSifraMesta.getText(), 
					tfNazivMesta.getText(), tfEmail.getText(), tfTelefon.getText()});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void zoomForm(String btnName) {
		NaseljenoMestoStandardForm zoomForm = new NaseljenoMestoStandardForm();
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		tfSifraMesta.setText(zoomForm.getTfSifraMesta().getText());
		tfNazivMesta.setText(zoomForm.getTfNazivMesta().getText());

	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfJMBG.setText("");
		tfIme.setText("");
		tfPrezime.setText("");
		tfAdresa.setText("");
		tfSifraMesta.setText("");
		tfNazivMesta.setText("");
		tfEmail.setText("");
		tfTelefon.setText("");
		bgPol.clearSelection();
		datePicker.getModel().setValue(null);
		
		if(setFocus)
			tfJMBG.requestFocus();

	}

	@Override
	protected void textFieldsEditable(boolean editable) {
		tfJMBG.setEditable(editable);
		tfIme.setEditable(editable);
		tfPrezime.setEditable(editable);
		tfAdresa.setEditable(editable);
		tfSifraMesta.setEditable(editable);
		tfNazivMesta.setEditable(editable);
		tfEmail.setEditable(editable);
		tfTelefon.setEditable(editable);
		rbPolMuski.setEnabled(editable);
		rbPolZenski.setEnabled(editable);
		
		datePicker.getComponent(1).setEnabled(editable);
		btnLookup.setEnabled(editable);

	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfJMBG.setEditable(false);
		
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		if(tfJMBG.getText().trim().equals("")) 
			errorMessage += "JMBG je obavezno polje.\n";
		else if(tfJMBG.getText().length() < 13){
			errorMessage += "JMBG nije validan, mora imati 13 brojeva.\n";
		}
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT fl_jmbg FROM korisnik_fl WHERE fl_jmbg = '" + tfJMBG.getText().trim() + "'");
				if (rset.next()) errorMessage += "Korisnik ciji je jmbg " + tfJMBG.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfSifraMesta.getText().trim().equals("")) 
			errorMessage += "Sifra mesta je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT nm_sifra FROM naseljeno_mesto WHERE nm_sifra = '" + tfSifraMesta.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Naseljeno mesto sa šifrom " + tfSifraMesta.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(tfIme.getText().trim().equals("")) errorMessage += "Ime korisnika je obavezno polje.\n";
		if(tfPrezime.getText().trim().equals("")) errorMessage += "Prezime korisnika je obavezno polje.\n";
		if(!rbPolMuski.isSelected() && !rbPolZenski.isSelected()) errorMessage += "Pol korisnika je obavezno polje.\n";
		if(tfAdresa.getText().trim().equals("")) errorMessage += "Adresa korisnika je obavezno polje.\n";
		if(model.getValue() == null) errorMessage += "Datum rodjenja korisnika je obavezno polje.\n";
		
		return errorMessage;
	}

	public JTextField getTfJMBG() {
		return tfJMBG;
	}

	public JTextField getTfIme() {
		return tfIme;
	}

	public JTextField getTfPrezime() {
		return tfPrezime;
	}
	
	

}
