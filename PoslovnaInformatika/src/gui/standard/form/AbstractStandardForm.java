package gui.standard.form;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import actions.standard.form.AddAction;
import actions.standard.form.CommitAction;
import actions.standard.form.DeleteAction;
import actions.standard.form.FirstAction;
import actions.standard.form.HelpAction;
import actions.standard.form.LastAction;
import actions.standard.form.NextAction;
import actions.standard.form.NextFormAction;
import actions.standard.form.NextFormItemAction;
import actions.standard.form.PickupAction;
import actions.standard.form.PreviousAction;
import actions.standard.form.RefreshAction;
import actions.standard.form.RollbackAction;
import actions.standard.form.SearchAction;
import gui.main.form.MainFrame;
import model.tables.AbstractTableModel;
import net.miginfocom.swing.MigLayout;

public abstract class AbstractStandardForm extends JDialog {

	private static final long serialVersionUID = 1L;
	
	protected static final int MODE_EDIT = 0;
	protected static final int MODE_ADD = 1;
	protected static final int MODE_SEARCH = 2;
	protected int mode;
	
	protected String title;
	protected Label lblMode;
	protected JToolBar toolbar;
	protected AbstractTableModel tblModel;
	protected JTable tblGrid = new JTable();
	protected JPanel statusBar = new JPanel();
	protected JButton btnCommit,  btnRollback;
	protected JPopupMenu popup;
	protected MouseEvent e;
	
	protected JButton btnAdd, btnDelete, btnFirst, btnLast, btnHelp, btnNext, btnNextForm,
	btnPickup, btnRefresh, btnSearch, btnPrevious, btnLookup;
	protected HashMap<String, AbstractStandardForm> nextForms;
	protected String where;
	
	public AbstractStandardForm(String title, AbstractTableModel tblModel)
	{
		nextForms = new HashMap<String, AbstractStandardForm>();
		this.title = title;
		this.tblModel = tblModel;
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		lblMode = new Label();
		setLayout(new MigLayout("fill"));
		setSize(new Dimension(800, 600));
		setTitle(title);
		setLocationRelativeTo(this.getParent());
		setModal(true);
		
		initTextFields();
		initTable();
		initToolbar();
		initStatusBar();
		
		statusBar.setLayout(new MigLayout("fillx"));
		statusBar.add(lblMode, "dock west");
		statusBar.setBorder(BorderFactory.createEtchedBorder());
		add(statusBar, "dock south, grow");
		
		changeMode(MODE_EDIT);
	}
	
	public void initToolbar()
	{
		toolbar = new JToolBar();
		btnSearch = new JButton(new SearchAction(this));
		toolbar.add(btnSearch);


		btnRefresh = new JButton(new RefreshAction(this));
		toolbar.add(btnRefresh);

		btnPickup = new JButton(new PickupAction(this));
		toolbar.add(btnPickup);


		btnHelp = new JButton(new HelpAction());
		toolbar.add(btnHelp);

		toolbar.addSeparator();

		btnFirst = new JButton(new FirstAction(this));
		toolbar.add(btnFirst);

		btnPrevious = new JButton(new PreviousAction(this));
		toolbar.add(btnPrevious);
		
		popup = new JPopupMenu();
		btnNext = new JButton(new NextAction(this));
		toolbar.add(btnNext);

		btnLast = new JButton(new LastAction(this));
		toolbar.add(btnLast);

		toolbar.addSeparator();


		btnAdd = new JButton(new AddAction(this));
		toolbar.add(btnAdd);

		btnDelete = new JButton(new DeleteAction(this));
		toolbar.add(btnDelete);

		toolbar.addSeparator();

		btnNextForm = new JButton(new NextFormAction(this));
		btnNextForm.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                e = ev;
            }
        });
		toolbar.add(btnNextForm);
		
		toolbar.add(lblMode, "dock east");
		add(toolbar, "dock north, grow");
	}
	
	public void initTable()
	{
		JScrollPane scrollPane  = new JScrollPane(tblGrid);
		tblGrid.setModel(tblModel);
		
		try {
			tblModel.open();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tblGrid.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()){
					return;
				}
				if(tblGrid.getSelectedRow()<0)
				{
					btnCommit.setEnabled(false);
					btnRollback.setEnabled(false);	
				}
				else
				{
					btnCommit.setEnabled(true);
					btnRollback.setEnabled(true);
				}
				ClearTextFields(false);
				sync();
				textFieldsEditable(true);
				DisableAllUnEditableFields();
			}
		});;
		
		tblGrid.setRowSelectionAllowed(true);
		tblGrid.setColumnSelectionAllowed(false);
		tblGrid.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		add(scrollPane, "grow, wrap");
	}
	
	private void initStatusBar(){
		lblMode.setForeground(Color.BLUE);
		statusBar.setLayout(new MigLayout("fillx"));
		statusBar.add(lblMode, "dock west, grow");
		statusBar.setBorder(BorderFactory.createEtchedBorder());
		add(statusBar, "dock south, grow");
	}
	
	public void firsRow() {
		if (tblModel.getRowCount() == 0) {
			btnFirst.getToolkit().beep();
			return;
		}
		tblGrid.setRowSelectionInterval(0, 0);
	};

	public void previousRow() {
		if (tblModel.getRowCount() == 0) {
			btnPrevious.getToolkit().beep();
			return;
		}
		int prethodni = tblGrid.getSelectedRow();
		if (--prethodni < 0 || prethodni == -1) {
			int rowCount = tblModel.getRowCount();
			tblGrid.setRowSelectionInterval(rowCount - 1, rowCount - 1);
			return;
		}
		tblGrid.setRowSelectionInterval(prethodni, prethodni);
	};

	public void nextRow() {
		if (tblModel.getRowCount() == 0) {
			btnNext.getToolkit().beep();
			return;
		}
		int sledeci = tblGrid.getSelectedRow();
		if (++sledeci > (tblGrid.getModel().getRowCount() - 1) || sledeci == -1) {
			tblGrid.setRowSelectionInterval(0,0);
			return;
		}
		tblGrid.setRowSelectionInterval(sledeci, sledeci);
	};

	public void lastRow() {
		if (tblModel.getRowCount() == 0) {
			btnLast.getToolkit().beep();
			return;
		}
		int rowCount = tblModel.getRowCount();
		if (rowCount > 0)
			tblGrid.setRowSelectionInterval(rowCount - 1, rowCount - 1);

	};
	
	public void Commit(){
		if(mode == MODE_SEARCH){
			Pretraga();
			changeMode(MODE_EDIT);
			btnCommit.setEnabled(false);
		}
		else if(mode == MODE_EDIT){
			String errorMessage = ValidateTextFields();
			if(errorMessage == "")
			{
				Izmena();
			}
			else
			{
				JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		else{
			String errorMessage = ValidateTextFields();
			if(errorMessage == "")
			{
				Dodaj();
				ClearTextFields(true);
				changeMode(MODE_EDIT);
			}
			else
			{
				JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public void changeMode(int newMode) {
		mode = newMode;
		if (mode == 0) {
			lblMode.setText("Edit    ");
			tblGrid.setEnabled(true);
			textFieldsEditable(false);
		} else if (mode == 1) {
			lblMode.setText("Add");
			ClearTextFields(true);
			tblGrid.setEnabled(false);
		} else if (mode == 2) {
			lblMode.setText("Search");
			ClearTextFields(true);
			tblGrid.setEnabled(false);
		}
	}

	public void Delete() {
		int index = tblGrid.getSelectedRow();
		if(index < 0 || mode != MODE_EDIT){
			return;
		}
		try {
			if (JOptionPane.showConfirmDialog((Component) this, "Da li ste sigurni?", "Pitanje", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				tblModel.obrisiRed(index);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void Refresh() {
		try {
			tblModel.Refresh();
		} catch (SQLException e) {
			//e.printStackTrace();
		}
	}

	public void nextFormMenu() {
		if(tblGrid.getSelectedRow()>=0)
		{
			popup = new JPopupMenu();
			for (String key : nextForms.keySet())
			{
				popup.add(new JMenuItem(new NextFormItemAction(key, this)));
			}
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
		else
		{
			btnNextForm.getToolkit().beep();
		}
	}

	public void nextForm(String key) {
		int index = tblGrid.getSelectedRow();
		if(index < 0)
			return;
		AbstractStandardForm nextForm = nextForms.get(key);
		nextForm.getTblModel().setWhereStm(where + "'" + (String) tblModel.getValueAt(index, 0) + "'");
		nextForm.Refresh();
		nextForm.setVisible(true);
	}
	
	public void Add(){
		changeMode(MODE_ADD);
		ClearTextFields(true);
		textFieldsEditable(true);
		btnCommit.setEnabled(true);
		btnRollback.setEnabled(true);
	}

	public void Rollback(){	
		changeMode(MODE_EDIT);
		Refresh();
		ClearTextFields(false);
		textFieldsEditable(false);
	}

	public void Search(){
		changeMode(MODE_SEARCH);
		ClearTextFields(true);
		textFieldsEditable(true);
		btnCommit.setEnabled(true);
		btnRollback.setEnabled(true);
	}
	
	public void zoomPickup(){
		//implementirati proveru da li zoom treba da se izvrsi
		if(tblGrid.getSelectedRow() < 0 || this.getParent() == MainFrame.getInstance())
		{
			btnPickup.getToolkit().beep();
			return;
		}
	
		dispose();
	}

	protected void initGui()
	{
		btnCommit = new JButton(new CommitAction(this));
		btnRollback = new JButton(new RollbackAction(this));
		btnCommit.setEnabled(false);
		btnRollback.setEnabled(false);
	}
	protected abstract void initTextFields();
	public abstract void sync();
	public abstract void Dodaj();
	public abstract void Izmena();
	public abstract void Pretraga();
	public abstract void zoomForm(String btnName);
	protected abstract void ClearTextFields(boolean setFocus);
	protected abstract void DisableAllUnEditableFields();
	protected abstract void textFieldsEditable(boolean editable);
	public abstract String ValidateTextFields();
	
	public AbstractTableModel getTblModel() {
		return tblModel;
	}
	
	public JTable getTblGrid() {
		return tblGrid;
	}
}
