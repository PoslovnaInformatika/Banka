package gui.standard.form;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.BankaTableModel;
import net.miginfocom.swing.MigLayout;

public class BankaStandardForm  extends AbstractStandardForm{
	private static final long serialVersionUID = 1L;
	
	private JTextField tfID;
	private JTextField tfSifra;
	private JTextField tfPIB;
	private JTextField tfNaziv;
	private JTextField tfAdresa;
	private JTextField tfEmail;
	private JTextField tfWeb;
	private JTextField tfTelefon;
	private JTextField tfFax;

	public BankaStandardForm(){
		super("Banka", new BankaTableModel(new String[]{"ID", "Šifra", "PIB", "Naziv", "Adresa", "Email", "Web", "Telefon", "Fax"}, 0));
		
		initGui();
		setupTableLook();
		where = " WHERE b_id=";
		nextForms.put("Racuni", new RacunStandardForm());
		nextForms.put("Kursne liste", new KursnaListaStandardForm());
	}
	
	@Override
	protected void initTextFields() {
		tfID = new JTextField(4);
		tfID.addKeyListener(new TextFieldTextValidator(4, tfID, TextFieldTextValidator.CharTypes.Letters));
		tfSifra = new JTextField(4);
		tfSifra.addKeyListener(new TextFieldTextValidator(3, tfSifra, TextFieldTextValidator.CharTypes.Whatever));
		tfPIB = new JTextField(10);
		tfPIB.addKeyListener(new TextFieldTextValidator(10, tfPIB, TextFieldTextValidator.CharTypes.Numbers));
		tfNaziv = new JTextField(30);
		tfNaziv.addKeyListener(new TextFieldTextValidator(120, tfNaziv, TextFieldTextValidator.CharTypes.Letters));
		tfAdresa = new JTextField(30);
		tfAdresa.addKeyListener(new TextFieldTextValidator(120, tfAdresa, TextFieldTextValidator.CharTypes.Whatever));
		tfEmail = new JTextField(30);
		tfEmail.addKeyListener(new TextFieldTextValidator(128, tfEmail, TextFieldTextValidator.CharTypes.Whatever));
		tfWeb = new JTextField(30);
		tfWeb.addKeyListener(new TextFieldTextValidator(128, tfWeb, TextFieldTextValidator.CharTypes.Whatever));
		tfTelefon = new JTextField(10);
		tfTelefon.addKeyListener(new TextFieldTextValidator(20, tfTelefon, TextFieldTextValidator.CharTypes.Numbers));
		tfFax = new JTextField(10);
		tfFax.addKeyListener(new TextFieldTextValidator(20, tfFax, TextFieldTextValidator.CharTypes.Numbers));
		
	}
	
	protected void setupTableLook() {
		tblGrid.removeColumn(tblGrid.getColumn("Email"));
		tblGrid.removeColumn(tblGrid.getColumn("Web"));
		tblGrid.removeColumn(tblGrid.getColumn("Fax"));
		tblGrid.removeColumn(tblGrid.getColumn("Telefon"));
	}
	
	protected void initGui(){
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();

		JLabel lblID = new JLabel ("ID banke:");
		JLabel lblSifra = new JLabel("Šifra banke:");
		JLabel lblPIB = new JLabel("PIB:");
		JLabel lblNaziv = new JLabel ("Naziv:");
		JLabel lblAdresa = new JLabel("Adresa:");
		JLabel lblEmail = new JLabel("E-Mail:");
		JLabel lblWeb = new JLabel ("Web:");
		JLabel lblTelefon = new JLabel("Telefon:");
		JLabel lblFax = new JLabel("Fax:");
		
		dataPanel.add(lblID);
		dataPanel.add(tfID,"wrap");
		
		dataPanel.add(lblSifra);
		dataPanel.add(tfSifra, "wrap");
		
		dataPanel.add(lblPIB);
		dataPanel.add(tfPIB,"wrap");
		
		dataPanel.add(lblNaziv);
		dataPanel.add(tfNaziv,"wrap");
		
		dataPanel.add(lblAdresa);
		dataPanel.add(tfAdresa,"wrap");
		
		dataPanel.add(lblEmail);
		dataPanel.add(tfEmail,"wrap");
		
		dataPanel.add(lblWeb);
		dataPanel.add(tfWeb,"wrap");
		
		dataPanel.add(lblTelefon);
		dataPanel.add(tfTelefon,"wrap");
		
		dataPanel.add(lblFax);
		dataPanel.add(tfFax,"wrap");
		
		bottomPanel.add(dataPanel);

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel,"dock east");
		
		add(bottomPanel, "grow, wrap");
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			ClearTextFields(false);
		}
		else{
			tfID.setText((String) tblModel.getValueAt(index, 0));
			tfSifra.setText((String) tblModel.getValueAt(index, 1));
			tfPIB.setText((String) tblModel.getValueAt(index, 2));
			tfNaziv.setText((String) tblModel.getValueAt(index, 3));
			tfAdresa.setText((String) tblModel.getValueAt(index, 4));
			tfEmail.setText((String) tblModel.getValueAt(index, 5));
			tfWeb.setText((String) tblModel.getValueAt(index, 6));
			tfTelefon.setText((String) tblModel.getValueAt(index, 7));
			tfFax.setText((String) tblModel.getValueAt(index, 8));	
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed(new String[] {tfID.getText(), tfSifra.getText(), tfPIB.getText(), tfNaziv.getText(), tfAdresa.getText(), tfEmail.getText(), tfWeb.getText(), tfTelefon.getText(), tfFax.getText()});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		 try {
			tblModel.izmeniRed(index, new String[] {tfID.getText(), tfSifra.getText(), tfPIB.getText(), tfNaziv.getText(), tfAdresa.getText(), tfEmail.getText(), tfWeb.getText(), tfTelefon.getText(), tfFax.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		String id = tfID.getText();
		String sifra = tfSifra.getText();
		String pib = tfPIB.getText();
		String naziv = tfNaziv.getText();
		String adresa = tfAdresa.getText();
		String email = tfEmail.getText();
		String web = tfWeb.getText();
		String telefon = tfTelefon.getText();
		String fax = tfFax.getText();
		
		if(id.equals("") && sifra.equals("") && pib.equals("") && naziv.equals("") && adresa.equals("") && email.equals("") && web.equals("") && telefon.equals("") && fax.equals(""))
			return;
		
		try {
			tblModel.Pretraga(new String[] {id, sifra, pib, naziv,  adresa, email, web, telefon, fax});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfID.setText("");
		tfSifra.setText("");
		tfPIB.setText("");
		tfNaziv.setText("");
		tfAdresa.setText("");
		tfEmail.setText("");
		tfWeb.setText("");
		tfTelefon.setText("");
		tfFax.setText("");
		
		if(setFocus)
			tfID.requestFocus();
	}

	public JTextField getTfID() {
		return tfID;
	}

	public JTextField getTfSifra() {
		return tfSifra;
	}
	
	public JTextField getTfPIB() {
		return tfPIB;
	}
	
	public JTextField getTfNaziv() {
		return tfNaziv;
	}

	public JTextField getTfAdresa() {
		return tfAdresa;
	}
	
	public JTextField getTfEmail() {
		return tfEmail;
	}
	
	public JTextField getTfWeb() {
		return tfWeb;
	}

	public JTextField getTfTelefon() {
		return tfTelefon;
	}
	
	public JTextField getTfFax() {
		return tfFax;
	}
	
	@Override
	protected void textFieldsEditable(boolean editable) {
			tfID.setEditable(editable);
			tfSifra.setEditable(editable);
			tfPIB.setEditable(editable);
			tfNaziv.setEditable(editable);
			tfAdresa.setEditable(editable);
			tfEmail.setEditable(editable);
			tfWeb.setEditable(editable);
			tfTelefon.setEditable(editable);
			tfFax.setEditable(editable);
			
	}
	
	@Override
	public void zoomForm(String btnName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfID.setEditable(false);
		tfSifra.setEditable(false);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		if(tfID.getText().trim().equals("")) errorMessage += "ID banke je obavezno polje.\n";
			
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				Statement stmnt1 = DBConnection.getConnection().createStatement();
			
				ResultSet rset = stmnt.executeQuery("SELECT b_id FROM banka WHERE b_id = '" + tfID.getText().trim() + "'");
				ResultSet rset1 = stmnt1.executeQuery("SELECT b_sifra FROM banka WHERE b_sifra = '" + tfSifra.getText().trim() + "'");
	
				if (rset.next()) errorMessage += "Banka koja ima identifikaciju " + tfID.getText().trim() + " već postoji.\n";
				if (rset1.next()) errorMessage += "Banka koja ima sifru " + tfSifra.getText().trim() + " već postoji.\n";
				
				rset.close();
				rset1.close();
				
				stmnt.close();
				stmnt1.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfNaziv.getText().trim().equals("")) errorMessage += "Naziv Banke je obavezno polje.\n";
		if(tfPIB.getText().trim().equals("")) errorMessage += "PIB banke je obavezno polje.\n";
		if(tfSifra.getText().trim().equals("")) errorMessage += "Sifra banke je obavezno polje.\n";
		if(tfAdresa.getText().trim().equals("")) errorMessage += "Adresa banke je obavezno polje.\n";
		
		return errorMessage;
	}
}

