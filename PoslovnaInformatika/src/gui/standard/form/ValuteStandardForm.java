package gui.standard.form;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.ValuteTableModel;
import net.miginfocom.swing.MigLayout;

public class ValuteStandardForm extends AbstractStandardForm{

	private static final long serialVersionUID = -4222693595613109859L;
	
	private JTextField tfID;
	private JTextField tfSifra;
	private JTextField tfNaziv;
	private JTextField tfSifraDrzave;

	public ValuteStandardForm() {
		super("Valute", new ValuteTableModel(new String[]{"ID valute", "Zvanična šifra", "Naziv valute", "Sifra drzave"}, 0));
		where = " WHERE val_id =";
		nextForms.put("Kurs u valuti", new KursUValutiStandardForm());
		nextForms.put("Racuni", new RacunStandardForm());
		initGui();
	}	
	
	@Override
	protected void initTextFields() {
		tfID = new JTextField(5);
		tfID.addKeyListener(new TextFieldTextValidator(4, tfID, TextFieldTextValidator.CharTypes.Whatever));
		tfSifra = new JTextField(5);
		tfSifra.addKeyListener(new TextFieldTextValidator(3, tfSifra, TextFieldTextValidator.CharTypes.Whatever));
		tfNaziv = new JTextField(15);
		tfNaziv.addKeyListener(new TextFieldTextValidator(30, tfNaziv, TextFieldTextValidator.CharTypes.Letters));
		tfSifraDrzave = new JTextField(5);
		tfSifraDrzave.addKeyListener(new TextFieldTextValidator(4, tfSifraDrzave, TextFieldTextValidator.CharTypes.Letters));
		
		btnLookup = new JButton(new ZoomFormAction(this, null));
	}

	public JTextField getTfID() {
		return tfID;
	}

	public void setTfID(JTextField tfID) {
		this.tfID = tfID;
	}

	public JTextField getTfNaziv() {
		return tfNaziv;
	}

	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		JPanel dataPanel = new JPanel();
		JPanel dataPanelDrzava = new JPanel();
		
		bottomPanel.setLayout(new MigLayout("fillx"));
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelDrzava.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();
		
		btnLookup.setMaximumSize(new Dimension(70, 20));
		
		JLabel lblID = new JLabel("ID valute:");
		JLabel lblSifra = new JLabel("Šifra valute:");
		JLabel lblNaziv = new JLabel("Naziv valute:");
		JLabel lblDrzava = new JLabel("Država:");
		JLabel lblSifraDrzave = new JLabel("Šifra države:");
		
		dataPanel.add(lblID);
		dataPanel.add(tfID, "wrap");
		dataPanel.add(lblSifra);
		dataPanel.add(tfSifra, "wrap");
		dataPanel.add(lblNaziv);
		dataPanel.add(tfNaziv, "wrap");
		
		dataPanelDrzava.add(lblDrzava);
		dataPanelDrzava.add(btnLookup, "wrap");
		
		dataPanelDrzava.add(lblSifraDrzave);
		dataPanelDrzava.add(tfSifraDrzave, "wrap");

		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelDrzava, "dock west, wrap");
		
		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			ClearTextFields(false);
		} else {
			tfID.setText((String) tblModel.getValueAt(index, 0));
			tfSifra.setText((String) tblModel.getValueAt(index, 1));
			tfNaziv.setText((String) tblModel.getValueAt(index, 2));
			tfSifraDrzave.setText((String) tblModel.getValueAt(index, 3));
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed( new String[] {tfID.getText(), tfSifra.getText(), tfNaziv.getText(), tfSifraDrzave.getText()});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			tblModel.izmeniRed(index, new String[] { tfID.getText(), tfSifra.getText(), tfNaziv.getText(), tfSifraDrzave.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		String idValute = tfID.getText();
		String sifraValute = tfSifra.getText();
		String nazivValute = tfNaziv.getText();
		String sifraDrzave = tfSifraDrzave.getText();
		
		if(idValute.equals("") && sifraValute.equals("") && nazivValute.equals("") && sifraDrzave.equals(""))
			return;
		
		try {
			tblModel.Pretraga( new String[] {idValute, sifraValute, nazivValute, sifraDrzave});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfID.setText("");
		tfSifra.setText("");
		tfNaziv.setText("");
		tfSifraDrzave.setText("");
		
		if(setFocus)
			tfID.requestFocus();
		
	}

	@Override
	public void zoomForm(String btnName) {
		DrzavaStandardForm zoomForm = new DrzavaStandardForm();
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		tfSifraDrzave.setText(zoomForm.getTfSifra().getText());
		
	}

	@Override
	protected void textFieldsEditable(boolean editable) {
			tfID.setEditable(editable);
			tfSifra.setEditable(editable);
			tfSifraDrzave.setEditable(editable);
			tfNaziv.setEditable(editable);
			btnLookup.setEnabled(editable);
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfID.setEditable(false);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		
		if(tfID.getText().trim().equals("")) errorMessage += "ID valute je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT val_id FROM valute WHERE val_id = '" + tfID.getText().trim() + "'");
				if (rset.next()) errorMessage += "Valuta sa ID " + tfID.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfSifra.getText().trim().equals("")) errorMessage += "Šifra valute je obavezno polje.\n";
		if(tfNaziv.getText().trim().equals("")) errorMessage += "Naziv valute je obavezno polje.\n";
		if(tfSifraDrzave.getText().trim().equals("")) errorMessage += "Šifra države je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT dr_sifra FROM drzava WHERE dr_sifra = '" + tfSifraDrzave.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Država sa šifrom " + tfSifraDrzave.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return errorMessage;
	}
}
