package gui.standard.form;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.KursnaListaTableModel;
import net.miginfocom.swing.MigLayout;
import utility.DateLabelFormatter;

public class KursnaListaStandardForm extends AbstractStandardForm{

	private static final long serialVersionUID = 1L;
	
	private JTextField tfID;
	private SqlDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private JTextField tfBrKursneListe;
	private SqlDateModel modelPrimOd;
	private JDatePanelImpl datePanelPrimOd;
	private JDatePickerImpl datePickerPrimOd;
	private JTextField tfIDBanke;
	
	public KursnaListaStandardForm() {
		super("KursnaLista", new KursnaListaTableModel(new String[]{"ID", "Datum", "Broj kursne liste", "Primjenjuje se od", "Banka"}, 0));
		initGui();
		where = " WHERE kurl_val_u_listi=";
		nextForms.put("Kurs u valuti", new KursUValutiStandardForm());	
	}
	
	public JTextField getTfID() {
		return tfID;
	}

	public void setTfID(JTextField tfID) {
		this.tfID = tfID;
	}

	@Override
	protected void initGui() {
		
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelBanka = new JPanel();
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelBanka.setLayout(new MigLayout("gapx 15px"));
		
		JPanel buttonsPanel = new JPanel();

		btnLookup.setMaximumSize(new Dimension(70, 20));
		
		JLabel lblID = new JLabel ("ID kursne liste:");
		JLabel lblDatum = new JLabel("Datum:");
		JLabel lblBrojKL = new JLabel("Broj kursne liste:");
		JLabel lblPrimOd = new JLabel ("Primjenjuje se od:");
		JLabel lblBanka = new JLabel("Banka:");
		JLabel lblIDBanke = new JLabel("ID Banke:");
		
		dataPanel.add(lblID);
		dataPanel.add(tfID, "wrap");
		
		dataPanel.add(lblDatum);
		dataPanel.add(datePicker, "wrap");
		
		dataPanel.add(lblBrojKL);
		dataPanel.add(tfBrKursneListe, "wrap");
		
		dataPanel.add(lblPrimOd);
		dataPanel.add(datePickerPrimOd, "wrap");
		
		dataPanelBanka.add(lblBanka);
		dataPanelBanka.add(btnLookup, "wrap");
		
		dataPanelBanka.add(lblIDBanke);
		dataPanelBanka.add(tfIDBanke, "wrap");
		
		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelBanka, "dock west, wrap");
		
		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");
	}

	@Override
	protected void initTextFields() {
		
		tfID = new JTextField(5);
		tfID.addKeyListener(new TextFieldTextValidator(4, tfID, TextFieldTextValidator.CharTypes.Whatever));
		model = new SqlDateModel();
		model.setSelected(true);
		model.setValue(null);
		datePanel = new JDatePanelImpl(model, new Properties());
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setShowYearButtons(true);
		tfBrKursneListe = new JTextField(15);
		tfBrKursneListe.addKeyListener(new TextFieldTextValidator(3, tfBrKursneListe, TextFieldTextValidator.CharTypes.Numbers));
		modelPrimOd = new SqlDateModel();
		modelPrimOd.setSelected(true);
		modelPrimOd.setValue(null);
		datePanelPrimOd = new JDatePanelImpl(modelPrimOd, new Properties());
		datePickerPrimOd = new JDatePickerImpl(datePanelPrimOd, new DateLabelFormatter());
		datePickerPrimOd.setShowYearButtons(true);
		tfIDBanke = new JTextField(5);
		tfIDBanke.addKeyListener(new TextFieldTextValidator(4, tfIDBanke, TextFieldTextValidator.CharTypes.Letters));

		btnLookup = new JButton(new ZoomFormAction(this, null));
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			ClearTextFields(false);
		}
		else{
			tfID.setText((String) tblModel.getValueAt(index, 0));
			java.util.Date utilDate = null;
			try {
				utilDate = new SimpleDateFormat("yyyy-MM-dd").parse((String)tblModel.getValueAt(index, tblModel.findColumn("Datum")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
			model.setValue(sqlDate);
			tfBrKursneListe.setText((String) tblModel.getValueAt(index, 2));
			java.util.Date utilDate1 = null;
			try {
				utilDate1 = new SimpleDateFormat("yyyy-MM-dd").parse((String)tblModel.getValueAt(index, tblModel.findColumn("Primjenjuje se od")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			java.sql.Date sqlDate1 = new java.sql.Date(utilDate1.getTime());
			modelPrimOd.setValue(sqlDate1);
			tfIDBanke.setText((String) tblModel.getValueAt(index, 4));
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed( new String[] {tfID.getText(), datePanel.getModel().getValue().toString(), tfBrKursneListe.getText(), datePanelPrimOd.getModel().getValue().toString(), tfIDBanke.getText()});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}

	@Override
	public void Izmena() {
		
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			tblModel.izmeniRed(index, new String[] {tfID.getText(), datePanel.getModel().getValue().toString(), tfBrKursneListe.getText(), datePanelPrimOd.getModel().getValue().toString(), tfIDBanke.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		try {
			tblModel.Pretraga(new String[] { tfID.getText(), String.valueOf(datePanel.getModel().getValue()), tfBrKursneListe.getText(), String.valueOf(datePanelPrimOd.getModel().getValue()), tfIDBanke.getText()});
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void zoomForm(String btnName) {
		
		BankaStandardForm zoomForm = new BankaStandardForm();
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		tfIDBanke.setText(zoomForm.getTfID().getText());
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfID.setText("");
		datePicker.getModel().setValue(null);
		tfBrKursneListe.setText("");
		datePickerPrimOd.getModel().setValue(null);
		tfIDBanke.setText("");
		
		if(setFocus)
			tfID.requestFocus();
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfID.setEditable(false);
	}

	@Override
	protected void textFieldsEditable(boolean editable) {
		
		tfID.setEditable(editable);
		datePicker.getComponent(1).setEnabled(editable);
		tfBrKursneListe.setEditable(editable);
		datePickerPrimOd.getComponent(1).setEnabled(editable);
		tfIDBanke.setEditable(editable);
		
		btnLookup.setEnabled(editable);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		if(tfID.getText().trim().equals("")) errorMessage += "ID kursne liste je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT kurl_id FROM kursna_lista WHERE kurl_id = '" + tfID.getText().trim() + "'");
				if (rset.next()) errorMessage += "Kursna lista sa ID " + tfID.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(model.getValue() == null) errorMessage += "Datum kursne liste je obavezno polje.\n";
		if(tfBrKursneListe.getText().trim().equals("")) errorMessage += "Broj kursne liste je obavezno polje\n";
		if(modelPrimOd.getValue() == null) errorMessage += "Datum početka važenja kursne liste je obavezno polje.\n";
		if(tfIDBanke.getText().trim().equals("")) errorMessage += "ID banke je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT b_id FROM banka WHERE b_id = '" + tfIDBanke.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Banka sa ID " + tfIDBanke.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return errorMessage;
	}
}
