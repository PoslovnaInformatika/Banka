package gui.standard.form;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.standard.form.ZoomFormAction;
import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.NaseljenaMestaTableModel;
import net.miginfocom.swing.MigLayout;

public class NaseljenoMestoStandardForm extends AbstractStandardForm{

	private static final long serialVersionUID = -4222693595613109859L;
	
	private JTextField tfSifraMesta;
	private JTextField tfNazivMesta;
	private JTextField tfSifraDrzave;
	private JTextField tfNazivDrzave;
	private JTextField tfPttOznaka;

	public NaseljenoMestoStandardForm() {
		super("Naseljena mesta", new NaseljenaMestaTableModel(new String[]{"Sifra Mesta", "Naziv mesta", "PTT broj", "Sifra drzave"}, 0));
		initGui();
		btnNextForm.setEnabled(false);
	}	
	
	@Override
	protected void initTextFields() {
		tfSifraMesta = new JTextField(5);
		tfSifraMesta.addKeyListener(new TextFieldTextValidator(2, tfSifraMesta, TextFieldTextValidator.CharTypes.Letters));
		tfNazivMesta = new JTextField(15);
		tfNazivMesta.addKeyListener(new TextFieldTextValidator(60, tfNazivMesta, TextFieldTextValidator.CharTypes.Letters));
		tfNazivDrzave = new JTextField(15);
		tfNazivDrzave.addKeyListener(new TextFieldTextValidator(80, tfNazivDrzave, TextFieldTextValidator.CharTypes.Letters));
		tfSifraDrzave = new JTextField(5);
		tfSifraDrzave.addKeyListener(new TextFieldTextValidator(3, tfSifraDrzave, TextFieldTextValidator.CharTypes.Letters));
		tfPttOznaka = new JTextField(15);
		tfPttOznaka.addKeyListener(new TextFieldTextValidator(12, tfPttOznaka, TextFieldTextValidator.CharTypes.Numbers));

		btnLookup = new JButton(new ZoomFormAction(this, null));
	}

	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		JPanel dataPanel = new JPanel();
		JPanel dataPanelDrzava = new JPanel();
		
		bottomPanel.setLayout(new MigLayout("fillx"));
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelDrzava.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();
		
		btnLookup.setMaximumSize(new Dimension(70, 20));
		
		JLabel lblSifra = new JLabel("Šifra mesta:");
		JLabel lblNaziv = new JLabel("Naziv mesta:");
		JLabel lblPTT = new JLabel("PTT broj mesta:");
		JLabel lblDrzava = new JLabel("Drzava");
		JLabel lblSifraDrzave = new JLabel("Šifra drzave:");
		JLabel lblNazivDrzave = new JLabel("Naziv drzave:");

		dataPanel.add(lblSifra);
		dataPanel.add(tfSifraMesta, "wrap");
		dataPanel.add(lblNaziv);
		dataPanel.add(tfNazivMesta, "wrap");
		dataPanel.add(lblPTT);
		dataPanel.add(tfPttOznaka, "wrap");
		
		dataPanelDrzava.add(lblDrzava);
		dataPanelDrzava.add(btnLookup, "wrap");
		
		dataPanelDrzava.add(lblSifraDrzave);
		dataPanelDrzava.add(tfSifraDrzave, "wrap");
		
		dataPanelDrzava.add(lblNazivDrzave);
		dataPanelDrzava.add(tfNazivDrzave, "wrap");
		

		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelDrzava, "dock west, wrap");
		
		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			ClearTextFields(false);
		} else {
			tfSifraMesta.setText((String) tblModel.getValueAt(index, 0));
			tfNazivMesta.setText((String) tblModel.getValueAt(index, 1));
			tfPttOznaka.setText((String) tblModel.getValueAt(index, 2));
			tfSifraDrzave.setText((String) tblModel.getValueAt(index, 3));
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed( new String[] {tfSifraMesta.getText(), tfNazivMesta.getText(), tfPttOznaka.getText(), tfSifraDrzave.getText()});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		
		 try {
			tblModel.izmeniRed(index, new String[] { tfSifraMesta.getText(), tfNazivMesta.getText(), tfPttOznaka.getText(), tfSifraDrzave.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		String nazivMesta = tfNazivMesta.getText();
		String sifraMesta = tfSifraMesta.getText();
		String pttOznaka = tfPttOznaka.getText();
		String sifraDrzave = tfSifraDrzave.getText();
		String nazivDrzave = tfNazivDrzave.getText();
		
		if(nazivMesta.equals("") && sifraMesta.equals("") && pttOznaka.equals("") && sifraDrzave.equals("") && nazivDrzave.equals(""))
			return;
		
		try {
			tblModel.Pretraga( new String[] {sifraMesta, nazivMesta, pttOznaka, sifraDrzave, nazivDrzave});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfSifraMesta.setText("");
		tfNazivMesta.setText("");
		tfNazivDrzave.setText("");
		tfSifraDrzave.setText("");
		tfPttOznaka.setText("");
		
		if(setFocus)
			tfSifraMesta.requestFocus();
		
	}

	@Override
	public void zoomForm(String btnName) {
		DrzavaStandardForm zoomForm = new DrzavaStandardForm();
		zoomForm.setLocation((int)this.getLocation().getX() + 20, (int)this.getLocation().getY() + 20);
		zoomForm.setVisible(true);
		tfSifraDrzave.setText(zoomForm.getTfSifra().getText());
		tfNazivDrzave.setText(zoomForm.getTfNaziv().getText());
		
	}

	@Override
	protected void textFieldsEditable(boolean editable) {
			tfSifraMesta.setEditable(editable);
			tfNazivMesta.setEditable(editable);
			tfSifraDrzave.setEditable(editable);
			tfNazivDrzave.setEditable(editable);
			tfPttOznaka.setEditable(editable);
			
			btnLookup.setEnabled(editable);
	}

	
	public JTextField getTfSifraMesta() {
		return tfSifraMesta;
	}
	

	public JTextField getTfNazivMesta() {
		return tfNazivMesta;
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfSifraMesta.setEditable(false);
		tfNazivDrzave.setEditable(false);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		
		if(tfSifraMesta.getText().trim().equals("")) errorMessage += "Šifra naseljenog mesta je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT nm_sifra FROM naseljeno_mesto WHERE nm_sifra = '" + tfSifraMesta.getText().trim() + "'");
				if (rset.next()) errorMessage += "Naseljeno mesto sa šifrom " + tfSifraMesta.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfNazivMesta.getText().trim().equals("")) errorMessage += "Naziv mesta je obavezno polje.\n";
		if(tfSifraDrzave.getText().trim().equals("")) errorMessage += "Šifra države je obavezno polje.\n";
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT dr_sifra FROM drzava WHERE dr_sifra = '" + tfSifraDrzave.getText().trim() + "'");
				if (!rset.next()) errorMessage += "Država sa šifrom " + tfSifraDrzave.getText().trim() + " ne postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfPttOznaka.getText().trim().equals("")) errorMessage += "PTT oznaka mesta je obavezno polje.\n";
		
		return errorMessage;
	}	
}
