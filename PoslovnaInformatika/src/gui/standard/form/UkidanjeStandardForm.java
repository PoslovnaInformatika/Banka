package gui.standard.form;

import model.tables.UkidanjeTableModel;

public class UkidanjeStandardForm extends AbstractStandardForm{
	private static final long serialVersionUID = 1L;
	
	public UkidanjeStandardForm(){
		super("Ukidanje", new UkidanjeTableModel(new String[]{"ID", "Datum", "Račun"}, 0));
		btnPickup.setEnabled(false);
		btnAdd.setEnabled(false);
		btnDelete.setEnabled(false);
		btnNextForm.setEnabled(false);
		btnSearch.setEnabled(false);
	}
	
	@Override
	protected void initTextFields() {
		
	}

	@Override
	public void sync() {
		
	}

	@Override
	public void Dodaj() {
		
	}

	@Override
	public void Izmena() {
		
	}

	@Override
	public void Pretraga() {
		
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		
	}
	
	@Override
	protected void textFieldsEditable(boolean editable) {
			
	}
	
	@Override
	public void zoomForm(String btnName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void DisableAllUnEditableFields() {
		
	}

	@Override
	public String ValidateTextFields() {
		return "";
	}
}
