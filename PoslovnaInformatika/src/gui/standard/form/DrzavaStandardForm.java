package gui.standard.form;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import database.DBConnection;
import listeners.TextFieldTextValidator;
import model.tables.DrzaveTableModel;
import net.miginfocom.swing.MigLayout;

public class DrzavaStandardForm extends AbstractStandardForm{
	private static final long serialVersionUID = 1L;
	
	private JTextField tfSifra;
	private JTextField tfNaziv;
	
	public DrzavaStandardForm(){
		super("Države", new DrzaveTableModel(new String[]{"Sifra", "Naziv"}, 0));
		nextForms.put("Naseljena mesta", new NaseljenoMestoStandardForm());
		nextForms.put("Valute", new ValuteStandardForm());
		where = " WHERE dr_sifra=";
		initGui();
	}
	
	@Override
	protected void initTextFields() {
		tfSifra = new JTextField(5);
		tfSifra.addKeyListener(new TextFieldTextValidator(3, tfSifra, TextFieldTextValidator.CharTypes.Letters));
		tfNaziv = new JTextField(30);
		tfNaziv.addKeyListener(new TextFieldTextValidator(80, tfNaziv, TextFieldTextValidator.CharTypes.Letters));
	}
	
	protected void initGui(){
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();

		JLabel lblSifra = new JLabel ("Šifra države:");
		JLabel lblNaziv = new JLabel("Naziv države:");
		
		dataPanel.add(lblSifra);
		dataPanel.add(tfSifra,"wrap");
		dataPanel.add(lblNaziv);
		dataPanel.add(tfNaziv, "wrap");
		
		bottomPanel.add(dataPanel);

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel,"dock east");
		
		add(bottomPanel, "grow, wrap");
	}

	@Override
	public void sync() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			ClearTextFields(false);
		}
		else{
			tfSifra.setText((String) tblModel.getValueAt(index, 0));
			tfNaziv.setText((String) tblModel.getValueAt(index, 1));
		}
	}

	@Override
	public void Dodaj() {
		try {
			tblModel.dodajRed(new String[] {tfSifra.getText(), tfNaziv.getText()});
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void Izmena() {
		int index = tblGrid.getSelectedRow();
		if(index < 0){
			return;
		}
		 try {
			tblModel.izmeniRed(index, new String[] { tfSifra.getText(), tfNaziv.getText()});
			tblGrid.setRowSelectionInterval(index, index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void Pretraga() {
		String naziv = tfNaziv.getText();
		String sifra = tfSifra.getText();
		
		if(naziv.equals("") && sifra.equals(""))
			return;
		
		try {
			tblModel.Pretraga(new String[] {sifra, naziv});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		tfNaziv.setText("");
		tfSifra.setText("");
		
		if(setFocus)
			tfSifra.requestFocus();
	}

	public JTextField getTfSifra() {
		return tfSifra;
	}

	public JTextField getTfNaziv() {
		return tfNaziv;
	}
	
	@Override
	protected void textFieldsEditable(boolean editable) {
			tfNaziv.setEditable(editable);
			tfSifra.setEditable(editable);
	}
	
	@Override
	public void zoomForm(String btnName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void DisableAllUnEditableFields() {
		tfSifra.setEditable(false);
	}

	@Override
	public String ValidateTextFields() {
		String errorMessage = "";
		if(tfSifra.getText().trim().equals("")) errorMessage += "Šifra države je obavezno polje.\n";
		else if(mode == MODE_ADD){
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT dr_sifra FROM drzava WHERE dr_sifra = '" + tfSifra.getText().trim() + "'");
				if (rset.next()) errorMessage += "Država sa šifrom " + tfSifra.getText().trim() + " već postoji.\n";
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(tfNaziv.getText().trim().equals("")) errorMessage += "Naziv drzave je obavezno polje.\n";
		
		return errorMessage;
	}
}
