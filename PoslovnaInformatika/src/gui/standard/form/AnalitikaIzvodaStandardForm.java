package gui.standard.form;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import actions.standard.form.ZoomFormAction;
import model.tables.AnalitikaIzvodaTableModel;
import net.miginfocom.swing.MigLayout;

public class AnalitikaIzvodaStandardForm extends AbstractStandardForm  {

	public AnalitikaIzvodaStandardForm() {
		
		super("Analitika izvoda", new AnalitikaIzvodaTableModel(new String[]{"Broj stavke", "Duznik", "Svrha placanja","Poverilac", "Datum prijema", "Datum valute", "Racun duznika", "Model zaduzenja", "Poziv na broj zaduzenja", "Racun poverioca", "Model odobrenja", "Poziv na broj odobrenja", "Hitno", "Iznos", "Vrsta placanja", "Broj izvoda"},0));
		// TODO Auto-generated constructor stub
		initGui();
		btnPickup.setEnabled(false);
		btnAdd.setEnabled(false);
		btnDelete.setEnabled(false);
		btnSearch.setEnabled(false);
		btnNextForm.setEnabled(false);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelOptional = new JPanel();
		
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelOptional.setLayout(new MigLayout("gapx 15px"));

		JPanel buttonsPanel = new JPanel();

		btnLookup = new JButton(new ZoomFormAction(this, null));
		
	}

	@Override
	protected void initTextFields() {
		
	}
	

	@Override
	public void sync() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Dodaj() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Izmena() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Pretraga() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zoomForm(String btnName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void DisableAllUnEditableFields() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void textFieldsEditable(boolean editable) {
		// TODO Auto-generated method stub

	}

	@Override
	public String ValidateTextFields() {
		// TODO Auto-generated method stub
		return null;
	}

}
	