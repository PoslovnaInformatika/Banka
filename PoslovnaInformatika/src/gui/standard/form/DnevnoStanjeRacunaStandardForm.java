package gui.standard.form;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.standard.form.ZoomFormAction;
import model.tables.DnevnoStanjeRacunaTableModel;
import net.miginfocom.swing.MigLayout;

public class DnevnoStanjeRacunaStandardForm extends AbstractStandardForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField tfBrojIzvoda;
	private JTextField tfDatumPrometa;
	private JTextField tfPrethodnoStanje;
	private JTextField tfPrometUKorst;
	private JTextField tfPrometNaTeret;
	private JTextField tfNovoStanje;

	
	public DnevnoStanjeRacunaStandardForm() {
		super("Dnevno stanje racuna", new DnevnoStanjeRacunaTableModel(new String[]{"Broj izvoda", "Datum prometa", "Prethodno stanje","Promet u korist", "Promet na teret", "Novo stanje"},0));
		
		initGui();
		setupTableLook();
		btnPickup.setEnabled(false);
		btnAdd.setEnabled(false);
		btnDelete.setEnabled(false);
		where = " WHERE dsr_brojizvoda=";
		nextForms.put("Analitika izvoda", new AnalitikaIzvodaStandardForm());
	}

	@Override
	protected void initGui() {
		super.initGui();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx"));
		JPanel dataPanel = new JPanel();
		JPanel dataPanelOptional = new JPanel();
		
		dataPanel.setLayout(new MigLayout("gapx 15px"));
		dataPanelOptional.setLayout(new MigLayout("gapx 15px"));
		
		JPanel buttonsPanel = new JPanel();
		btnLookup = new JButton(new ZoomFormAction(this, null));

		JLabel lblBrojIzvoda = new JLabel("Broj izvoda:");
		JLabel lblDatumPrometa = new JLabel("Datum prometa:");
		JLabel lblPrethodnoStanje = new JLabel("Prethodno stanje:");
		JLabel lblPrometUKorst = new JLabel("Promet u korist");
		JLabel lblPrometNaTeret = new JLabel("Promet na teret:");
		JLabel lblNovoStanje = new JLabel("Novo stanje:");
		
		
		dataPanel.add(lblBrojIzvoda);
		dataPanel.add(tfBrojIzvoda, "wrap");
		
		dataPanel.add(lblDatumPrometa);
		dataPanel.add(tfDatumPrometa, "wrap");
		
		dataPanel.add(lblPrethodnoStanje);
		dataPanel.add(tfPrethodnoStanje, "wrap");
		
		
		dataPanelOptional.add(lblPrometUKorst);
		dataPanelOptional.add(tfPrometUKorst, "wrap");
		
		dataPanelOptional.add(lblPrometNaTeret);
		dataPanelOptional.add(tfPrometNaTeret, "wrap");
		
		dataPanelOptional.add(lblNovoStanje);
		dataPanelOptional.add(tfNovoStanje, "wrap");
		
		bottomPanel.add(dataPanel, "dock west");
		bottomPanel.add(dataPanelOptional, "dock west, wrap");

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		
		bottomPanel.add(buttonsPanel, "dock east");

		add(bottomPanel, "grow, wrap");	
	}

	@Override
	protected void initTextFields() {
		tfBrojIzvoda = new JTextField(20);
		tfDatumPrometa = new JTextField(20);
		tfPrethodnoStanje = new JTextField(20);
		tfPrometUKorst = new JTextField(20);
		tfPrometNaTeret = new JTextField(20);
		tfNovoStanje = new JTextField(20);
	}

	@Override
	public void sync() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Dodaj() {

		
	}

	@Override
	public void Izmena() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Pretraga() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zoomForm(String btnName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void ClearTextFields(boolean setFocus) {		
		tfBrojIzvoda.setText("");
		tfDatumPrometa.setText("");
		tfPrethodnoStanje.setText("");
		tfPrometUKorst.setText("");
		tfPrometNaTeret.setText("");
		tfNovoStanje.setText("");
		
		if(setFocus)
			tfBrojIzvoda.requestFocus();

	}

	@Override
	protected void textFieldsEditable(boolean editable) {
		tfBrojIzvoda.setEditable(editable);
		tfDatumPrometa.setEditable(editable);
		tfPrethodnoStanje.setEditable(editable);
		tfPrometUKorst.setEditable(editable);
		tfPrometNaTeret.setEditable(editable);
		tfNovoStanje.setEditable(editable);
		
	}
	private void setupTableLook(){
	
	}

	@Override
	protected void DisableAllUnEditableFields() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String ValidateTextFields() {
		// TODO Auto-generated method stub
		return null;
	}

}
