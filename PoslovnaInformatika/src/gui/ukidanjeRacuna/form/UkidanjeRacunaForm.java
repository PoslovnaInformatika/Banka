package gui.ukidanjeRacuna.form;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.main.form.MainFrame;
import gui.standard.form.RacunStandardForm;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class UkidanjeRacunaForm extends JDialog{
	
	private JLabel idRacunaZaUkidanjeLabel;
	private JTextField idRacunaZaUkidanjeTf;
	private JLabel brojRacunaZaUkidanjeLabel;
	private JTextField brojRacunaZaUkidanjeTf;
	private JButton zoomButton;
	private JTextField racunTf;
	private JButton potvrdaButton;
	
	public UkidanjeRacunaForm() {
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		this.setTitle("Ukidanje racuna korisnika");
		setLayout(new MigLayout("gapx 15px"));
		setSize(new Dimension(350,230));
		setLocationRelativeTo(MainFrame.getInstance());
		setResizable(false);
		setModal(true);
		//Dimension dim = new Dimension(50,20);
		
		idRacunaZaUkidanjeLabel = new JLabel("ID racuna koji se ukida: ");
		idRacunaZaUkidanjeTf = new JTextField(5);
		//idRacunaZaUkidanjeTf.setPreferredSize(dim);
		brojRacunaZaUkidanjeLabel = new JLabel("Broj racuna koji se ukida: ");
		brojRacunaZaUkidanjeTf = new JTextField(18);
		//brojRacunaZaUkidanjeTf.setPreferredSize(dim);
		racunTf = new JTextField(5);
		zoomButton = new JButton("...");
		zoomButton.setMaximumSize(new Dimension(70, 20));
		zoomButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				RacunStandardForm racunForm = new RacunStandardForm();
				racunForm.setVisible(true);
				racunTf.setText(racunForm.getTfID().getText());
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		potvrdaButton = new JButton("Prebaci sredstva");
		
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new BorderLayout());
		
		JPanel panel = new JPanel();
		//panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));\
		GridLayout grid = new GridLayout(0,2);
		panel.setLayout(grid);
		JPanel panelZoom = new JPanel();
		panelZoom.setLayout(new MigLayout("gapx 15px"));
		
		panel.add(idRacunaZaUkidanjeLabel);
		panel.add(idRacunaZaUkidanjeTf);
		panel.add(brojRacunaZaUkidanjeLabel);
		panel.add(brojRacunaZaUkidanjeTf);
		panelZoom.add(racunTf);
		panelZoom.add(zoomButton, "wrap");
		panel.add(panelZoom);
		
		dataPanel.add(panel, BorderLayout.NORTH);
		potvrdaButton.setMaximumSize(new Dimension(30, 30));
		dataPanel.add(potvrdaButton, BorderLayout.SOUTH);
		
		dataPanel.setBorder(BorderFactory.createTitledBorder("Ukidanje i prebacivanje sredstava:")) ;
		add(dataPanel);
	}
	
	
}
