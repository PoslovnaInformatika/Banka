package gui.xmlimport.form;

import gui.standard.form.AnalitikaIzvodaStandardForm;

import java.awt.Dimension;
import java.sql.SQLException;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import jaxb.uplatnica.duznik.UplatnicaDuznik;
import model.tables.AnalitikaIzvodaTableModel;

public class ImportDuznikForm extends AbstractImportXMLForm {

	private static final long serialVersionUID = 32282042166203503L;
	
	private UplatnicaDuznik duznik; 
	private String[][] rows;
	
	public ImportDuznikForm() {
		super("Duznik");
		duznik = new UplatnicaDuznik();
		vrstaPlacanja = VrstaPlacanja.ISPLATA.toString();
	}

	@Override
	public void setupLabels() {
		
		rows = new String[18][2];
		dataScrollPanel.setMinimumSize(new Dimension(400, 200));
		String[] header = {"Labela", "Podaci"};
		JTable tb = new JTable(rows, header);
		tb.getColumn("Labela").setMinWidth(100);
		tb.getColumn("Podaci").setMinWidth(200);
		dataScrollPanel = new JScrollPane(tb);
		
		rows[0][0] = "Duznik";
		rows[1][0] = "Naziv duznika";
		rows[2][0] = "Adresa duznika";
		rows[3][0] = "Racun duznika";
		
		rows[4][0] = "Poverilac";
		rows[5][0] = "Naziv poverioca";
		rows[6][0] = "Adresa poverioca";
		rows[7][0] = "Racun poverioca";
		
		rows[8][0] = "Placanje";
		rows[9][0] = "Svrha placanja";
		rows[10][0] = "Sifra placanja";
		rows[11][0] = "Valuta";
		rows[12][0] = "Iznos";
		rows[13][0] = "Broj modela";
		rows[14][0] = "Poziv na broj";
		rows[15][0] = "Datum prijema";
		rows[16][0] = "Mesto prijema";
		rows[17][0] = "Datum valute";
		 
	}
	
	@Override
	public void unmarshallFile(String path) {
		duznik = (UplatnicaDuznik) konverter.unmarshall(new UplatnicaDuznik(), path);
		
	}


	@Override
	public void fillLabels() {
		
		rows[0][1] = "";
		rows[1][1] = duznik.getDuznik().getNaziv();
		rows[2][1] = duznik.getDuznik().getAdresa().getUlica() + " " + duznik.getDuznik().getAdresa().getBroj() + ", " 
						+ duznik.getDuznik().getAdresa().getPosta() + " " + duznik.getDuznik().getAdresa().getMesto();
		
		rows[3][1] = duznik.getDuznik().getRacun();
		
		rows[4][1] = "";
		rows[5][1] = duznik.getPoverilac().getNaziv();
		rows[6][1] = duznik.getPoverilac().getAdresa().getUlica() + " " + duznik.getPoverilac().getAdresa().getBroj() + ", " 
						+ duznik.getPoverilac().getAdresa().getPosta() + " " + duznik.getPoverilac().getAdresa().getMesto();
		rows[7][1] = duznik.getPoverilac().getRacun();
		
		rows[8][1] = "";
		rows[9][1] = duznik.getPlacanje().getSvrhaPlacanja().trim();
		rows[10][1] = String.valueOf(duznik.getPlacanje().getSifraPlacanja());
		rows[11][1] = duznik.getPlacanje().getValuta();
		rows[12][1] = String.valueOf(duznik.getPlacanje().getIznos());
		rows[13][1] = String.valueOf(duznik.getPlacanje().getDuznik().getBrojModela());
		rows[14][1] = String.valueOf(duznik.getPlacanje().getDuznik().getPozivNaBroj());
		rows[15][1] = duznik.getPlacanje().getDatumIMestoPrijema().getDatum();
		rows[16][1] = duznik.getPlacanje().getDatumIMestoPrijema().getMesto();
		rows[17][1] = duznik.getPlacanje().getDatumValute();
		
		dataScrollPanel.repaint();
		
	}

	@Override
	public void izvrsiTransakciju() {
		
		AnalitikaIzvodaStandardForm analitika = new AnalitikaIzvodaStandardForm();
		AnalitikaIzvodaTableModel tblmodel = (AnalitikaIzvodaTableModel) analitika.getTblModel();
		String[] values = new String[15];
		values[0] = duznik.getDuznik().getNaziv();
		values[1] = duznik.getPlacanje().getSvrhaPlacanja();
		values[2] = duznik.getPoverilac().getNaziv();
		values[3] = duznik.getPlacanje().getDatumIMestoPrijema().getDatum();
		values[4] = duznik.getPlacanje().getDatumValute();
		values[5] = String.valueOf(duznik.getDuznik().getRacun());
		values[6] = String.valueOf(duznik.getPlacanje().getDuznik().getBrojModela());
		values[7] = String.valueOf(duznik.getPlacanje().getDuznik().getPozivNaBroj());
		values[8] = "/";
		values[9] = "/";
		values[10] = "/";
		values[11] = String.valueOf(false);
		values[12] = duznik.getPlacanje().getIznos();
		values[13] = vrstaPlacanja;
		values[14] = duznik.getPlacanje().getValuta();
		
			try {
				tblmodel.dodajRed(values);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
}

}