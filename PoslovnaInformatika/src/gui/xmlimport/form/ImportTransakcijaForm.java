package gui.xmlimport.form;

import gui.standard.form.AnalitikaIzvodaStandardForm;

import java.awt.Dimension;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import database.DBConnection;
import jaxb.uplatnica.transakcija.UplatnicaTransakcija;
import model.tables.AnalitikaIzvodaTableModel;
import utility.ExportClearingAndRTGS;

public class ImportTransakcijaForm extends AbstractImportXMLForm{

	private static final long serialVersionUID = -7564598101344758638L;
	
	private UplatnicaTransakcija transakcija; 
	private String[][] rows;
	private AnalitikaIzvodaStandardForm analitika;
	private AnalitikaIzvodaTableModel tblmodel;
	private double iznosTransakcijeRSD;
	ExportClearingAndRTGS export;

	public ImportTransakcijaForm() {
		super("Transakcija");
		transakcija = new UplatnicaTransakcija();
		vrstaPlacanja = VrstaPlacanja.TRANSAKCIJA.toString();
		
		analitika = new AnalitikaIzvodaStandardForm();
		tblmodel = (AnalitikaIzvodaTableModel) analitika.getTblModel();
		iznosTransakcijeRSD = 0;
	}

	@Override
	public void setupLabels() {
		rows = new String[21][2];
		dataScrollPanel.setMinimumSize(new Dimension(400, 200));
		String[] header = {"Labela", "Podaci"};
		JTable tb = new JTable(rows, header);
		tb.getColumn("Labela").setMinWidth(100);
		tb.getColumn("Podaci").setMinWidth(200);
		dataScrollPanel = new JScrollPane(tb);
		
		rows[0][0] = "Duznik";
		rows[1][0] = "Naziv duznika";
		rows[2][0] = "Adresa duznika";
		rows[3][0] = "Racun duznika";
		
		rows[4][0] = "Poverilac";
		rows[5][0] = "Naziv poverioca";
		rows[6][0] = "Adresa poverioca";
		rows[7][0] = "Racun poverioca";
		
		rows[8][0] = "Placanje";
		rows[9][0] = "Svrha placanja";
		rows[10][0] = "Sifra placanja";
		rows[11][0] = "Valuta";
		rows[12][0] = "Iznos";
		rows[13][0] = "Broj modela duznika";
		rows[14][0] = "Poziv na broj duznika";
		rows[15][0] = "Broj modela poverioca";
		rows[16][0] = "Poziv na broj poverioca";
		rows[17][0] = "Datum prijema";
		rows[18][0] = "Mesto prijema";
		rows[19][0] = "Datum valute";
		rows[20][0] = "Hitno";
		
	}

	@Override
	public void unmarshallFile(String path) {
		
		transakcija = (UplatnicaTransakcija) konverter.unmarshall(new UplatnicaTransakcija(), path);
		
	}

	@Override
	public void fillLabels() {
		rows[0][1] = "";
		rows[1][1] = transakcija.getDuznik().getNaziv();
		rows[2][1] = transakcija.getDuznik().getAdresa().getUlica() + " " + transakcija.getDuznik().getAdresa().getBroj() + "\n " 
						+ transakcija.getDuznik().getAdresa().getPosta() + " " + transakcija.getDuznik().getAdresa().getMesto();
		
		rows[3][1] = transakcija.getDuznik().getRacun();
		
		rows[4][1] = "";
		rows[5][1] = transakcija.getPoverilac().getNaziv();
		rows[6][1] = transakcija.getPoverilac().getAdresa().getUlica() + " " + transakcija.getPoverilac().getAdresa().getBroj() + " " 
						+ transakcija.getPoverilac().getAdresa().getPosta() + " " + transakcija.getPoverilac().getAdresa().getMesto();
		rows[7][1] = transakcija.getPoverilac().getRacun();
		
		rows[8][1] = "";
		rows[9][1] = transakcija.getPlacanje().getSvrhaPlacanja().trim();
		rows[10][1] = String.valueOf(transakcija.getPlacanje().getSifraPlacanja());
		rows[11][1] = transakcija.getPlacanje().getValuta();
		rows[12][1] = String.valueOf(transakcija.getPlacanje().getIznos());
		rows[13][1] = String.valueOf(transakcija.getPlacanje().getDuznik().getBrojModela());
		rows[14][1] = String.valueOf(transakcija.getPlacanje().getDuznik().getPozivNaBroj());
		rows[15][1] = String.valueOf(transakcija.getPlacanje().getPoverilac().getBrojModela());
		rows[16][1] = String.valueOf(transakcija.getPlacanje().getPoverilac().getPozivNaBroj());
		rows[17][1] = transakcija.getPlacanje().getDatumIMestoPrijema().getDatum();
		rows[18][1] = transakcija.getPlacanje().getDatumIMestoPrijema().getMesto();
		rows[19][1] = transakcija.getPlacanje().getDatumValute();
		rows[20][1] = String.valueOf(transakcija.getPlacanje().getHitno());
		
		dataScrollPanel.repaint();
		
	}

	@Override
	public void izvrsiTransakciju() {
		int statusTransakcije = 0;
	
		
		try {
			statusTransakcije = proveriBankeIValidnostRacuna();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(statusTransakcije == VrstaTransakcije.GRESKA_NEPOSTOJECI_RACUN.getID())
			return;
		else if(statusTransakcije == VrstaTransakcije.GRESKA_RACUN_ZAMRZNUT.getID())
			return;
		else if(statusTransakcije == VrstaTransakcije.GRESKA_ISTI_BROJ_RACUNA.getID())
			return;
		
//		analitika = new AnalitikaIzvodaStandardForm();
//		tblmodel = (AnalitikaIzvodaTableModel) analitika.getTblModel();
		
		
		
		if(statusTransakcije == VrstaTransakcije.RACUNI_U_ISTOJ_BANCI.getID()){
			obradiDuznika();
			obradiPoverioca();
		}
		else if(statusTransakcije == VrstaTransakcije.POSTOJECI_RACUN_DUZNIKA.getID()){
			export = new ExportClearingAndRTGS(this);
			
			if(transakcija.getPlacanje().getHitno().equals("Da") || iznosTransakcijeRSD >= 250000){
				obradiDuznika();
				export.exportRTGS(transakcija, VrstaTransakcije.POSTOJECI_RACUN_DUZNIKA.getID());
				return;
			}
			
			export.processClearing(transakcija);
			
			
		}
		else if(statusTransakcije == VrstaTransakcije.POSTOJECI_RACUN_POVERIOCA.getID()){
			export = new ExportClearingAndRTGS(this);
			
			if(transakcija.getPlacanje().getHitno().equals("Da") || iznosTransakcijeRSD >= 250000){
				obradiPoverioca();
				export.exportRTGS(transakcija, VrstaTransakcije.POSTOJECI_RACUN_POVERIOCA.getID());
				return;
			}
			
			export.processClearing(transakcija);
		}
		else if(statusTransakcije == VrstaTransakcije.POSTOJECI_RACUN_NA_RACUN_DRUGE_BANKE.getID()){
			export = new ExportClearingAndRTGS(this);
			
			if(transakcija.getPlacanje().getHitno().equals("Da") || iznosTransakcijeRSD >= 250000){
				obradiDuznika();
				obradiPoverioca();
				export.exportRTGS(transakcija, VrstaTransakcije.POSTOJECI_RACUN_NA_RACUN_DRUGE_BANKE.getID());
				return;
			}
			
			export.processClearing(transakcija);
		}
		
	}
	
	// proverava koji je tip transakcije, da li postoji samo jedan racun, da li postoje oba, da li su oba u istoj banci ili su 
	//   u razlicitim i vraca taj tip
	// takodje radi validaciju racuna i vraca tip greske ukoliko se pojavi
	private int proveriBankeIValidnostRacuna() throws SQLException{
		String bankaID1, racunVazeci1, brojRacuna1, bankaID2, racunVazeci2, brojRacuna2;
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("SELECT b_id, r_vazeci, r_brracuna FROM RACUN WHERE R_BRRACUNA = ? OR R_BRRACUNA = ?");
		
		stmnt.setString(1, transakcija.getDuznik().getRacun().replace("-", ""));
		stmnt.setString(2, transakcija.getPoverilac().getRacun().replace("-", ""));

		ResultSet rset = stmnt.executeQuery();
		
		if(!rset.next()){
			System.out.println("Transakcija nije validna jer racun ne postoji.");
			return VrstaTransakcije.GRESKA_NEPOSTOJECI_RACUN.getID();
		}
		
		bankaID1 = rset.getString("b_id");
		racunVazeci1 = rset.getString("r_vazeci");
		brojRacuna1 = rset.getString("r_brracuna");
		
		if(racunVazeci1.equals("0")){
			System.out.println("Greska pri validaciji racuna, racun je zamrznut.");
			return VrstaTransakcije.GRESKA_RACUN_ZAMRZNUT.getID();
		}
		
		if(!rset.next()){
			bankaID2 = null;
			racunVazeci2 = null;
			brojRacuna2 = null;
		}
		else{
			bankaID2 = rset.getString("b_id");
			racunVazeci2 = rset.getString("r_vazeci");
			brojRacuna2= rset.getString("r_brracuna");
			
			if(racunVazeci2.equals("0")){
				System.out.println("Greska pri validaciji racuna, racun je zamrznut.");
				return VrstaTransakcije.GRESKA_RACUN_ZAMRZNUT.getID();
			}
		}
		
		// proveravamo da li druga banka postoji u sistemu
		if(bankaID2 == null){
			// ako postoji samo jedna banka u sistemu onda proveravamo da li je duznik ili poverioc u toj banci
			if(brojRacuna1.equals(transakcija.getDuznik().getRacun().replace("-", ""))){
				konvertujIznosValute(bankaID1);
				return VrstaTransakcije.POSTOJECI_RACUN_DUZNIKA.getID();
			}
			else{
				konvertujIznosValute(bankaID1);
				return VrstaTransakcije.POSTOJECI_RACUN_POVERIOCA.getID();
			}
		}
		
		if(brojRacuna1.equals(brojRacuna2)){
			System.out.println("Greska racuna, duznik i primalac imaju isti racun.");
			return VrstaTransakcije.GRESKA_ISTI_BROJ_RACUNA.getID();
		}
		
		if(bankaID1.equals(bankaID2)){
			konvertujIznosValute(bankaID1);
			return VrstaTransakcije.RACUNI_U_ISTOJ_BANCI.getID();
		}
		else{
			konvertujIznosValute(bankaID1);
			return VrstaTransakcije.POSTOJECI_RACUN_NA_RACUN_DRUGE_BANKE.getID();
		}
	}
	
	
	// konvertuje valutu transakcije u RSD kako bi se korektno izvrsila provera da li je RTGS ili Clearing
	private int konvertujIznosValute(String bankaID1) throws SQLException{
		
		// ako je valuta transakcije RSD, ne radi nikakvu konverziju
		if(transakcija.getPlacanje().getValuta().equals("RSD")){
			iznosTransakcijeRSD = Double.valueOf( transakcija.getPlacanje().getIznos() );
			return 0;
		}
			
		// query za dobijanje kursne liste za banku u kojoj se desava transakcija
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("SELECT kurl_id FROM kursna_lista "
				+ "WHERE b_id = ? and kurl_datum = ? ");//<= ? ORDER BY kurl_prim_od DESC");
		stmnt.setString(1, bankaID1);
		stmnt.setString(2, transakcija.getPlacanje().getDatumValute());
		ResultSet result = stmnt.executeQuery();
		if(!result.next()){
			System.out.println("Kursna lista sa trazenim datumom ne postoji.");
			stmnt.close();
			return -1;
		}
					
		// query za dobijanje vrednosti valute koju konvertujemo u odnosu na valutu racuna
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM kurs_u_valuti "
				+ "WHERE kurl_val_u_listi = ? and (val_id_osn_val = ? or val_id_osn_val = ?) and (val_id_prema_val = ? or val_id_prema_val = ?) ");
				
		stmnt.setString(1, result.getString("kurl_id"));
		stmnt.setString(2, "RSD");
		stmnt.setString(3, transakcija.getPlacanje().getValuta());
		stmnt.setString(4, "RSD");
		stmnt.setString(5, transakcija.getPlacanje().getValuta());
		ResultSet resultValuta = stmnt.executeQuery();
				
		double odnos = 1;
		
		while(resultValuta.next()){
			if(resultValuta.getString("val_id_osn_val").trim().equals(transakcija.getPlacanje().getValuta()) && resultValuta.getString("val_id_prema_val").trim().equals("RSD")){
				odnos =  ( 1 / resultValuta.getDouble("kuruv_srednji") );
			}
			else if(resultValuta.getString("val_id_osn_val").trim().equals("RSD") && resultValuta.getString("val_id_prema_val").trim().equals(transakcija.getPlacanje().getValuta() )){
				odnos = resultValuta.getDouble("kuruv_srednji");
			}
		}
		stmnt.close();
			
		iznosTransakcijeRSD = Double.valueOf(transakcija.getPlacanje().getIznos()) * odnos;
		
		return -1;
	}
	
	private void obradiDuznika(){
		String[] valuesDuznik = new String[16];
		valuesDuznik[0] = transakcija.getDuznik().getNaziv();
		valuesDuznik[1] = transakcija.getPlacanje().getSvrhaPlacanja();
		valuesDuznik[2] = transakcija.getPoverilac().getNaziv();
		valuesDuznik[3] = transakcija.getPlacanje().getDatumIMestoPrijema().getDatum();
		valuesDuznik[4] = transakcija.getPlacanje().getDatumValute();
		valuesDuznik[5] = String.valueOf(transakcija.getDuznik().getRacun());
		valuesDuznik[6] = String.valueOf(transakcija.getPlacanje().getDuznik().getBrojModela());
		valuesDuznik[7] = String.valueOf(transakcija.getPlacanje().getDuznik().getPozivNaBroj());
		valuesDuznik[8] = String.valueOf(transakcija.getPoverilac().getRacun());
		valuesDuznik[9] = String.valueOf(transakcija.getPlacanje().getPoverilac().getBrojModela());
		valuesDuznik[10] = String.valueOf(transakcija.getPlacanje().getPoverilac().getPozivNaBroj());
		valuesDuznik[11] = String.valueOf(false);
		valuesDuznik[12] = transakcija.getPlacanje().getIznos();
		valuesDuznik[13] = VrstaPlacanja.ISPLATA.toString();
		valuesDuznik[14] = transakcija.getPlacanje().getValuta();
		valuesDuznik[15] = "duznik";
		
		try {
			tblmodel.dodajRed(valuesDuznik);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void obradiPoverioca(){
		String[] valuesPoverilac = new String[16];
		valuesPoverilac[0] = transakcija.getDuznik().getNaziv();
		valuesPoverilac[1] = transakcija.getPlacanje().getSvrhaPlacanja();
		valuesPoverilac[2] = transakcija.getPoverilac().getNaziv();
		valuesPoverilac[3] = transakcija.getPlacanje().getDatumIMestoPrijema().getDatum();
		valuesPoverilac[4] = transakcija.getPlacanje().getDatumValute();
		valuesPoverilac[5] = String.valueOf(transakcija.getDuznik().getRacun());
		valuesPoverilac[6] = String.valueOf(transakcija.getPlacanje().getDuznik().getBrojModela());
		valuesPoverilac[7] = String.valueOf(transakcija.getPlacanje().getDuznik().getPozivNaBroj());
		valuesPoverilac[8] = String.valueOf(transakcija.getPoverilac().getRacun());
		valuesPoverilac[9] = String.valueOf(transakcija.getPlacanje().getPoverilac().getBrojModela());
		valuesPoverilac[10] = String.valueOf(transakcija.getPlacanje().getPoverilac().getPozivNaBroj());
		valuesPoverilac[11] = String.valueOf(false);
		valuesPoverilac[12] = transakcija.getPlacanje().getIznos();
		valuesPoverilac[13] = VrstaPlacanja.UPLATA.toString();
		valuesPoverilac[14] = transakcija.getPlacanje().getValuta();
		valuesPoverilac[15] = "poverilac";
		
		try {
			tblmodel.dodajRed(valuesPoverilac);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void exportClearingAbstract() {
		if (export==null)
		export = new ExportClearingAndRTGS(this);
	
		export.exportClearing(transakcija);
		
			
	}
  
}
