package gui.xmlimport.form;

import gui.standard.form.AnalitikaIzvodaStandardForm;

import java.awt.Dimension;
import java.sql.SQLException;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import jaxb.uplatnica.poverilac.UplatnicaPoverilac;
import model.tables.AnalitikaIzvodaTableModel;

public class ImportPoverilacForm extends AbstractImportXMLForm{

	
	private static final long serialVersionUID = 1294597491206286823L;
	
	
	private UplatnicaPoverilac poverilac;
	private String[][] rows;


	public ImportPoverilacForm() {
		super("Poverilac");
		poverilac = new UplatnicaPoverilac();
		vrstaPlacanja = VrstaPlacanja.UPLATA.toString();
	}

	@Override
	public void setupLabels() {
		
		rows = new String[18][2];
		dataScrollPanel.setMinimumSize(new Dimension(400, 200));
		String[] header = {"Labela", "Podaci"};
		JTable tb = new JTable(rows, header);
		tb.getColumn("Labela").setMinWidth(100);
		tb.getColumn("Podaci").setMinWidth(200);
		dataScrollPanel = new JScrollPane(tb);
		
		rows[0][0] = "Duznik";
		rows[1][0] = "Naziv duznika";
		rows[2][0] = "Adresa duznika";
		rows[3][0] = "Racun duznika";
		
		rows[4][0] = "Poverilac";
		rows[5][0] = "Naziv poverioca";
		rows[6][0] = "Adresa poverioca";
		rows[7][0] = "Racun poverioca";
		
		rows[8][0] = "Placanje";
		rows[9][0] = "Svrha placanja";
		rows[10][0] = "Sifra placanja";
		rows[11][0] = "Valuta";
		rows[12][0] = "Iznos";
		rows[13][0] = "Broj modela";
		rows[14][0] = "Poziv na broj";
		rows[15][0] = "Datum prijema";
		rows[16][0] = "Mesto prijema";
		rows[17][0] = "Datum valute";
		
	}

	@Override
	public void unmarshallFile(String path) {
		
		poverilac = (UplatnicaPoverilac) konverter.unmarshall(new UplatnicaPoverilac(), path);
		
	}

	@Override
	public void fillLabels() {
		
		rows[0][1] = "";
		rows[1][1] = poverilac.getDuznik().getNaziv();
		rows[2][1] = poverilac.getDuznik().getAdresa().getUlica() + " " + poverilac.getDuznik().getAdresa().getBroj() + "\n " 
						+ poverilac.getDuznik().getAdresa().getPosta() + " " + poverilac.getDuznik().getAdresa().getMesto();
		
		rows[3][1] = poverilac.getDuznik().getRacun();
		
		rows[4][1] = "";
		rows[5][1] = poverilac.getPoverilac().getNaziv();
		rows[6][1] = poverilac.getPoverilac().getAdresa().getUlica() + " " + poverilac.getPoverilac().getAdresa().getBroj() + " " 
						+ poverilac.getPoverilac().getAdresa().getPosta() + " " + poverilac.getPoverilac().getAdresa().getMesto();
		rows[7][1] = poverilac.getPoverilac().getRacun();
		
		rows[8][1] = "";
		rows[9][1] = poverilac.getPlacanje().getSvrhaPlacanja().trim();
		rows[10][1] = String.valueOf(poverilac.getPlacanje().getSifraPlacanja());
		rows[11][1] = poverilac.getPlacanje().getValuta();
		rows[12][1] = String.valueOf(poverilac.getPlacanje().getIznos());
		rows[13][1] = String.valueOf(poverilac.getPlacanje().getPoverilac().getBrojModela());
		rows[14][1] = String.valueOf(poverilac.getPlacanje().getPoverilac().getPozivNaBroj());
		rows[15][1] = poverilac.getPlacanje().getDatumIMestoPrijema().getDatum();
		rows[16][1] = poverilac.getPlacanje().getDatumIMestoPrijema().getMesto();
		rows[17][1] = poverilac.getPlacanje().getDatumValute();
		
		dataScrollPanel.repaint();
		
	}

	@Override
	public void izvrsiTransakciju() {
		AnalitikaIzvodaStandardForm analitika = new AnalitikaIzvodaStandardForm();
		AnalitikaIzvodaTableModel tblmodel = (AnalitikaIzvodaTableModel) analitika.getTblModel();
		String[] values = new String[15];
		values[0] = poverilac.getDuznik().getNaziv();
		values[1] = poverilac.getPlacanje().getSvrhaPlacanja();
		values[2] = poverilac.getPoverilac().getNaziv();
		values[3] = poverilac.getPlacanje().getDatumIMestoPrijema().getDatum();
		values[4] = poverilac.getPlacanje().getDatumValute();
		values[5] = "/";
		values[6] = "/";
		values[7] = "/";
		values[8] = String.valueOf(poverilac.getPoverilac().getRacun());
		values[9] = String.valueOf(poverilac.getPlacanje().getPoverilac().getBrojModela());
		values[10] = String.valueOf(poverilac.getPlacanje().getPoverilac().getPozivNaBroj());
		values[11] = String.valueOf(false);
		values[12] = poverilac.getPlacanje().getIznos();
		values[13] = vrstaPlacanja;
		values[14] = poverilac.getPlacanje().getValuta();
		
			try {
				tblmodel.dodajRed(values);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
