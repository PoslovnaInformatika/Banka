package gui.xmlimport.form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;
import utility.UplatnicaJAXB;

public abstract class AbstractImportXMLForm extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1466301975396786350L;
	
	public enum VrstaPlacanja {
		  UPLATA {
		      public String toString() {
		          return "Uplata";
		      }
		  },

		  ISPLATA {
		      public String toString() {
		          return "Isplata";
		      }
		  },
		  
		  TRANSAKCIJA {
		      public String toString() {
		          return "Transakcija";
		      }
		  }
		}
	
	public enum VrstaTransakcije{
		POSTOJECI_RACUN_DUZNIKA(1), 
		POSTOJECI_RACUN_POVERIOCA(2),
		RACUNI_U_ISTOJ_BANCI(3),
		POSTOJECI_RACUN_NA_RACUN_DRUGE_BANKE(4),
		GRESKA_NEPOSTOJECI_RACUN(-1),
		GRESKA_RACUN_ZAMRZNUT(-2),
		GRESKA_ISTI_BROJ_RACUNA(-3)
		;

		  private int id;

		  VrstaTransakcije(int id){
		    this.id = id;
		  }

		  public int getID(){
		    return id;
		  }

		}
	
	
	protected String vrstaPlacanja;
	private JButton izvrsi, openFile;
	private JPanel mainPanel;
	protected JScrollPane dataScrollPanel;
	private JFileChooser fc;
	private File fajl;
	protected UplatnicaJAXB konverter;
	
	public AbstractImportXMLForm(String title){
		izvrsi = new JButton("Izvrsi");
		openFile = new JButton("Import");
		openFile.addActionListener(this);
		izvrsi.addActionListener(this);
		izvrsi.setEnabled(false);
		konverter = new UplatnicaJAXB();
		mainPanel = new JPanel(new MigLayout("gapx 15px"));
		dataScrollPanel = new JScrollPane();
		dataScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		setLayout(new MigLayout("fill"));
		fc = new JFileChooser("xml.examples");
		
		setSize(600, 400);
		setTitle(title);
		setLocationRelativeTo(this.getParent());
		setModal(true);
		
		setupLabels();
		
		mainPanel.add(openFile, "wrap");
		mainPanel.add(izvrsi);
		add(mainPanel, "dock east");
		add(dataScrollPanel, "dock west");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == openFile) {
            int returnVal = fc.showOpenDialog(AbstractImportXMLForm.this);
 
            if (returnVal == JFileChooser.APPROVE_OPTION) {
            	// ovde odraditi unmarshal
                fajl = fc.getSelectedFile();
                unmarshallFile(fajl.getAbsolutePath());
                fillLabels();
                izvrsi.setEnabled(true);
            }

        } else if (e.getSource() == izvrsi) {
        	izvrsiTransakciju();
        }
	}
	
	public abstract void setupLabels();
	public abstract void unmarshallFile(String path);
	public abstract void fillLabels();
	public abstract void izvrsiTransakciju();

}
