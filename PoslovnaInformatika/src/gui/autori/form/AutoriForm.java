package gui.autori.form;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.main.form.MainFrame;

public class AutoriForm extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AutoriForm(){

		setSize(new Dimension(300,450));
		setLocationRelativeTo(MainFrame.getInstance());
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		setTitle("Autori");
		GridLayout grid = new GridLayout(5, 1);
		grid.setHgap(4);
		grid.setVgap(4);
		setLayout(grid);
		setModal(true);
		InitGui();
	}

	private void InitGui() {
		JPanel panelVlada = makePanel("vlada.jpg", "Vladimir Matović", "Vladica Šećerlemica");
		JPanel panelJecka = makePanel("jelena.jpg", "Jelena Pecelj", "Plavuša");
		JPanel panelMisko = makePanel("misko.jpg", "Miodrag Gavrić", "Misko Šmekerica");
		JPanel panelSonja = makePanel("sonja.jpg", "Sonja Živković", "Šefica podzemlja");
		JPanel panelMile = makePanel("mile.jpg", "Branislav Milutinov", "Opasan momak iz kvarta");
		
		this.add(panelVlada);
		this.add(panelJecka);
		this.add(panelMisko);
		this.add(panelSonja);
		this.add(panelMile);
	}

	private JPanel makePanel(String filePath, String ime, String opis) {
		JPanel infoPanel = new JPanel(new GridLayout(1, 2));
		JLabel pic = new JLabel(new ImageIcon(getClass().getResource("/img/autori/" + filePath)));
		JPanel dataPanel = new JPanel(new GridLayout(2,1));
		JLabel name  = new JLabel(ime);
		JLabel desc   = new JLabel(opis);
		infoPanel.add(pic);
		dataPanel.add(name);
		dataPanel.add(desc);
		infoPanel.add(dataPanel);
		return infoPanel;
	}
}
