package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class FizickoLiceTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 5959728551242275269L;

	public FizickoLiceTableModel(Object[] colNames, int rowCount) {
		super(colNames, rowCount);
		basicQuery = "SELECT fl_jmbg, fl_ime, fl_prezime, fl_pol, fl_datum_rodjenja, fl_adresa, nm_sifra, fl_email, fl_telefon FROM korisnik_fl";
		orderBy = " ORDER BY fl_ime";
		whereStm = "";
	}

	@Override
	public void popuniTabelu(String sql) throws SQLException {
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("FL_JMBG"), rset.getString("FL_IME"), rset.getString("FL_PREZIME"), 
					rset.getString("FL_POL"), rset.getString("FL_DATUM_RODJENJA"), 
					rset.getString("FL_ADRESA"), rset.getString("NM_SIFRA"), rset.getString("FL_EMAIL"), rset.getString("FL_TELEFON")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();

	}

	@Override
	public void izmeniRed(int selRow, String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE korisnik_fl SET fl_ime = ?, "
				+ "fl_prezime = ?, fl_pol = ?, fl_datum_rodjenja = ?, fl_adresa = ?, nm_sifra = ?, fl_email = ?, fl_telefon = ? WHERE fl_jmbg = ?");
		
		for(int i = 1; i < values.length; i++){
			stmnt.setString(i, values[i]);
		}
		
		stmnt.setString(9, values[0]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void dodajRed(String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO korisnik_fl (fl_jmbg, fl_ime, "
				+ "fl_prezime, fl_pol, fl_datum_rodjenja, fl_adresa, nm_sifra, fl_email, fl_telefon) "
				+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		
		for(int i = 0; i < values.length; i++){
			stmnt.setString(i+1, values[i]);
		}
		
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		PreparedStatement stmntKorisnikTabela = DBConnection.getConnection().prepareStatement("INSERT INTO korisnik (k_id, pl_pib, fl_jmbg) VALUES(?, ?, ?)");
		stmntKorisnikTabela.setString(1, values[0]);
		stmntKorisnikTabela.setString(2, null);
		stmntKorisnikTabela.setString(3, values[0]);
		stmntKorisnikTabela.executeUpdate();
		stmntKorisnikTabela.close();
		
		
		DBConnection.getConnection().commit();
		
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void obrisiRed(int index) throws SQLException {
		PreparedStatement stmntKorisnikTabela = DBConnection.getConnection().prepareStatement("DELETE FROM korisnik WHERE k_id = ?");
		stmntKorisnikTabela.setString(1, (String)getValueAt(index, 0));
		stmntKorisnikTabela.executeUpdate();
		stmntKorisnikTabela.close();
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM korisnik_fl WHERE fl_jmbg = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void Pretraga(String[] values) throws SQLException {

		PreparedStatement stmnt;
		boolean allEmpty = true;
		
		for(int i = 0; i < values.length; i++){
			if(values[i].equals("") || values[i].equals("null"))
				values[i] = null;
			else
				allEmpty = false;
		}
		
		if(allEmpty)
			return;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM korisnik_fl JOIN naseljeno_mesto ON korisnik_fl.nm_sifra = naseljeno_mesto.nm_sifra "
				+ "WHERE fl_jmbg = ISNULL(?, fl_jmbg) AND fl_ime = ISNULL(?, fl_ime) AND fl_prezime = ISNULL(?, fl_prezime) "
				+ "AND fl_pol = ISNULL(?, fl_pol) AND fl_datum_rodjenja = ISNULL(?, fl_datum_rodjenja)  AND fl_adresa = ISNULL(?, fl_adresa)"
				+ "AND korisnik_fl.nm_sifra = ISNULL(?, korisnik_fl.nm_sifra) AND nm_naziv = ISNULL(?, nm_naziv) "
				+ "AND fl_email = ISNULL(?, fl_email) AND fl_telefon = ISNULL(?, fl_telefon)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("FL_JMBG"), rset.getString("FL_IME"), rset.getString("FL_PREZIME"), 
					rset.getString("FL_POL"), rset.getString("FL_DATUM_RODJENJA"), 
					rset.getString("FL_ADRESA"), rset.getString("NM_SIFRA"), rset.getString("FL_EMAIL"), rset.getString("FL_TELEFON")});
		}
		stmnt.close();
		rset.close();
		
		fireTableDataChanged();

	}

}
