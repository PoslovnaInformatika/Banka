package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class KursnaListaTableModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	
	

	public KursnaListaTableModel(Object[] colNames, int rowCount) {
		super(colNames, rowCount);
		basicQuery = "SELECT kurl_id,kurl_datum,kurl_br_kurl,kurl_prim_od,b_id FROM kursna_lista";
		orderBy = " ORDER BY b_id";
		whereStm = "";
	}

	@Override
	public void popuniTabelu(String sql) throws SQLException {
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("KURL_ID"), rset.getString("KURL_DATUM"), rset.getString("KURL_BR_KURL"), rset.getString("KURL_PRIM_OD"), rset.getString("B_ID")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
		
	}

	@Override
	public void izmeniRed(int selRow, String[] properties) throws SQLException {
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE kursna_lista SET  kurl_datum = ?, kurl_br_kurl = ?, kurl_prim_od = ?, b_id = ?  WHERE kurl_id = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[2]);
		stmnt.setString(3, properties[3]);
		stmnt.setString(4, properties[4]);
		stmnt.setString(5, properties[0]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void dodajRed(String[] values) throws SQLException {
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO kursna_lista (kurl_id, kurl_datum, kurl_br_kurl, kurl_prim_od, b_id) VALUES(?, ?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void obrisiRed(int index) throws SQLException {
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM kursna_lista WHERE kurl_id = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void Pretraga(String[] values) throws SQLException {
		PreparedStatement stmnt;
		boolean allEmpty = true;
		
		for(int i = 0; i < values.length; i++){
			if(values[i].equals("") || values[i].equals("null"))
				values[i] = null;
			else
				allEmpty = false;
		}
		
		if(allEmpty)
			return;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM kursna_lista WHERE kurl_id = ISNULL(?, kurl_id) AND kurl_datum = ISNULL(?, kurl_datum) AND kurl_br_kurl = ISNULL(?, kurl_br_kurl) AND kurl_prim_od = ISNULL(?, kurl_prim_od) AND b_id = ISNULL(?, b_id)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("KURL_ID"), rset.getString("KURL_DATUM"), rset.getString("KURL_BR_KURL"), rset.getString("KURL_PRIM_OD"), rset.getString("B_ID")});
		}
		stmnt.close();
		rset.close();

		fireTableDataChanged();
	}

}
