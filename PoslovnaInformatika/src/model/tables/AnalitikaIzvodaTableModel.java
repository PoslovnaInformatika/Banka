package model.tables;

import gui.standard.form.DnevnoStanjeRacunaStandardForm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class AnalitikaIzvodaTableModel extends AbstractTableModel {
	
	private String uplata = null;
	private String isplata = null;
	private double iznos = 0;
	private double uplataD = 0;
	private double isplataD = 0;
	private double srednjiKursValute = 1;
	
	public AnalitikaIzvodaTableModel(Object[] colNames, int rowCount) {
		super(colNames, rowCount);
		
		uplata = null;
		isplata = null;
		iznos = 0;
		
		basicQuery = "SELECT * FROM ANALITIKA_IZVODA";
		orderBy = " ORDER BY AI_BROJSTAVKE";
		whereStm = "";
}

	private static final long serialVersionUID = 1L;
	@Override
	public void popuniTabelu(String sql) throws SQLException {
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("AI_BROJSTAVKE"), rset.getString("ai_duznik"),
					rset.getString("AI_SVRHAPLACANJA"), rset.getString("AI_POVERILAC"),
					rset.getString("AI_DATUMPRIJEMA"), rset.getString("AI_DATUMVALUTE"),
					rset.getString("AI_RACUNDUZNIKA"), rset.getString("AI_MODELZADUZENJA"),
					rset.getString("AI_POZIVNABROJZADUZENJA"), rset.getString("AI_RACUNPOVERIOCA"),
					rset.getString("AI_MODELODOBRENJA"), rset.getString("AI_POZIVNABROJODOBRENJA"),
					rset.getString("AI_HITNO"), rset.getString("AI_IZNOS"),
					rset.getString("AI_VRSTAPLACANJA"), rset.getString("DSR_BROJIZVODA")
					});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}

	@Override
	public void izmeniRed(int selRow, String[] properties) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dodajRed(String[] values) throws SQLException {

		String dnevnoStanjeID = obradiDnevnoStanje(values);
		
		// ako dnevno stanje nije uspesno azurirano izbacuje gresku
		if(dnevnoStanjeID == null){
			System.out.println("Greska pri obradi dnevnog stanja.");
			return;
		}
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO ANALITIKA_IZVODA (ai_duznik, AI_SVRHAPLACANJA, AI_POVERILAC, AI_DATUMPRIJEMA, AI_DATUMVALUTE, AI_RACUNDUZNIKA, AI_MODELZADUZENJA, AI_POZIVNABROJZADUZENJA, AI_RACUNPOVERIOCA, AI_MODELODOBRENJA, AI_POZIVNABROJODOBRENJA, AI_HITNO, AI_IZNOS, AI_VRSTAPLACANJA, DSR_BROJIZVODA) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		for(int i = 0; i < values.length-2; i++)
			stmnt.setString(i+1, values[i]); 

		// ubacuje ID dnevnog stanja u analitiku izvoda
		stmnt.setString(15, dnevnoStanjeID);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void obrisiRed(int index) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Pretraga(String[] properties) throws SQLException {
		// TODO Auto-generated method stub
		
	}
	
	private String obradiDnevnoStanje(String [] values) throws SQLException{
		
		DnevnoStanjeRacunaStandardForm form = new DnevnoStanjeRacunaStandardForm();
		String racunID, valutaID,valutaSifra, banka;
		
		PreparedStatement stmntRacun = DBConnection.getConnection().prepareStatement("SELECT r_id, RACUN.val_id, VALUTE.val_sifra, b_id FROM RACUN JOIN VALUTE ON RACUN.VAL_ID = VALUTE.VAL_ID WHERE r_brracuna = ?");
		
//		if(!values[5].equals("/"))
//			stmntRacun.setString(1, values[5]);
//		else if(!values[8].equals("/"))
//			stmntRacun.setString(1, values[8]);
		if(values[15].equals("duznik"))
			stmntRacun.setString(1, values[5]);
		else if(values[15].equals("poverilac"))
			stmntRacun.setString(1, values[8]);
		else {
			System.out.println("Upatnica nije validna, ne sadrzi broj racuna.");
			return null;
		}
		
		ResultSet racun = stmntRacun.executeQuery();
		
		if(!racun.next()){
			System.out.println("Ne postoji racun u bazi.");
			return null;
		}
		else{
			racunID = racun.getString("r_id");
			valutaID = racun.getString("val_id");
			valutaSifra = racun.getString("val_sifra");
			banka = racun.getString("b_id");
		}
		
		
		PreparedStatement stmntDnevnoStanje = DBConnection.getConnection().prepareStatement("SELECT * FROM DNEVNO_STANJE_RACUNA WHERE R_ID = ? ORDER BY DSR_DATUMPROMETA DESC");
		stmntDnevnoStanje.setString(1, racunID);
		ResultSet dnvnStanje = stmntDnevnoStanje.executeQuery();
		
		if(obradiIznos(values, valutaID,valutaSifra, banka) == null){
			System.out.println("Greska pri obradi iznosa.");
			return null;
		}
		
		
		// dodavanje novog dnevnog stanja za nepostojeci racun
		if(!dnvnStanje.next()){
			
			//obradiIznos(values);
			
			form.getTblModel().dodajRed(new String[]{values[3], "0", String.valueOf(uplataD), String.valueOf(isplataD), String.valueOf(iznos), racunID});
			
			stmntDnevnoStanje.close();
			
			return getDnevnoStanjeID(racunID, values[3]);
		}
		// azuriranje vec postojeceg dnevnog stanja za racun
		else if(dnvnStanje.getString("dsr_datumprometa").equals(values[3])){
			
			//obradiIznos(values);
			
			double novaVrednost = Double.valueOf(dnvnStanje.getString("dsr_novostanje")) + Double.valueOf(iznos);
			
			form.getTblModel().izmeniRed(0, new String[]{dnvnStanje.getString("dsr_brojizvoda"), dnvnStanje.getString("dsr_novostanje"), 
					String.valueOf(uplataD), String.valueOf(isplataD), String.valueOf(novaVrednost)});
			
			String dnevnoStanjeID = dnvnStanje.getString("dsr_brojizvoda");
			stmntDnevnoStanje.close();
			
			return dnevnoStanjeID;
		}
		// dodavanje novog dnevnog stanja za nepostojeci datum postojeceg racuna
		else if(!dnvnStanje.getString("dsr_datumprometa").equals(values[3])){
			
			double novaVrednost = Double.valueOf(dnvnStanje.getString("dsr_novostanje")) + Double.valueOf(iznos);
			
			form.getTblModel().dodajRed(new String[]{values[3], dnvnStanje.getString("dsr_novostanje"), uplata, isplata, 
					String.valueOf(novaVrednost), racunID});
			
			stmntDnevnoStanje.close();
			
			return getDnevnoStanjeID(racunID, values[3]);
		}
		
		stmntDnevnoStanje.close();
		return null;
		
	}
	
	// pronalazi i vraca sifru dnevnog stanja racuna zavisno od racuna i datuma
	
	private String getDnevnoStanjeID(String racunID, String datum) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("SELECT dsr_brojizvoda FROM dnevno_stanje_racuna "
																		+ "WHERE r_id = ? and dsr_datumprometa = ?");
		stmnt.setString(1, racunID);
		stmnt.setString(2, datum);
		ResultSet dnvnst = stmnt.executeQuery();
		
		if(!dnvnst.next()){
			System.out.println("Ne postoji dnevno stanje.");
			return null;
		}
		else {
			return dnvnst.getString("dsr_brojizvoda");
		}
	}
	
	// Uzima uplatu i isplatu iz dolazeceg niza vrednosti, ubacuje ih u lokalne promenljive, 
	//  konvertuje po potrebi vrednosti u valutu racuna i racuna sumu koju treba dodati ili oduzeti od stanja racuna
	
	private String obradiIznos(String[] values, String valutaID, String valutaSifra, String banka) throws SQLException{
		srednjiKursValute = 1;
		
		// u slucaju da se valute razlikuju, konvertuje valutu koja se uplacuje u valutu racuna na koji se uplacuje
		if(!values[14].equals(valutaSifra)){
			
			// query za dobijanje kursne liste za banku u kojoj se desava transakcija
			PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("SELECT kurl_id FROM kursna_lista "
					+ "WHERE b_id = ? and kurl_datum = ? ");//<= ? ORDER BY kurl_prim_od DESC");
			stmnt.setString(1, banka);
			stmnt.setString(2, values[4]);
			ResultSet result = stmnt.executeQuery();
			if(!result.next()){
				System.out.println("Kursna lista sa trazenim datumom ne postoji.");
				stmnt.close();
				return null;
			}
			
			// query za dobijanje vrednosti valute koju konvertujemo u odnosu na valutu racuna
			stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM kurs_u_valuti "
					+ "WHERE kurl_val_u_listi = ? and (val_id_osn_val = ? or val_id_osn_val = ?) and (val_id_prema_val = ? or val_id_prema_val = ?) ");
		
			stmnt.setString(1, result.getString("kurl_id"));
			stmnt.setString(2, values[14]);
			stmnt.setString(3, valutaSifra);
			stmnt.setString(4, values[14]);
			stmnt.setString(5, valutaSifra);
			ResultSet resultValuta = stmnt.executeQuery();
			
			int check = 0;
			
			while(resultValuta.next()){
				if(resultValuta.getString("val_id_osn_val").trim().equals(values[14]) && resultValuta.getString("val_id_prema_val").trim().equals(valutaSifra)){
					srednjiKursValute *= ( 1 / resultValuta.getDouble("kuruv_srednji") );
					check = -1;
					break;
				}
				else if(resultValuta.getString("val_id_osn_val").trim().equals(valutaSifra) && resultValuta.getString("val_id_prema_val").trim().equals(values[14])){
					srednjiKursValute *= resultValuta.getDouble("kuruv_srednji");
					check = -1;
					break;
				}
			}
			
			if(check != -1){
				System.out.println("Valute za konverziju nisu pronadjene u listi Kurs_u_valuti.");
				stmnt.close();
				return null;
			}
			
			stmnt.close();
		}
		
//		if(values[5].equals("/")){
//			uplata = values[12];
//			isplata = "0";
//		}
//		else if(values[8].equals("/")){
//			uplata = "0";
//			isplata = values[12];
//		}
		
		if(values[15].equals("poverilac")){
			uplata = values[12];
			isplata = "0";
		}
		else if(values[15].equals("duznik")){
			uplata = "0";
			isplata = values[12];
		}
		
		uplataD = Double.valueOf(uplata);
		isplataD = Double.valueOf(isplata);
		
		iznos = (uplataD - isplataD) * srednjiKursValute;
		
		uplataD *= srednjiKursValute;
		isplataD *= srednjiKursValute;
		
		return "OK";
	}
}