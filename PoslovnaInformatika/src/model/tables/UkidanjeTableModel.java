package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class UkidanjeTableModel  extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	
	public UkidanjeTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT ukidanje_id, ukidanje_datum, r_id FROM ukidanje";
		orderBy = " ORDER BY ukidanje_id";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("UKIDANJE_ID"), rset.getString("UKIDANJE_DATUM"), rset.getString("R_ID")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO ukidanje (ukidanje_id, ukidanje_datum, r_id) VALUES(?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		
	}
	
	public void Pretraga(String [] values) throws SQLException{
		
	}
}
