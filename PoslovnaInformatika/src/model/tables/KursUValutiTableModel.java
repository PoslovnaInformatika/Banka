package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class KursUValutiTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	public KursUValutiTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT kuruv_redni_br, kuruv_kupovni, kuruv_srednji, kuruv_prodajni, val_id_osn_val, val_id_prema_val, kurl_val_u_listi FROM kurs_u_valuti";
		orderBy = " ORDER BY kuruv_redni_br";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("KURUV_REDNI_BR"), rset.getString("KURUV_KUPOVNI"), rset.getString("KURUV_SREDNJI"), rset.getString("KURUV_PRODAJNI"), rset.getString("VAL_ID_OSN_VAL"), rset.getString("VAL_ID_PREMA_VAL"), rset.getString("KURL_VAL_U_LISTI")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE kurs_u_valuti SET  kuruv_kupovni = ?, kuruv_srednji=?, kuruv_prodajni= ?, val_id_osn_val = ?, val_id_prema_val=?, kurl_val_u_listi= ?  WHERE kuruv_redni_br = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[2]);
		stmnt.setString(3, properties[3]);
		stmnt.setString(4, properties[4]);
		stmnt.setString(5, properties[5]);
		stmnt.setString(6, properties[6]);
		stmnt.setString(7, properties[0]);
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO kurs_u_valuti (kuruv_redni_br, kuruv_kupovni, kuruv_srednji, kuruv_prodajni, val_id_osn_val, val_id_prema_val, kurl_val_u_listi) VALUES(?, ?, ?, ?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM kurs_u_valuti WHERE kuruv_redni_br = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String [] values) throws SQLException{
		PreparedStatement stmnt;
		
		for(int i = 0; i < values.length; i++)
			if(values[i].equals(""))
				values[i] = null;
			
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM kurs_u_valuti "
				+ "WHERE kuruv_redni_br = ISNULL(?, kuruv_redni_br) AND kuruv_kupovni = ISNULL(?, kuruv_kupovni) AND kuruv_srednji = ISNULL(?, kuruv_srednji) AND kuruv_prodajni = ISNULL(?, kuruv_prodajni) "
				+ "AND val_id_osn_val = ISNULL(?, val_id_osn_val) AND val_id_prema_val = ISNULL(?, val_id_prema_val) AND kurl_val_u_listi = ISNULL(?, kurl_val_u_listi)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
			
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("KURUV_REDNI_BR"), rset.getString("KURUV_KUPOVNI"), rset.getString("KURUV_SREDNJI"), rset.getString("KURUV_PRODAJNI"), rset.getString("VAL_ID_OSN_VAL"), rset.getString("VAL_ID_PREMA_VAL"), rset.getString("KURL_VAL_U_LISTI")});
		}
		stmnt.close();
		rset.close();
		
		fireTableDataChanged();
	}
}
