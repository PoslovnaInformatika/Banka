package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class RacunTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 4548445544483287397L;

	public RacunTableModel(Object[] colNames, int rowCount) {
		super(colNames, rowCount);
		basicQuery = "SELECT r_id, r_brracuna, r_datumotvaranja, r_vazeci, b_id, k_id, val_id FROM racun";
		orderBy = " ORDER BY r_brracuna";
		whereStm = "";
	}

	@Override
	public void popuniTabelu(String sql) throws SQLException {
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		String vazeci;
		
		while(rset.next()){
			if(rset.getString("R_VAZECI").equals("1"))
				vazeci = "Da";
			else
				vazeci = "Ne";
			
			addRow(new String[]{rset.getString("R_ID"), rset.getString("R_BRRACUNA"), rset.getString("R_DATUMOTVARANJA"), 
					vazeci, rset.getString("B_ID"), rset.getString("K_ID"), rset.getString("VAL_ID")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();

	}

	@Override
	public void izmeniRed(int selRow, String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE racun SET r_brracuna = ?, "
				+ "r_datumotvaranja = ?, r_vazeci = ?, b_id = ?, k_id = ?, val_id = ? WHERE r_id = ?");
		
		for(int i = 1; i < values.length; i++){
			stmnt.setString(i, values[i]);
		}
		
		stmnt.setString(7, values[0]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void dodajRed(String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO racun (r_id, r_brracuna,"
				+ " r_datumotvaranja, r_vazeci, b_id, k_id, val_id) "
				+ "VALUES(?, ?, ?, ?, ?, ?, ?)");
		
		
		for(int i = 0; i < values.length; i++){
			stmnt.setString(i+1, values[i]);
		}
		
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		DBConnection.getConnection().commit();
		
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void obrisiRed(int index) throws SQLException {
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM racun WHERE r_id = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}

	}

	@Override
	public void Pretraga(String[] values) throws SQLException {
		PreparedStatement stmnt;
		boolean allEmpty = true;
		
		for(int i = 0; i < values.length; i++){
			if(values[i].equals("") || values[i].equals("null"))
				values[i] = null;
			else
				allEmpty = false;
		}
		
		if(allEmpty)
			return;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM RACUN "
				+ "WHERE R_ID = ISNULL(?, R_ID) AND R_BRRACUNA = ISNULL(?, R_BRRACUNA) "
				+ "AND R_DATUMOTVARANJA = ISNULL(?, R_DATUMOTVARANJA) AND R_VAZECI = ISNULL(?, R_VAZECI)"
				+ "AND B_ID = ISNULL(?, B_ID) AND K_ID = ISNULL(?, K_ID) AND VAL_ID = ISNULL(?, VAL_ID)" );
		
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		String vazeci;
		
		while(rset.next()){
			
			if(rset.getString("R_VAZECI").equals("1"))
				vazeci = "Da";
			else
				vazeci = "Ne";
			
			addRow(new String[]{rset.getString("R_ID"), rset.getString("R_BRRACUNA"), rset.getString("R_DATUMOTVARANJA"), 
					vazeci, rset.getString("B_ID"), rset.getString("K_ID"), rset.getString("VAL_ID")});
		}
		stmnt.close();
		rset.close();
		
		fireTableDataChanged();

	}

}
