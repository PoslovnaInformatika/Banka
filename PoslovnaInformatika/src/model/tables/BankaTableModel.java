package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class BankaTableModel extends AbstractTableModel {
private static final long serialVersionUID = 1L;
	
	public BankaTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT b_id, b_sifra, b_pib, b_naziv, b_adresa, b_email, b_web, b_telefon, b_fax FROM banka";
		orderBy = " ORDER BY b_id";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("B_ID"), rset.getString("B_SIFRA"), rset.getString("B_PIB"), rset.getString("B_NAZIV"), rset.getString("B_ADRESA"), rset.getString("B_EMAIL"), rset.getString("B_WEB"), rset.getString("B_TELEFON"), rset.getString("B_FAX")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE banka SET  b_sifra = ?, b_pib = ?, b_naziv = ?, b_adresa = ?, b_email = ?, b_web = ?, b_telefon = ?, b_fax = ?  WHERE b_id = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[2]);
		stmnt.setString(3, properties[3]);
		stmnt.setString(4, properties[4]);
		stmnt.setString(5, properties[5]);
		stmnt.setString(6, properties[6]);
		stmnt.setString(7, properties[7]);
		stmnt.setString(8, properties[8]);
		stmnt.setString(9, properties[0]);
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO banka (b_id, b_sifra, b_pib, b_naziv, b_adresa, b_email, b_web, b_telefon, b_fax) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
		
		
		
	}
	
	public void obrisiRed(int index) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM banka WHERE b_id = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String [] values) throws SQLException{
		PreparedStatement stmnt;
		boolean allEmpty = true;
		
		for(int i = 0; i < values.length; i++){
			if(values[i].equals("") || values[i].equals("null"))
				values[i] = null;
			else
				allEmpty = false;
		}
		
		if(allEmpty)
			return;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM banka WHERE b_id = ISNULL(?, b_id) AND b_sifra = ISNULL(?, b_sifra) AND b_pib = I SNULL(?, b_pib)AND b_naziv = ISNULL(?, b_naziv) AND b_adresa = ISNULL(?, b_adresa) AND b_email = ISNULL(?, b_email)	 AND b_web = ISNULL(?, b_web) AND b_telefon = ISNULL(?, b_telefon) AND b_fax = ISNULL(?, b_fax)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("B_ID"), rset.getString("B_SIFRA"), rset.getString("B_PIB"), rset.getString("B_NAZIV"), rset.getString("B_ADRESA"), rset.getString("B_EMAIL"), rset.getString("B_WEB"), rset.getString("B_TELEFON"), rset.getString("B_FAX")});
		}
		stmnt.close();
		rset.close();

		fireTableDataChanged();
	}	
}
