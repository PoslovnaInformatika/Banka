package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class DrzaveTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	
	public DrzaveTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT dr_sifra, dr_naziv FROM drzava";
		orderBy = " ORDER BY dr_sifra";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("DR_SIFRA"), rset.getString("DR_NAZIV")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE drzava SET dr_naziv = ? WHERE dr_sifra = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[0]);
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO drzava (dr_sifra, dr_naziv) VALUES(?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM drzava WHERE dr_sifra = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String [] values) throws SQLException{
		PreparedStatement stmnt;

		for(int i = 0; i < values.length; i++)
			if(values[i].equals(""))
				values[i] = null;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM drzava WHERE dr_sifra = ISNULL(?, dr_sifra) AND dr_naziv = ISNULL(?, dr_naziv)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("DR_SIFRA"), rset.getString("DR_NAZIV")});
		}
		stmnt.close();
		rset.close();

		fireTableDataChanged();
	}
}
