package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class PravnaLicaTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -3112995938426904530L;
	
	public PravnaLicaTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT pl_pib, pl_naziv, pl_adresa, nm_sifra, pl_web, pl_email, pl_telefon, pl_fax FROM korisnik_pl";
		orderBy = " ORDER BY pl_naziv";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("PL_PIB"), rset.getString("PL_NAZIV"), rset.getString("PL_ADRESA"), 
					rset.getString("NM_SIFRA"), rset.getString("PL_EMAIL"), 
					rset.getString("PL_TELEFON"), rset.getString("PL_WEB"), rset.getString("PL_FAX")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String[] values) throws SQLException{
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE korisnik_pl SET pl_naziv = ?, pl_adresa = ?, "
				+ "nm_sifra = ?, pl_email = ?, pl_telefon = ?, pl_web = ?, pl_fax = ? WHERE pl_pib = ?");
		
		stmnt.setString(1, values[1]);
		stmnt.setString(2, values[2]);
		stmnt.setString(3, values[3]);
		stmnt.setString(4, values[4]);
		stmnt.setString(5, values[5]);
		stmnt.setString(6, values[6]);
		stmnt.setString(7, values[7]);
		stmnt.setString(8, values[0]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String[] values) throws SQLException{
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO korisnik_pl (pl_pib, pl_naziv, pl_adresa, "
				+ "nm_sifra, pl_email, pl_telefon, pl_web, pl_fax ) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		PreparedStatement stmntKorisnikTabela = DBConnection.getConnection().prepareStatement("INSERT INTO korisnik (k_id, pl_pib, fl_jmbg) VALUES(?, ?, ?)");
		stmntKorisnikTabela.setString(1, values[0]);
		stmntKorisnikTabela.setString(2, values[0]);
		stmntKorisnikTabela.setString(3, null);
		stmntKorisnikTabela.executeUpdate();
		stmntKorisnikTabela.close();
		
		
		DBConnection.getConnection().commit();
		
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		
		PreparedStatement stmntKorisnikTabela = DBConnection.getConnection().prepareStatement("DELETE FROM korisnik WHERE k_id = ?");
		stmntKorisnikTabela.setString(1, (String)getValueAt(index, 0));
		stmntKorisnikTabela.executeUpdate();
		stmntKorisnikTabela.close();
		
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM korisnik_pl WHERE pl_pib = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String[] values) throws SQLException{
		
		PreparedStatement stmnt;
		boolean allEmpty = true;
		
		for(int i = 0; i < values.length; i++){
			if(values[i].equals(""))
				values[i] = null;
			else
				allEmpty = false;
		}
		
		if(allEmpty)
			return;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM korisnik_pl JOIN naseljeno_mesto ON korisnik_pl.nm_sifra = naseljeno_mesto.nm_sifra "
				+ "WHERE pl_pib = ISNULL(?, pl_pib) AND pl_naziv = ISNULL(?, pl_naziv) AND pl_adresa = ISNULL(?, pl_adresa) "
				+ "AND korisnik_pl.nm_sifra = ISNULL(?, korisnik_pl.nm_sifra) AND nm_naziv = ISNULL(?, nm_naziv) "
				+ "AND pl_email = ISNULL(?, pl_email) AND pl_telefon = ISNULL(?, pl_telefon)"
				+ "AND pl_web = ISNULL(?, pl_web) AND pl_fax = ISNULL(?, pl_fax)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("PL_PIB"), rset.getString("PL_NAZIV"), rset.getString("PL_ADRESA"), 
					rset.getString("NM_SIFRA"), rset.getString("PL_EMAIL"), 
					rset.getString("PL_TELEFON"), rset.getString("PL_WEB"), rset.getString("PL_FAX")});
		}
		stmnt.close();
		rset.close();
		
		fireTableDataChanged();
		
	}
}
