package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class ValuteTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	
	public ValuteTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT val_id, val_sifra, val_naziv, dr_sifra FROM valute";
		orderBy = " ORDER BY val_id";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("VAL_ID"), rset.getString("VAL_SIFRA"), rset.getString("VAL_NAZIV"), rset.getString("DR_SIFRA")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE valute SET val_sifra = ?, val_naziv = ?, dr_sifra = ? WHERE val_id = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[2]);
		stmnt.setString(3, properties[3]);
		stmnt.setString(4, properties[0]);
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO valute (val_id, val_sifra, val_naziv, dr_sifra) VALUES(?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM valute WHERE val_id = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String [] values) throws SQLException{
		PreparedStatement stmnt;

		for(int i = 0; i < values.length; i++)
			if(values[i].equals(""))
				values[i] = null;
		
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM valute WHERE val_id = ISNULL(?, val_id) AND val_sifra = ISNULL(?, val_sifra) AND val_naziv = ISNULL(?, val_naziv) AND dr_sifra = ISNULL(?, dr_sifra)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("VAL_ID"), rset.getString("VAL_SIFRA"), rset.getString("VAL_NAZIV"), rset.getString("DR_SIFRA")});
		}
		stmnt.close();
		rset.close();

		fireTableDataChanged();
	}
}
