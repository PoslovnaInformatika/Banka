package model.tables;

import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public abstract class AbstractTableModel extends DefaultTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String basicQuery;
	protected String orderBy;
	protected String whereStm;
	
	public AbstractTableModel(Object[] colNames, int rowCount)
	{
		super(colNames, rowCount);
	}
	
	public abstract void popuniTabelu(String sql) throws SQLException;
	public void open() throws SQLException
	{
		popuniTabelu(basicQuery + whereStm + orderBy);
	};
	
	public abstract void izmeniRed(int selRow, String [] properties) throws SQLException;
	public abstract void dodajRed(String [] properties) throws SQLException;
	public abstract void obrisiRed(int index) throws SQLException;
	public abstract void Pretraga(String [] properties) throws SQLException;
	
	public void Refresh() throws SQLException
	{
		open();
		fireTableDataChanged();
	};
	
	@Override
	public boolean isCellEditable(int row, int column){
		return false;
	}

	public String getBasicQuery() {
		return basicQuery;
	}

	public void setBasicQuery(String basicQuery) {
		this.basicQuery = basicQuery;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getWhereStm() {
		return whereStm;
	}

	public void setWhereStm(String whereStm) {
		this.whereStm = whereStm;
	}
}
