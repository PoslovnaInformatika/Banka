package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class DnevnoStanjeRacunaTableModel extends AbstractTableModel {

	public DnevnoStanjeRacunaTableModel(Object[] colNames, int rowCount) {
		super(colNames, rowCount);
		basicQuery = "SELECT * FROM dnevno_stanje_racuna";
		orderBy = " ORDER BY dsr_brojizvoda";
		whereStm = "";
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void popuniTabelu(String sql) throws SQLException {
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("DSR_BROJIZVODA"), rset.getString("DSR_DATUMPROMETA"), 
					rset.getString("DSR_PRETHODNOSTANJE"), rset.getString("DSR_PROMETUKORIST"), 
					rset.getString("DSR_PROMETNATERET"), rset.getString("DSR_NOVOSTANJE")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
		
	}

	@Override
	public void izmeniRed(int selRow, String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE DNEVNO_STANJE_RACUNA SET "
				+ "DSR_PRETHODNOSTANJE = ?, DSR_PROMETUKORIST = ?, DSR_PROMETNATERET = ?, DSR_NOVOSTANJE = ? WHERE "
				+ "DSR_BROJIZVODA = ?");
		
		for(int i = 1; i < values.length; i++){
			stmnt.setString(i, values[i]);
		}
		
		stmnt.setString(5, values[0]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void dodajRed(String[] values) throws SQLException {
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO DNEVNO_STANJE_RACUNA (dsr_datumprometa,"
				+ " dsr_prethodnostanje, dsr_prometukorist, dsr_prometnateret, dsr_novostanje, r_id) "
				+ "VALUES(?, ?, ?, ?, ?, ?)");
		
		
		for(int i = 0; i < values.length; i++){
			stmnt.setString(i+1, values[i]);
		}
		
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		
		DBConnection.getConnection().commit();
		
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
		
	}

	@Override
	public void obrisiRed(int index) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Pretraga(String[] properties) throws SQLException {
		// TODO Auto-generated method stub
		
	}
}
