package model.tables;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnection;

public class NaseljenaMestaTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	public NaseljenaMestaTableModel(Object[] colNames, int rowCount){
		super(colNames, rowCount);
		basicQuery = "SELECT nm_sifra, dr_sifra, nm_naziv, nm_pttoznaka FROM naseljeno_mesto";
		orderBy = " ORDER BY nm_sifra";
		whereStm = "";
	}
	
	public void popuniTabelu(String sql) throws SQLException{
		setRowCount(0);
		Statement stmnt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmnt.executeQuery(sql);
		
		while(rset.next()){
			addRow(new String[]{rset.getString("NM_SIFRA"), rset.getString("NM_NAZIV"), rset.getString("NM_PTTOZNAKA"), rset.getString("DR_SIFRA")});
		}
		
		rset.close();
		stmnt.close();
		fireTableDataChanged();
	}
	
	public void izmeniRed(int selRow, String [] properties) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("UPDATE naseljeno_mesto SET  nm_naziv = ?, nm_pttoznaka=?, dr_sifra= ?  WHERE nm_sifra = ?");
		stmnt.setString(1, properties[1]);
		stmnt.setString(2, properties[2]);
		stmnt.setString(3, properties[3]);
		stmnt.setString(4, properties[0]);
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void dodajRed(String [] values) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("INSERT INTO naseljeno_mesto (nm_sifra, nm_naziv, nm_pttoznaka, dr_sifra) VALUES(?, ?, ?, ?)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
		
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void obrisiRed(int index) throws SQLException{
		PreparedStatement stmnt = DBConnection.getConnection().prepareStatement("DELETE FROM naseljeno_mesto WHERE nm_sifra = ?");
		stmnt.setString(1, (String)getValueAt(index, 0));
		int rowsAffected = stmnt.executeUpdate();
		stmnt.close();
		DBConnection.getConnection().commit();
		if(rowsAffected > 0){
			open();
			fireTableDataChanged();
		}
	}
	
	public void Pretraga(String [] values) throws SQLException{
		PreparedStatement stmnt;
		
		for(int i = 0; i < values.length; i++)
			if(values[i].equals(""))
				values[i] = null;
			
		stmnt = DBConnection.getConnection().prepareStatement("SELECT * FROM naseljeno_mesto INNER JOIN drzava ON naseljeno_mesto.dr_sifra = drzava.dr_sifra "
				+ "WHERE nm_sifra = ISNULL(?, nm_sifra) AND nm_naziv = ISNULL(?, nm_naziv) AND nm_pttoznaka = ISNULL(?, nm_pttoznaka) AND naseljeno_mesto.dr_sifra = ISNULL(?, naseljeno_mesto.dr_sifra) AND dr_naziv = ISNULL(?, dr_naziv)");
		
		for(int i = 0; i < values.length; i++)
			stmnt.setString(i+1, values[i]);
			
		ResultSet rset = stmnt.executeQuery();
		setRowCount(0);
		while(rset.next()){
			addRow(new String[]{rset.getString("NM_SIFRA"), rset.getString("NM_NAZIV"), rset.getString("NM_PTTOZNAKA"), rset.getString("DR_SIFRA")});
		}
		stmnt.close();
		rset.close();
		
		fireTableDataChanged();
	}
}
