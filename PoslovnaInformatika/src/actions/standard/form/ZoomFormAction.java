package actions.standard.form;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.standard.form.AbstractStandardForm;


public class ZoomFormAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private AbstractStandardForm standardForm;
	private String btnName;
	
	public ZoomFormAction(AbstractStandardForm standardForm, String btnName) {
		putValue(SHORT_DESCRIPTION, "Zoom");
		putValue(NAME, "...");
		this.standardForm=standardForm;
		this.btnName = btnName;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		standardForm.zoomForm(btnName);
	}
	
}
