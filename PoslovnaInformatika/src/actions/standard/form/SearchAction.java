package actions.standard.form;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import gui.standard.form.AbstractStandardForm;

public class SearchAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private AbstractStandardForm standardForm;

	public SearchAction(AbstractStandardForm forma) {
		putValue(SMALL_ICON, new ImageIcon(getClass().getResource("/img/search.gif")));
		putValue(SHORT_DESCRIPTION, "Pretraga");
		this.standardForm=forma;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		standardForm.Search();
	}
}
