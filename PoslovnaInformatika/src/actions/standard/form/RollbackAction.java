
package actions.standard.form;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import gui.standard.form.AbstractStandardForm;

public class RollbackAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private AbstractStandardForm standardForm;

	public RollbackAction(AbstractStandardForm standardForm) {
		putValue(SMALL_ICON, new ImageIcon(getClass().getResource("/img/remove.gif")));
		putValue(SHORT_DESCRIPTION, "Poništi");
		this.standardForm=standardForm;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		standardForm.Rollback();
	}
}
