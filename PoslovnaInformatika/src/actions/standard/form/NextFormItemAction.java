package actions.standard.form;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.standard.form.AbstractStandardForm;


public class NextFormItemAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private AbstractStandardForm standardForm;
	private String title;
	
	public NextFormItemAction(String title, AbstractStandardForm standardForm) {
		putValue(SHORT_DESCRIPTION, title);
		putValue(NAME, title);
		this.standardForm  = standardForm;
		this.title = title;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		standardForm.nextForm(title);
	}
}
