package actions.standard.form;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class HelpAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public HelpAction() {
		putValue(SMALL_ICON, new ImageIcon(getClass().getResource("/img/help.gif")));
		putValue(SHORT_DESCRIPTION, "Pomoc");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			
			Runtime.getRuntime().exec("hh.exe Help/Help.chm");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
