package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class ExitAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExitAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Izlaz");
		putValue(NAME, "Izlaz");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}
}
