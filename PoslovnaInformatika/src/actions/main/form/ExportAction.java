package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import export.ExportForm;

public class ExportAction extends AbstractAction{
private static final long serialVersionUID = 1L;
	
	public ExportAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Export analitike izvoda korisnika");
		putValue(NAME, "Export analitike izvoda korisnika");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ExportForm form = new ExportForm();
		form.setVisible(true);
	}
}
