package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import gui.autori.form.AutoriForm;

public class AutoriAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AutoriAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Autori");
		putValue(NAME, "Autori");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		AutoriForm autori = new AutoriForm();
		autori.setVisible(true);
	}
}
