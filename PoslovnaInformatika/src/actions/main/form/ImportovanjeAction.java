package actions.main.form;

import gui.xmlimport.form.AbstractImportXMLForm;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class ImportovanjeAction extends AbstractAction {

	private static final long serialVersionUID = 2255868017319452333L;

	private AbstractImportXMLForm form;
	private boolean firstCall;
	
	public ImportovanjeAction(String name, int keyCode, AbstractImportXMLForm form) {
		this.form = form;
		firstCall = false;
		
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(keyCode, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Uplatnica" + name);
		putValue(NAME, name);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(firstCall)
			try {
				form = form.getClass().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		form.setVisible(true);
		firstCall = true;
	}

}
