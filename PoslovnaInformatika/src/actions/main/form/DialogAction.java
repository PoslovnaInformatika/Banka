package actions.main.form;

import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import gui.standard.form.AbstractStandardForm;

public class DialogAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private AbstractStandardForm form;
	
	public DialogAction(String title, int keyCode, AbstractStandardForm form) {
		this.form = form;
		
		KeyStroke ctrlKeyStroke = KeyStroke.getKeyStroke(keyCode, ActionEvent.CTRL_MASK);
		putValue(ACCELERATOR_KEY,ctrlKeyStroke);
		putValue(SHORT_DESCRIPTION, title);
		putValue(NAME, title);		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			form.getTblModel().Refresh();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		form.setVisible(true);
	}
}
