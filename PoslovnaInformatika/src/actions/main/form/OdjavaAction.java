package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import gui.login.form.LoginDialog;
import gui.main.form.MainFrame;

public class OdjavaAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OdjavaAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Odjava");
		putValue(NAME, "Odjava");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LoginDialog dialog = new LoginDialog(MainFrame.getInstance());
		dialog.setVisible(true);
	}

}
