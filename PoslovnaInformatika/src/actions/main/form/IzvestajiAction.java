package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import izvestaji.IzvestajForm;

public class IzvestajiAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public IzvestajiAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Izveštaji");
		putValue(NAME, "Izveštaji");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		IzvestajForm form = new IzvestajForm();
		form.setVisible(true);
	}
}
