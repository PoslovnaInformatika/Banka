package actions.main.form;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class HelpAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HelpAction() {
		KeyStroke shiftKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.SHIFT_MASK);
		putValue(ACCELERATOR_KEY,shiftKeyStroke);
		putValue(SHORT_DESCRIPTION, "Pomoć");
		putValue(NAME, "Pomoć");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			
			Runtime.getRuntime().exec("hh.exe Help/Help.chm");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getSource(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
