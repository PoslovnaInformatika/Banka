package izvestaji;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import database.DBConnection;
import gui.main.form.MainFrame;
import gui.standard.form.BankaStandardForm;
import gui.standard.form.RacunStandardForm;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import utility.DateLabelFormatter;

public class IzvestajForm extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel odabirBankeLabel;
	private JTextField bankaTf;
	private JButton createReportButton;
	private JButton zoomButton;
	
	private JLabel lblDatumOd;
	private SqlDateModel modelDatumOd;
	private JDatePanelImpl panelDatumOd;
	private JDatePickerImpl pickerDatumOd;
	
	private JLabel lblDatumDo;
	private SqlDateModel modelDatumDo;
	private JDatePanelImpl panelDatumDo;
	private JDatePickerImpl pickerDatumDo;
	
	private JLabel odabirRacunaLabel;
	private JTextField racunTf;
	private JButton createReportButton1;
	private JButton zoomButton1;
	
	public IzvestajForm()
	{
		this.setIconImage(new ImageIcon(getClass().getResource("/img/svinja.png")).getImage());
		this.setTitle("Izveštaji");
		setLayout(new MigLayout("gapx 15px"));
		setSize(new Dimension(200,380));
		setLocationRelativeTo(MainFrame.getInstance());
		setResizable(false);
		setModal(true);
		
		JPanel dataPanel = new JPanel();
		dataPanel.setMaximumSize(new Dimension(250, 250));
		dataPanel.setLayout(new BorderLayout());
		
		odabirBankeLabel = new JLabel("Banka: ");
		bankaTf = new JTextField(13);
		bankaTf.setMinimumSize(new Dimension(75, 30));
		zoomButton = new JButton("...");
		zoomButton.setMaximumSize(new Dimension(70, 30));
		createReportButton = new JButton("Prikaz izveštaja");
		
		JPanel bankInput = new JPanel();
		bankInput.setLayout(new MigLayout("gapx 15px"));
		bankInput.add(odabirBankeLabel, "wrap");
		bankInput.add(bankaTf);
		bankInput.add(zoomButton, "wrap");
		dataPanel.add(bankInput, BorderLayout.NORTH);
		dataPanel.add(createReportButton, BorderLayout.SOUTH);
		dataPanel.setBorder(BorderFactory.createTitledBorder("Spisak računa sa stanjem")) ;
		add(dataPanel, BorderLayout.SOUTH);
		
		
		lblDatumOd = new JLabel("Od datuma:");
		modelDatumOd = new SqlDateModel();
		modelDatumOd.setSelected(true);
		modelDatumOd.setValue(null);
		panelDatumOd = new JDatePanelImpl(modelDatumOd, new Properties());
		pickerDatumOd = new JDatePickerImpl(panelDatumOd, new DateLabelFormatter());
		pickerDatumOd.setShowYearButtons(true);
		
		lblDatumDo = new JLabel("Do datuma:");
		modelDatumDo = new SqlDateModel();
		modelDatumDo.setSelected(true);
		modelDatumDo.setValue(null);
		panelDatumDo = new JDatePanelImpl(modelDatumDo, new Properties());
		pickerDatumDo = new JDatePickerImpl(panelDatumDo, new DateLabelFormatter());
		pickerDatumDo.setShowYearButtons(true);
		
		
		odabirRacunaLabel = new JLabel("Racun: ");
		racunTf = new JTextField(11);
		zoomButton1 = new JButton("...");
		createReportButton1 = new JButton("Prikaz izveštaja");
		
		JPanel dataPanel1 = new JPanel(new BorderLayout());
		JPanel panel1 = new JPanel();
		panel1.setLayout(new MigLayout("gapx 15px"));
		panel1.add(lblDatumOd, "wrap");
		panel1.add(pickerDatumOd, "wrap");
		panel1.add(lblDatumDo, "wrap");
		panel1.add(pickerDatumDo, "wrap");
		panel1.add(odabirRacunaLabel, "wrap");
		
		JPanel pan = new JPanel(new BorderLayout());
		pan.setMaximumSize(new Dimension(200, 30));
		pan.add(racunTf, BorderLayout.WEST);
		pan.add(zoomButton1, BorderLayout.EAST);
		panel1.add(pan, "wrap");
		dataPanel1.add(panel1);
		createReportButton1.setMaximumSize(new Dimension(100, 30));
		dataPanel1.add(createReportButton1, BorderLayout.SOUTH);
		
		dataPanel1.setBorder(BorderFactory.createTitledBorder("Izvod klijenta")) ;
		add(new JPanel(), BorderLayout.CENTER);
		add(dataPanel1, BorderLayout.NORTH);
		
		zoomButton.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				BankaStandardForm bankaForm = new BankaStandardForm();
				bankaForm.setVisible(true);
				bankaTf.setText(bankaForm.getTfID().getText());
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		createReportButton.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(validateInputRacuni() == false) return;
				
				String pathJrxml = new File("jrxml/SpisakRacuna.jrxml").getAbsolutePath();
				
				try {
					JasperDesign design = JRXmlLoader.load(pathJrxml);
					JRDesignQuery query = (JRDesignQuery) design.getQuery();
					String queryString = query.getText();
					queryString = queryString.replace("BANKID", bankaTf.getText().trim());
					query.setText(queryString);
					design.setQuery(query);
					
					JasperReport report = JasperCompileManager.compileReport(design);
					
					JasperPrint jp;
					jp = JasperFillManager.fillReport(report, null, DBConnection.getConnection());
					
					JasperViewer jasperViewer = new JasperViewer(jp, false, Locale.getDefault());
					JDialog jasperDialog = new JDialog();
					jasperDialog.setModal(true);
					jasperDialog.setSize(1200, 700);
					jasperDialog.setLocationRelativeTo(null);
					jasperDialog.setTitle("Spisak računa banke");
					jasperDialog.add(jasperViewer.getContentPane());
					jasperDialog.setVisible(true);
					
				} catch (JRException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch(Exception ex) 
				{
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		
		zoomButton1.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				RacunStandardForm racunForm = new RacunStandardForm();
				racunForm.setVisible(true);
				racunTf.setText(racunForm.getTfBrojRacuna().getText());
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});

		createReportButton1.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(validateInputKlijenti() == false) return;
				
				String pathJrxml = new File("jrxml/IzvodKlijenata.jrxml").getAbsolutePath();
				
				try {
					JasperDesign design = JRXmlLoader.load(pathJrxml);
					JRDesignQuery query = (JRDesignQuery) design.getQuery();
					String queryString = query.getText();
					queryString = queryString.replace("RACUNRACUN", racunTf.getText().trim());
					queryString = queryString.replace("DATUMODDATUMOD", String.valueOf(panelDatumOd.getModel().getValue()));
					queryString = queryString.replace("DATUMDODATUMDO", String.valueOf(panelDatumDo.getModel().getValue()));
					queryString = queryString.replace("PERIODPERIOD", String.valueOf(panelDatumOd.getModel().getValue()) + " do " + String.valueOf(panelDatumDo.getModel().getValue()));
					System.out.println(queryString);
					query.setText(queryString);
					design.setQuery(query);
					
					JasperReport report = JasperCompileManager.compileReport(design);
					
					JasperPrint jp;
					jp = JasperFillManager.fillReport(report, null, DBConnection.getConnection());
					
					JasperViewer jasperViewer = new JasperViewer(jp, false, Locale.getDefault());
					JDialog jasperDialog = new JDialog();
					jasperDialog.setModal(true);
					jasperDialog.setSize(1200, 700);
					jasperDialog.setLocationRelativeTo(null);
					jasperDialog.setTitle("Izvod klijenta");
					jasperDialog.add(jasperViewer.getContentPane());
					jasperDialog.setVisible(true);
					
				} catch (JRException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch(Exception ex) 
				{
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
				
			}

			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
	}
	
	private boolean validateInputRacuni()
	{
		boolean valid = true;
		if(bankaTf.getText().trim().equals("")) 
		{
			JOptionPane.showMessageDialog(null, "Niste izabrali banku za koju želite izveštaj!", "Error", JOptionPane.ERROR_MESSAGE);
			valid = false;
		}
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT b_id FROM banka WHERE b_id = '" + bankaTf.getText().trim() + "'");
				if (!rset.next())
				{
					JOptionPane.showMessageDialog(null, "Banka čiji je ID " + bankaTf.getText().trim() + " ne postoji.", "Error", JOptionPane.ERROR_MESSAGE);
					valid = false;
				}
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return valid;
	}
	

	
	private boolean validateInputKlijenti() {
		String errorMessage = "";
		if((modelDatumOd.getValue() == null))
		{
			errorMessage += "Datum početka izveštaja nije odabran.\n";
		}
		if((modelDatumDo.getValue() == null))
		{
			errorMessage += "Datum kraja izveštaja nije odabran.\n";
		}
		if(racunTf.getText().trim().equals("")) 
		{
			errorMessage += "Račun za koji kreiramo izveštaj nije odabran.\n";
		}
		else {
			try {
				Statement stmnt = DBConnection.getConnection().createStatement();
				ResultSet rset = stmnt.executeQuery("SELECT r_brracuna FROM racun WHERE r_brracuna = '" + racunTf.getText().trim() + "'");
				if (!rset.next())
				{
					errorMessage += "Račun čiji je ID " + racunTf.getText().trim() + " ne postoji.\n";
				}
				rset.close();
				stmnt.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		if (errorMessage.equals(""))
		{
			return true;
		}
		else
		{
			JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
}
